<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default validation settings
    |--------------------------------------------------------------------------
    |
    | //
    |
    */

    'user' => [
		'signin' => [
			'user.email'		=> 'required'
		],
		'signup' => [
			'user.email'		=> 'required',
			'user.first_name'	=> 'required',
			'user.last_name'	=> 'required'
		]
	]

];
