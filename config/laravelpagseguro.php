<?php

return [
    'sandbox' => (env('APP_ENV') == 'production') ? false : true, //DEFINE SE SERÁ UTILIZADO O AMBIENTE DE TESTES

    'credentials' => [//SETA AS CREDENCIAIS DE SUA LOJA
        'email' => env('PAGSEGURO_EMAIL'),
        'token' => env('PAGSEGURO_TOKEN')
    ],
    'currency' => [//MOEDA QUE SERÁ UTILIZADA COMO MEIO DE PAGAMENTO
        'type' => 'BRL'
    ],
    'reference' => [//CRIAR UMA REFERENCIA PARA OS PRODUTOS VENDIDOS
        'idReference' => NULL
    ],
    'proxy' => [//CONFIGURAÇÃO PARA PROXY
        'user'     => NULL,
        'password' => NULL,
        'url'      => NULL,
        'port'     => NULL,
        'protocol' => NULL
    ],
    'routes' => [
        'notification' => [
            'callback' => function ($information) { // Callable
                $information->reference;
                $invoice = \App\Models\Invoice::find($information->reference);

                // Saves data into invoice callback field
                $invoice->callback = json_encode($information);
                $invoice->save();

                \Log::debug(print_r($information, 1));
            },
            'credential' => 'default',
            'route-name' => 'pagseguro.notification'
        ],
        'checkout' => [
            'route-name' => 'home'
        ]
    ],
    'http' => [
        'adapter' => [
            'type' => 'curl',
            'options' => [
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_0,
                //CURLOPT_PROXY => 'http://user:pass@host:port', // PROXY OPTION
            ]
        ],
    ],
    'url' => [
        'checkout' => (env('APP_ENV') == 'production') ? 'https://ws.pagseguro.uol.com.br/v2/checkout' : 'https://ws.sandbox.pagseguro.uol.com.br/v2/checkout',
        'transactions' => '/v3/transactions',
        'transactions-notifications' => '/v3/transactions/notifications',
        'transactions-history' => '/v2/transactions',
        'transactions-abandoned' => '/v2/transactions/abandoned',
    ]
];
