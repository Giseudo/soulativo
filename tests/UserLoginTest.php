<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserLoginTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $user = factory(App\Models\User::class)->create();

        $this->visit('/login')
            ->type($user->email, 'email')
            ->type(bcrypt($user->password), 'password')
            ->press('Login')
            ->seePageIs('/home');
    }
}
