<fieldset>
    <h2>@lang('front.profile.customer')</h2>
    <div class="form-group">
        <label class="text-help" for="">@lang('model.customer.telephone')</label>
        {!! Form::text('customer[telephone]', null, ['id' => 'customer-telephone', 'class' => 'form-control -dashed']) !!}
    </div>
    <div class="row">
        <div class="form-group col-xs-12 col-md-6">
            <label class="text-help" for="">@lang('model.customer.birthday')</label>
            {!! Form::text('customer[birthday]', (!is_null(@$customer) ? $customer->formatedBirthday : null ), ['id' => 'customer-birthday', 'class' => 'form-control -dashed']) !!}
        </div>
        <div class="form-group col-xs-12 col-md-6">
            <label class="text-help" for="">@lang('model.customer.document')</label>
            {!! Form::text('customer[document]', null, ['id' => 'customer-document', 'class' => 'form-control -dashed']) !!}
        </div>
    </div>
</fieldset>
