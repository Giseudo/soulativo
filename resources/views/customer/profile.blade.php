@extends('layouts.app')

@section('content')

    <header class="-page-head" style="background-image: url(/images/delete/bg-top-01.jpg)">
        <div class="container">
            <div class="wrapper">
                <h1>@lang('front.profile.title')</h1>
            </div>
        </div>
    </header>
    <div class="container">
        <div class="customer">
            <div class="-profile col-xs-12 col-md-6">
                <h3>{{$customer->user->name}}</h3>
                <h4>{{$customer->user->email}}</h4>
                <p>@lang('model.customer.document') {{$customer->document}}</p>
                <ul>
                    <li>{{$customer->address->street}}, {{$customer->address->number}}</li>
                    <li>{{$customer->address->complement}}</li>
                    <li>{{$customer->address->neighbourhood}}</li>
                    <li>{{$customer->address->city}}, {{$customer->address->state}}</li>
                    <li>{{$customer->address->zipcode}}</li>
                </ul>
                <p>{{$customer->telephone}}</p>

                <a href="/customer/profile/edit" class="edit">Editar cadastro</a>
            </div>
        </div>

        <span class="-sep p-t-3 p-b-2"></span>

        <div class="orders -list col-xs-12 col-md-8 col-md-offset-2">
            <header>
                <h2>@lang('front.profile.orders')</h2>
            </header>
            @foreach($orders as $order)
                @include('orders.loop')
            @endforeach
        </div>
    </div>

@endsection
