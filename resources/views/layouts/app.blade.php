<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <?php $locale = str_replace('-','_', config('app.locale'));  ?>
    <meta property="og:locale" content="{{$locale}}">
    <meta property="og:url" content="{{Request::url()}}">
    @if(!empty(Session::get('seo')))
        <meta property="og:title" content="{{Session::get('seo')->title}}">
        <meta property="og:site_name" content="Soulativo">
        <meta property="og:description" content="{{Session::get('seo')->description}}">
        <meta property="og:type" content="website">
        <meta property="og:image" content="https://soulativo.com.br/images/soulativo-logo.png">
        <meta property="og:image:type" content="image/png">
    @endif
    <link href="/favicon.png" rel="icon" type="image/png">
    <meta charset="utf-8">
    <title>SoulAtivo</title>

    <!-- Styles -->
    <link  href="/css/app.css" rel="stylesheet">

    <script type="text/javascript">
        window.config = {
            locale : '{{ config('app.locale') }}',
            stripe_key : '{{ config('services.stripe.key') }}'
        }
    </script>
    <script> (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) })(window,document,'script','https://www.google-analytics.com/analytics.js','ga'); ga('create', 'UA-82881250-1', 'auto'); ga('send', 'pageview'); </script>
    <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','https://connect.facebook.net/en_US/fbevents.js');

            fbq('init', '250699488659954');
            fbq('track', "PageView");
        </script>
        <noscript>
            <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=250699488659954&ev=PageView&noscript=1"/>
        </noscript>
    <!-- End Facebook Pixel Code -->
</head>
<body data-view="@if(!empty($frontend['view'])){{ $frontend['view'] }}@endif">

<?php $totalItens = Cart::totalItems(true); ?>

    <div id="main-top" class="@if(!empty($frontend['top-class'])){{ $frontend['top-class'] }}@endif">
        <header>
            <div class="container">
                <a href="/" title="SoulAtivo" class="logo">
                    <img src="/images/soulativo-blue.png" alt="SoulAtivo" class="hidden-sm-down">
                    <img src="/images/soulativo-white.png" alt="SoulAtivo" class="hidden-md-up">
                </a>
                @include('partials.social', ['class' => 'hidden-md-down'])
                <nav id="smartphone-menu" class="hidden-md-up">
                    <button class="hamburger hamburger--collapse" type="button">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                    <button class="icon icon-icon-magnifier search"></button>


                    <div class="smartphone-site-map">
                        <nav class="lang col-xs-12 col-md-6">
                            <a href="/lang/en-US" class="@if(config('app.locale') == 'en-US') -active @endif">English <img src="/images/flags/american-flag@2x.png" alt="English"></a>
                            <!--<a href="/lang/es-ES" class="@if(config('app.locale') == 'es-ES') -active @endif">Español <img src="/images/flags/spanish-flag@2x.png" alt="Spanish"></a>-->
                            <a href="/lang/pt-BR" class="@if(config('app.locale') == 'pt-BR') -active @endif">Português <img src="/images/flags/brazilian-flag@2x.png" alt="Portuguese"></a>
                        </nav>

                        <ul class="first-menu">
                            <li class="dropdown">
                                <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown">
                                    @if(config('app.currency.name') == 'BRL')
                                        @lang('front.menu.currency.real')
                                    @elseif(config('app.currency.name') == 'USD')
                                        @lang('front.menu.currency.dollar')
                                    @elseif(config('app.currency.name') == 'EUR')
                                        @lang('front.menu.currency.euro')
                                    @endif
                                </button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="/currency/real">@lang('front.menu.currency.real')</a>
                                    <a class="dropdown-item" href="/currency/dollar">@lang('front.menu.currency.dollar')</a>
                                    <a class="dropdown-item" href="/currency/euro">@lang('front.menu.currency.euro')</a>
                                </div>
                            </li>
                            <li>
                                <a href="/cart" class="cart @if($totalItens > 0) -active @endif">
                                    <span class="icon icon-cart"></span>
                                    {{$totalItens}}
                                    @if($totalItens > 1)
                                        @lang('front.cart.bag.plural')
                                    @else
                                        @lang('front.cart.bag.singular')
                                    @endif
                                </a>
                            </li>
                        </ul>

                        <nav class="map -upper">
                            @foreach(\App\Models\Page::all() as $page)
                                <a href="/{{$page->slug}}">{{$page->title}}</a>
                            @endforeach
                        </nav>

                        <nav class="map -upper">
                            <a href="/404">FAQ</a>
                            <a href="/contact">@lang('front.contact.title')</a>
                        </nav>

                        <nav class="map -upper">
                            <a href="/blog">Blog</a>
                            @if(Auth::check())
                                <a href="/customer/profile">{{Auth::user()->first_name}} {{Auth::user()->last_name}}</a>
                            @else
                                <a href="#auth-modal" data-toggle="modal">@lang('front.auth.signin')</a>
                            @endif
                        </nav>

                        @if(!is_null(\App\Models\Category::findBySlug('adventure')->getDescendants()))
                            <nav class="map">
                                <h3>@lang('front.categories.adventure')</h3>
                                @foreach(@\App\Models\Category::findBySlug('adventure')->getDescendants() as $category)
                                    <a href="/packages/{{$category->slug}}">{{$category->name}}</a>
                                @endforeach
                            </nav>
                            @endif
                            @if(!is_null(\App\Models\Category::findBySlug('transport')->getDescendants()))
                            <nav class="map">
                                <h3>@lang('front.categories.transport')</h3>
                                @foreach(@\App\Models\Category::findBySlug('transport')->getDescendants() as $category)
                                    <a href="/packages/{{$category->slug}}">{{$category->name}}</a>
                                @endforeach
                            </nav>
                        @endif

                        @if(!is_null(\App\Models\Category::findBySlug('city-tour')->getDescendants()))
                            <nav class="map">
                                <h3>@lang('front.categories.city-tour')</h3>
                                @foreach(@\App\Models\Category::findBySlug('city-tour')->getDescendants() as $category)
                                    <a href="/packages/{{$category->slug}}">{{$category->name}}</a>
                                @endforeach
                            </nav>
                        @endif

                        @include('partials.social')
                    </div>
                </nav>
                <nav id="main-menu">
                    <ul>
                        <li class="-submenu dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown">
                                @lang('front.menu.lang.current')
                            </button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="/lang/pt-BR">@lang('front.menu.lang.portuguese')</a>
                                <a class="dropdown-item" href="/lang/en-US">@lang('front.menu.lang.english')</a>
                                <!--<a class="dropdown-item" href="/lang/es-ES">@lang('front.menu.lang.spanish')</a>-->
                            </div>
                        </li>
                        <li class="-submenu dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown">
                                @if(config('app.currency.name') == 'BRL')
                                    @lang('front.menu.currency.real')
                                @elseif(config('app.currency.name') == 'USD')
                                    @lang('front.menu.currency.dollar')
                                @elseif(config('app.currency.name') == 'EUR')
                                    @lang('front.menu.currency.euro')
                                @endif
                            </button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="/currency/real">@lang('front.menu.currency.real')</a>
                                <a class="dropdown-item" href="/currency/dollar">@lang('front.menu.currency.dollar')</a>
                                <a class="dropdown-item" href="/currency/euro">@lang('front.menu.currency.euro')</a>
                            </div>
                        </li>
                        @if(Auth::check())
                            <li>
                                <a href="/customer/profile" style="font-weight: bold;">{{Auth::user()->first_name}} {{Auth::user()->last_name}}</a>
                            </li>
                            <li>
                                <a href="/logout">@lang('front.auth.signout')</a>
                            </li>
                        @else
                            <li>
                                <a href="#auth-modal" data-toggle="modal">@lang('front.auth.signin')</a>
                            </li>
                        @endif
                        <li>
                            <a href="/contact">@lang('front.contact.title')</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </header>
        <footer class="@if(@$submenu != 'not-fixed') -fixed -dark @endif">
            <div class="container">
                <nav id="sub-menu">
                    <a href="/packages/adventure">@lang('front.categories.adventure')</a>
                    <a href="/packages/city-tour">@lang('front.categories.city-tour')</a>
                    <a href="/packages/transport">@lang('front.categories.transport')</a>
                    <!-- <a href="/packages" class="icon icon-dot-3">&nbsp;</a> -->
                </nav>
                <nav class="extra">
                    <a href="/cart" class="cart @if($totalItens > 0) -active @endif">
                        <span class="icon icon-cart"></span>
                        @if($totalItens < 1)
                            @lang('front.cart.bag.empty')
                        @else
                            {{$totalItens}}
                            @if($totalItens > 1)
                                @lang('front.cart.bag.plural')
                            @else
                                @lang('front.cart.bag.singular')
                            @endif
                        @endif
                    </a>
                    <button class="search">
                        <span class="icon icon-icon-magnifier"></span>&nbsp;
                    </button>
                </nav>
            </div>
        </footer>
    </div>

    @yield('content')

    <div id="main-bottom" class="-dark">
        <span class="soulativo-flag icon icon-icon"></span>
        <header class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-lg-5">
                    <div class="col-xs-12 col-md-4">
                        <nav class="-upper">
                            @foreach(\App\Models\Page::all() as $page)
                                <a href="/{{$page->slug}}">{{$page->title}}</a>
                            @endforeach
                        </nav>
                        <nav class="-upper">
                            <a href="/contact">@lang('front.contact.title')</a>
                        </nav>
                        <nav class="-upper">
                            <a href="/blog">Blog</a>
                            @if(Auth::check())
                                <a href="/customer/profile">@lang('front.profile.title')</a>
                            @else
                                <a href="#auth-modal" data-toggle="modal">@lang('front.auth.signin')</a>
                            @endif
                        </nav>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        @if(!is_null(\App\Models\Category::findBySlug('adventure')->getDescendants()))
                        <nav>
                            <h3>@lang('front.categories.adventure')</h3>
                            @foreach(@\App\Models\Category::findBySlug('adventure')->getDescendants() as $category)
                                <a href="/packages/{{$category->slug}}">{{$category->name}}</a>
                            @endforeach
                        </nav>
                        @endif
                        @if(!is_null(\App\Models\Category::findBySlug('transport')->getDescendants()))
                        <nav>
                            <h3>@lang('front.categories.transport')</h3>
                            @foreach(@\App\Models\Category::findBySlug('transport')->getDescendants() as $category)
                                <a href="/packages/{{$category->slug}}">{{$category->name}}</a>
                            @endforeach
                        </nav>
                        @endif
                    </div>
                    <div class="col-xs-12 col-md-4">
                        @if(!is_null(\App\Models\Category::findBySlug('city-tour')->getDescendants()))
                        <nav>
                            <h3>@lang('front.categories.city-tour')</h3>
                            @foreach(@\App\Models\Category::findBySlug('city-tour')->getDescendants() as $category)
                                <a href="/packages/{{$category->slug}}">{{$category->name}}</a>
                            @endforeach
                        </nav>
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-md-4 col-lg-offset-3">
                    @include('newsletter.footer')
                    <div class="methods col-xs-12">
                        <h4>@lang('front.payment.title')</h4>
                        <img src="/images/payments@2x.png" alt="">
                    </div>
                </div>
            </div>
        </header>
        <footer class="container">
            <div class="wrapper">
                <div class="row">
                    <nav class="lang col-xs-12 col-md-6">
                        <a href="/lang/en-US" class="@if(config('app.locale') == 'en-US') -active @endif">English <img src="/images/flags/american-flag@2x.png" alt="English"></a>
                        <!--<a href="/lang/es-ES" class="@if(config('app.locale') == 'es-ES') -active @endif">Español <img src="/images/flags/spanish-flag@2x.png" alt="Spanish"</a>-->
                        <a href="/lang/pt-BR" class="@if(config('app.locale') == 'pt-BR') -active @endif">Português <img src="/images/flags/brazilian-flag@2x.png" alt="Portuguese"</a>
                    </nav>
                    @include('partials.social')
                </div>
            </div>
        </footer>
        <small class="copyright">
            <a href="/" class="logo"><img src="/images/soulativo-white.png" alt=""></a>
            <span>© @lang('front.copyright')</span>
        </small>
    </div>

    @include('auth.modal')
    @include('search.modal')
    @include('partials.templates')

    <!-- JavaScripts -->
    <script src="{{elixir('js/app.js')}}"></script>
   
    <!-- Código do Google para tag de remarketing -->
    <!--------------------------------------------------
    As tags de remarketing não podem ser associadas a informações pessoais de identificação nem inseridas em páginas relacionadas a categorias de confidencialidade. Veja mais informações e instruções sobre como configurar a tag em: http://google.com/ads/remarketingsetup
    --------------------------------------------------->
    <script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 875379907;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
    </script>
    
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
    
    <noscript>
        <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/875379907/?value=1&amp;guid=ON&amp;script=0"/>
        </div>
    </noscript>
</body>
</html>
