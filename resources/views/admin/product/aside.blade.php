<aside class="product card">
    <header class="card-header">
        Price
    </header>
    <ul class="list-group list-group-flush">
        @if(@$product)
            <li class="list-group-item"><strong>BRL {{$product->getPrice('BRL')}}</strong></li>
            <li class="list-group-item">USD {{$product->getPrice('USD')}}</li>
            <li class="list-group-item">EUR {{$product->getPrice('EUR')}}</li>
        @endif
    </ul>
</aside>
