<fieldset id="product">
    <legend>@lang('model.product.singular')</legend>
    <div class="row">
        <div class="form-group col-xs-12 col-xl-6">
            <label class="text-help" for="product-name">@lang('model.product.name')</label>
            {!! Form::text('product[name]', null, ['id' => 'product-name', 'class' => 'form-control']) !!}
        </div>
        <div class="form-group col-xs-12 col-xl-6">
            <label class="text-help" for="product-slug">@lang('model.product.slug')</label>
            {!! Form::text('product[slug]', null, ['id' => 'product-slug', 'class' => 'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="text-help" for="product-content">@lang('model.product.content')</label>
        {!! Form::hidden('product[content]', null, ['id' => 'product-content', 'class' => 'form-control']) !!}
        <div class="-dante">
            <div id="page-editor" class="-content">
                @if(old('product')['content'])
                    {!!old('product')['content']!!}
                @else
                    {!!@$product[content]!!}
                @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-xs-12 col-xl-6">
            <label class="text-help" for="product-price">@lang('model.product.price')</label>
            <div class="input-group">
                <div class="input-group-addon">R$</div>
                {!! Form::text('product[price]', null, ['id' => 'product-price', 'class' => 'form-control']) !!}
                <div class="input-group-addon">.00</div>
            </div>
        </div>
        <div class="form-group col-xs-12 col-xl-6">
            <label class="text-help" for="product-child_percent">Criança(Percent.)</label>
            <div class="input-group">
                <div class="input-group-addon">%</div>
                {!! Form::text('product[child_percent]', null, ['id' => 'product-child_percent', 'class' => 'form-control']) !!}
                <div class="input-group-addon">.00</div>
            </div>
        </div>
        <div class="form-group col-xs-12 col-xl-6">
            <label class="text-help" for="product-store">@lang('model.product.store')</label>
            {!! Form::select('product[store_id]', $stores->lists('name', 'id'), null, ['id' => 'product-store', 'class' => 'form-control c-select']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="text-help" for="product-excerpt">@lang('model.product.excerpt')</label>
        {!! Form::textarea('product[excerpt]', null, ['id' => 'product-excerpt', 'class' => 'form-control', 'rows' => '5']) !!}
    </div>
    {!! Form::hidden('product[id]', null, ['id' => 'product-id']) !!}
</fieldset>
