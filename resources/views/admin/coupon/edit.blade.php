@extends('admin.layouts.app')

@section('content')
    {!! Form::model($coupon, array(
        'method' => 'post',
        'action' => ['Admin\CouponController@update', $coupon->id],
        'id' => 'form-coupon'
    )) !!}

        @include('admin.coupon.form', ['submitButton' => 'Update'])

    {!! Form::close() !!}
@endsection
