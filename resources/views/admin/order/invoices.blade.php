<div id="order-invoices" class="card">
    <header class="card-header">
        Order (#{{$order->id}}) – {{$order->created_at}}
    </header>
    <ul class="m-t-1">
        <li>Cliente: <a href="/admin/customer/edit/{{$order->customer->id}}">{{$order->customer->user->name}}</a></li>
        <li>
            Reservas:
            <ul>
                @foreach($order->bookings as $booking)
                    <li>
                        {{$booking->package->product->name}} – R${{$booking->getDiscountedPrice('BRL')}}
                        <ul>
                            <li>Adultos: {{$booking->adults}}</li>
                            <li>Crianças: {{$booking->childs}}</li>
                        </ul>
                    </li>
                @endforeach
            </ul>
        </li>
        <li>Total: R${{$order->getTotal()}}</li>
    </ul>
    @if(count($order->invoices) > 0)
        <table class="table table-striped m-b-0">
            <thead class="">
                <tr>
                    <th>Payment Method</th>
                    <th>Date</th>
                    <th>Value</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @foreach($order->invoices as $invoice)
                    @if($invoice->method != 'mercadopago')
                        <tr>
                            <td>{{ucwords($invoice->method)}}</td>
                            <td><small>{{$invoice->created_at->format(trans('model.dateformat.full'))}}</small></td>
                            <td>{{$invoice->getPrice()}}</td>
                            <td><span class="label {{$invoice->getStatus()['class']}}">{{$invoice->getStatus()['text']}}</span></td>
                        </tr>
                    @else
                        @if(isset($invoice->getCallback()->payments))
                            @foreach($invoice->getCallback()->payments as $key => $payment)
                                <tr>
                                    <td>{{ucwords($invoice->method)}}</td>
                                    <td><small>{{$invoice->created_at}}</small></td>
                                    <td>{{$invoice->getPrice($key)}}</td>
                                    <td><span class="label {{$invoice->getStatus($key)['class']}}">{{$invoice->getStatus($key)['text']}}</span></td>
                                </tr>
                            @endforeach
                        @endif
                    @endif
                @endforeach
            </tbody>
        </table>
    @endif
    <footer class="card-footer text-muted">
        <a href="#order-modal" data-toggle="modal">Send order to customer's email</a>
    </footer>
</div>
