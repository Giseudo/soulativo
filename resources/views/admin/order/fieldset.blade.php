<fieldset id="order">
    <legend>@lang('model.order.singular')</legend>
    <div class="row">
        <div class="form-group col-xs-12 col-xl-6">
            <label class="text-help" for="order-total">@lang('model.order.total')</label>
            {!! Form::text('order[total]', null, ['id' => 'order-total', 'class' => 'form-control', 'readonly']) !!}
        </div>
        <div class="form-group col-xs-12 col-xl-6">
            <label class="text-help" for="order-discount">@lang('model.order.discount')</label>
            {!! Form::text('order[discount]', null, ['id' => 'order-discount', 'class' => 'form-control', 'readonly']) !!}
        </div>
    </div>
</fieldset>
