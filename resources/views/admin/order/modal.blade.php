<div class="order modal fade" id="order-modal" tabindex="-1">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span>&times;</span>
                </button>
                <h4 class="modal-title">Send order to customer's email</h4>
            </div>

            <div class="modal-body">
                {!! Form::model($order, [
                    'method' => 'post',
                    'action' => ['Admin\OrderController@sendMail', $order->id],
                    'id' => 'form-order-sendmail'
                ]) !!}
                    <div class="form-group">
                        <label for="recipient-name">Recipient:</label>
                        <input type="text" class="form-control" name="email" id="recipient-name" readonly="readonly" value="{{$order->customer->user->email}}">
                    </div>
                    <div class="form-group">
                        <label for="message-text">Message:</label>
                        <textarea class="form-control" name="text" id="message-text" rows="5"></textarea>
                    </div>
                    <!-- If there's no coupon in the order -->
                    <div class="empty-coupon @if($order->coupon_id == '') -active @endif">
                        <div class="form-group">
                            <button class="add btn btn-primary-outline btn-sm">Novo cupom de desconto</button>
                        </div>
                    </div>
                {!! Form::close() !!}

                <!-- Show existing coupon -->
                {!! Form::model($order, [
                    'method' => 'post',
                    'action' => ['Admin\OrderController@removeCoupon', $order->id],
                    'id' => 'form-remove-coupon',
                    'class' => 'show-coupon ' . (($order->coupon_id != '') ? '-active' : '')
                ]) !!}
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <label for="">Valor do cupom:</label>
                                <div class="input-group">
                                    {!! Form::text('coupon[value]', (($order->coupon) ? $order->coupon->value : ''), ['id' => 'coupon-value', 'class' => 'form-control', 'readonly']) !!}
                                    <div class="input-group-addon">%</div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <label for="">Código do cupom:</label>
                                {!! Form::text('coupon[key]', (($order->coupon) ? $order->coupon->key : ''), ['id' => 'coupon-value', 'class' => 'form-control', 'readonly']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="add btn btn-primary-outline btn-sm">Novo cupom</button>
                        <button class="remove btn btn-danger-outline btn-sm">Remover</button>
                    </div>
                {!! Form::close() !!}

                <!-- Add new coupon inputs -->
                {!! Form::model($order, [
                    'method' => 'post',
                    'action' => ['Admin\OrderController@applyCoupon', $order->id],
                    'id' => 'form-add-coupon',
                    'class' => 'add-coupon'
                ]) !!}
                    <div class="form-group">
                        {!! Form::hidden('coupon[id]', null, ['id' => 'coupon-id']) !!}
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <label for="">Aplicar desconto:</label>
                                <div class="input-group">
                                    {!! Form::number('coupon[value]', null, ['id' => 'coupon-value', 'class' => 'form-control']) !!}
                                    <div class="input-group-addon">%</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="apply btn btn-primary-outline btn-sm">Aplicar</button>
                        <button class="cancel btn btn-danger-outline btn-sm">Cancelar</button>
                    </div>
                {!! Form::close() !!}
            </div>

            <div class="modal-footer">
                <span class="price">
                    <span class="subtotal">
                        <span class="old">R$ <span>{{$order->getTotal()}}</span></span>
                        <span class="discount">R$ @if($order->coupon) <span>{{$order->getDiscount()}}</span> @else <span>0.00</span> @endif</span>
                    </span>
                    <span class="total">R$ @if($order->coupon_id) <span>{{$order->getDiscountedTotal()}}</span> @else <span>{{$order->getTotal()}}</span> @endif</span>
                </span>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="submit btn btn-primary" data-dismiss="modal">Send email</button>
            </div>
        </div>
    </div>
</div>
