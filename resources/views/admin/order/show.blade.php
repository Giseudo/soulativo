@extends('admin.layouts.app')

@section('content')
    {!! Form::open(array(
        'url' => 'admin/booking/create',
        'id' => 'form-package',
        'method' => 'post'
    )) !!}

    <div class="col-xs-12 col-xl-8">
        @include('admin.partials.flash')
        @include('admin.order.invoices')
    </div>

    {!! Form::close() !!}

    @include('admin.order.modal')
@endsection
