@extends('admin.layouts.app')

@section('content')
    <div class="col-xs-12">
        <nav class="navbar navbar-light bg-faded m-b-1" style="background-color: #e3f2fd;">
            <a class="navbar-brand">@lang('model.order.plural')</a>
            @include('admin.search.form', ['action' => '/order/search'])
        </nav>

        <div class="tab-content">
            <table class="table table-striped">
                <thead class="">
                    <tr>
                        <th>Ref</th>
                        <th>@lang('model.customer.singular')</th>
                        <th>@lang('model.order.total')</th>
                        <th>@lang('model.order.created_at')</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($orders as $order)
                        <tr>
                            <td><a href="{{ url('admin/order/show/' . $order->id) }}">#{{$order->id}}</a></td>
                            <td><a href="{{ url('admin/customer/edit/' . $order->customer->id) }}">{{$order->customer->user->name}}</a></td>
                            <td>R${{$order->getTotal()}}</td>
                            <td><small>{{$order->created_at}}</small></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            <nav>
                <ul class="pager">
                    <li><a href="/order" class="paginate" data-page="1" data-limit="{{$perPage}}">Load more</a></li>
                </ul>
            </nav>
        </div>
    </div>

    <!-- Inclues template for pagination -->
    @include('admin/order/templates/list')
@endsection
