<script type="text/template" id="order-list-template">
    <% for(var order in result) { %>
        <tr>
            <td><a href="{{ url('admin/order/show') }}/<%= result[order].id %>">#<%= result[order].id %></a></td>
            <td><a href="{{ url('admin/customer/edit') }}/<%= result[order].customer_id %>"><%= result[order].user_name %></a></td>
            <td>R$<%= result[order].total %></td>
            <td><small><%= result[order].date %></small></td>
        </tr>
    <% } %>
</script>
