
<aside class="card">
    <header class="card-header">
        Order details
    </header>
    <div class="card-block">
        <div class="form-group">
            <label class="text-help" for="booking-package">@lang('model.customer.singular')</label>
            <select id="order-customer" name="order[customer_id]" class="form-control c-select">
                <option value="">– Select the customer –</option>
                @foreach($customers as $customer)
                    <option value="{{$customer->id}}" @if($customer->id == @$booking->order->customer->id) selected="selected" @endif>{{$customer->user->name}}</option>
                @endforeach
            </select>
        </div>
    </div>
</aside>
