<script type="text/template" id="post-list-template">
    <% for(var post in result) { %>
        <tr>
            <td>
                <input type="checkbox" name="checkbox[]" class="m-r-1" value="<%= result[post].id %>">
                <a href="{{ url('admin/post/edit') }}/<%= result[post].id %>"><%= result[post].title %></a></td>
            <td><small><%= result[post].date %></small></td>
        </tr>
    <% } %>
</script>
