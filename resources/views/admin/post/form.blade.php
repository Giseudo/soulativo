<div class="col-xs-12 col-xl-8">
    @include('admin.partials.flash')
    @include('admin.post.fieldset')
</div>

<aside class="col-xs-12 col-xl-4">
    @include('admin.partials.form.publish-fieldset')
    @include('admin.category.aside',
        [
            'id' => 'category-sidebar',
            'name' => 'post[categories][]',
            'root' => $root,
            'title' => trans('model.post.category'),
            'selected' => @$post->categories
        ]
    )
    @include('admin.gallery.thumbnail', [
        'id' => 'post-thumbnail_modal',
        'media' => @$post->thumbnail,
        'name' => 'post[thumbnail_id]'
    ])
</aside>
