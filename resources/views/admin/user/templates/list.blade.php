<script type="text/template" id="user-list-template">
    <% for(var user in result) { %>
        <tr>
            <td>
                <input type="checkbox" name="checkbox[]" class="m-r-1" value="<%= result[user].id %>">
                <a href="{{ url('admin/user/edit') }}/<%= result[user].id %>"><%= result[user].name %></a></td>
            <td><%= result[user].email %></td>
            <td><small><%= result[user].last_login %></small></td>
        </tr>
    <% } %>
</script>
