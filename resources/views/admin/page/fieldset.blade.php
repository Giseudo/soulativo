<fieldset id="package">
    <legend>@lang('model.page.singular')</legend>
    <div class="row">
        <div class="form-group col-xs-12 col-md-6">
            <label class="text-help" for="page-title">@lang('model.page.title')</label>
            {!! Form::text('page[title]', null, ['id' => 'page-title', 'class' => 'form-control']) !!}
        </div>
        <div class="form-group col-xs-12 col-md-6">
            <label class="text-help" for="page-slug">@lang('model.page.slug')</label>
            {!! Form::text('page[slug]', null, ['id' => 'page-slug', 'class' => 'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="text-help" for="product-content">@lang('model.product.content')</label>
        {!! Form::hidden('page[content]', null, ['id' => 'page-content', 'class' => 'form-control']) !!}
        <div class="-dante">
            <div id="page-editor" class="-content">
                @if(old('page')['content'])
                    {!!old('page')['content']!!}
                @else
                    {!!@$page[content]!!}
                @endif
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="text-help" for="page-excerpt">@lang('model.page.excerpt')</label>
        {!! Form::textarea('page[excerpt]', null, ['id' => 'page-excerpt', 'class' => 'form-control', 'rows' => '5']) !!}
    </div>
</fieldset>
