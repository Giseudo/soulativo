<script type="text/template" id="page-list-template">
    <% for(var page in result) { %>
        <tr>
            <td>
                <input type="checkbox" name="checkbox[]" class="m-r-1" value="<%= result[page].id %>">
                <a href="{{ url('admin/page/edit') }}/<%= result[page].id %>"><%= result[page].title %></a></td>
            <td><small><%= result[page].date %></small></td>
        </tr>
    <% } %>
</script>
