@extends('admin.layouts.app')

@section('content')
    {!! Form::model($page, array(
        'method' => 'post',
        'action' => ['Admin\PageController@update', $page->id],
        'id' => 'form-page'
    )) !!}

        @include('admin.page.form', ['submitButton' => 'Update'])

    {!! Form::close() !!}
@endsection
