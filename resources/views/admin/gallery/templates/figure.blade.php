<script type="text/template" id="gallery-figure-template">
    <% if(!loading){ %>
    <figure class="col-xs-6 col-md-4 col-xl-3" data-id="<%= id %>" data-filename="<%= filename %>" data-title="<%= title %>" data-created="<%= created %>" data-size="<%= size %>" data-width="<%= width %>" data-height="<%= height %>" data-caption="<%= caption %>">
        <span class="fa fa-check-square"></span>
        <span class="fa fa-minus-square"></span>
        <img src="/images/<%= filename %>?w=150&h=150&fit=crop-50-50" alt="<%= title %>">
    </figure>
    <% }else{ %>
        <figure class="loading col-xs-6 col-md-4 col-xl-3">
            <img src="/backend/images/loading.gif" alt="">
        </figure>
    <% } %>
</script>
