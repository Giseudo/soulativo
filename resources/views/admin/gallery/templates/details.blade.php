<script type="text/template" id="gallery-details-template">
    <h3>Detalhes do Anexo</h3>
    <div class="details">
        <figure>
            <img src="<%= path %>" alt="">
        </figure>
        <ul>
            <li><strong><%= title %></strong></li>
            <li>Criado em: <%= created %></li>
            <li>Tamanho: <%= size %>kb</li>
            <!--li>Dimensões: <%= width %> x <%= height %></li-->
            <li>
                <form action="/admin/media/destroy/<%= id %>" data-id="<%= id %>" class="destroy">
                    <input type="submit" value="Excluir permanentemente">
                </form>
            </li>
        </ul>
        <form action="/admin/media/update/<%= id %>" class="update">
        	<div class="alert alert-danger">
    			<p>Erro: tente novamente mais tarde</p>
        	</div>

        	<div class="alert alert-success">
        		<p>Dados atualizados com sucesso</p>
        	</div>

            <div class="form-group">
                <input type="text" name="media[title]" placeholder="File Title" class="form-control" value="<%= title %>">
            </div>
            <div class="form-group">
                <textarea rows="5" name="media[caption]" placeholder="File Caption" class="form-control"><%= caption %></textarea>
            </div>
            <div class="form-group">
                <button class="btn btn-primary-outline btn-sm">Update</button>
            </div>
        </form>
    </div>
</script>
