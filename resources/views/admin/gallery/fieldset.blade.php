<fieldset id="{{ $id }}">
    <legend>@lang('model.gallery.singular')</legend>
    <div class="form-group gallery">
        <label class="text-help" for="{{ $id }}-set">@lang('model.gallery.album')</label>
        <a href="#{{ $id }}-modal" id="{{ $id }}-set" data-toggle="modal">Add images</a>
        <div id="{{ $id }}-wrapper" class="album row">
            @each('admin.gallery.loop', $medias, 'media')
        </div>
        {!! Form::hidden($name, null, ['class' => 'form-control gallery-json']) !!}
    </div>
</fieldset>
