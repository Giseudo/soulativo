<script type="text/template" id="promotion-list-template">
    <% for(var promotion in result) { %>
        <tr>
            <td>
                <input type="checkbox" name="checkbox[]" class="m-r-1" value="<%= result[promotion].id %>">
                <a href="{{ url('admin/promotion/edit') }}/<%= result[promotion].id %>"><%= result[promotion].name %></a></td>
            <td><small><%= result[promotion].date %></small></td>
        </tr>
    <% } %>
</script>
