@extends('admin.layouts.app')

@section('content')
    <div class="col-xs-12">
        <nav class="navbar navbar-light bg-faded m-b-1" style="background-color: #e3f2fd;">
            <a class="navbar-brand">@lang('model.promotion.plural') <a href="{{ url('/admin/promotion/create') }}" class="btn btn-primary">Add new @lang('model.promotion.singular')</a></a>
            @include('admin.search.form', ['action' => '/promotion/search'])
        </nav>

        {!! Form::open(array('url' => '', 'method' => 'POST')) !!}
            <header class="m-t-1 m-b-1">
                <div class="btn-group m-r-1">
                    <button type="button" class="btn btn-primary-outline btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        – Actions in mass –
                    </button>
                    <div class="dropdown-menu">
                        <button data-action="/admin/promotion/destroy" data-target="#trash" class="dropdown-item">Move to Trash</button>
                    </div>
                </div>
            </header>
            <table class="table table-striped">
                <thead class="">
                    <tr>
                        <th>
                            <input type="checkbox" class="m-r-1" value=""> @lang('model.promotion.name')
                        </th>
                        <th>Created at</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($promotions as $promotion)
                    <tr>
                        <td>
                            <input type="checkbox" name="checkbox[]" class="m-r-1" value="{{$promotion->id}}">
                            <a href="{{ url('admin/promotion/edit/'.$promotion->id) }}">{{$promotion->name}}</a></td>
                        <td><small>{{ isset($promotion->created_at) ? $promotion->created_at->format(trans('model.dateformat.full')) : ' - ' }}</small></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

            <nav>
                <ul class="pager">
                    <li><a href="/promotion" class="paginate" data-page="1" data-limit="{{$perPage}}">Load more</a></li>
                </ul>
            </nav>
        {!! Form::close() !!}

    </div>

    <!-- Inclues template for pagination -->
    @include('admin/promotion/templates/list')
@endsection
