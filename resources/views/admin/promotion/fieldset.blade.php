<fieldset id="package">
    <legend>@lang('model.promotion.singular')</legend>
    <div class="row">
        <div class="form-group col-xs-12 col-md-6">
            <label class="text-help" for="promotion-name">@lang('model.promotion.name')</label>
            {!! Form::text('promotion[name]', null, ['id' => 'promotion-name', 'class' => 'form-control']) !!}
        </div>
        <div class="form-group col-xs-12 col-md-6">
            <label class="text-help" for="promotion-value">@lang('model.promotion.value')</label>
            <div class="input-group">
                {!! Form::number('promotion[value]', null, ['id' => 'promotion-value', 'class' => 'form-control']) !!}
                <span class="input-group-addon">%</span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-xs-12 col-md-6">
            <label class="text-help" for="promotion-from">@lang('model.promotion.from')</label>
            {!! Form::text('promotion[from]', null, ['id' => 'promotion-from', 'class' => 'form-control']) !!}
        </div>
        <div class="form-group col-xs-12 col-md-6">
            <label class="text-help" for="promotion-to">@lang('model.promotion.to')</label>
            {!! Form::text('promotion[to]', null, ['id' => 'promotion-to', 'class' => 'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="text-help" for="">@lang('model.promotion.rules')</label>
        <div id="promotion-rules"></div>
        <input id="product-categories" type="hidden" data-categories="{{$categories}}" value="{{$categories}}">
        {!! Form::hidden('promotion[rules]', null, ['id' => 'promotion-end', 'class' => 'form-control']) !!}
    </div>
    {!! Form::hidden('package[id]', null, ['id' => 'package-id']) !!}
</fieldset>
