@extends('admin.layouts.app')

@section('content')
    {!! Form::open(array(
        'url' => 'admin/promotion/create',
        'id' => 'form-promotion',
        'method' => 'post'
    )) !!}

        @include('admin/promotion/form', ['submitButton' => 'Create'])

    {!! Form::close() !!}
@endsection
