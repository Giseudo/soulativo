<script type="text/template" id="page-list-template">
    <% for(var page in result) { %>
        <tr>
            <a href="{{ url('admin/seo/page/edit') }}/<%= result[page].id %>"><%= result[page].title %></a>
        </tr>
    <% } %>
</script>
