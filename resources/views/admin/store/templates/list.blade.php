<script type="text/template" id="store-list-template">
    <% for(var store in result) { %>
        <tr>
            <td>
                <input type="checkbox" name="checkbox[]" class="m-r-1" value="<%= result[store].id %>">
                <a href="{{ url('admin/store/edit') }}/<%= result[store].id %>"><%= result[store].name %></a></td>
            <td><%= result[store].document %></td>
            <td><small><%= result[store].date %></small></td>
        </tr>
    <% } %>
</script>
