<fieldset id="package">
    <legend>@lang('model.booking.singular') (#{{@$booking->id}})</legend>
    <div class="row">
        <div class="form-group col-xs-12 col-xl-6">
            <label class="text-help" for="booking-date">@lang('model.booking.date')</label>
            {!! Form::text('booking[date]', null, ['id' => 'booking-date', 'class' => 'form-control']) !!}
        </div>
        <div class="form-group col-xs-12 col-md-6 col-xl-3">
            <label class="text-help" for="booking-adults">@lang('model.booking.adults')</label>
            {!! Form::number('booking[adults]', null, ['id' => 'booking-adults', 'class' => 'form-control']) !!}
        </div>
        <div class="form-group col-xs-12 col-md-6 col-xl-3">
            <label class="text-help" for="booking-childs">@lang('model.booking.childs')</label>
            {!! Form::number('booking[childs]', null, ['id' => 'booking-childs', 'class' => 'form-control']) !!}
        </div>
    </div>
    <div class="row">
        <div class="form-group col-xs-12 col-xl-6">
            <label class="text-help" for="booking-departure_location">@lang('model.booking.departure_location')</label>
            {!! Form::text('booking[departure_location]', null, ['id' => 'booking-departure_location', 'class' => 'form-control']) !!}
        </div>
        <div class="form-group col-xs-12 col-xl-6">
            <label class="text-help" for="booking-departure_hour">@lang('model.booking.departure_hour')</label>
            {!! Form::text('booking[departure_hour]', null, ['id' => 'booking-departure_hour', 'class' => 'form-control']) !!}
        </div>
    </div>
</fieldset>
