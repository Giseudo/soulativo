<aside class="card">
    <header class="card-header">
        Details
    </header>
    <div class="card-block">
        <div class="form-group">
            <label class="text-help" for="booking-package">@lang('model.package.singular')</label>
            <select id="booking-package" name="booking[package_id]" class="form-control c-select">
                <option value="">– Select the package –</option>
                @foreach($packages as $package)
                    <option value="{{$package->id}}" @if(@$booking->package_id == $package->id) selected="selected" @endif>{{$package->product->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label class="text-help" for="booking-status">@lang('model.booking.status')</label>
            {!! Form::select('booking[status]', [1 => "Pending", 2 => "Confirmed", 3 => "Rejected"], null, ['id' => 'booking-status', 'class' => 'form-control c-select']) !!}
        </div>
    </div>
</aside>
