<script type="text/template" id="booking-list-template">
    <% for(var booking in result) { %>
        <tr>
            <td>
                <input type="checkbox" name="checkbox[]" class="m-r-1" value="<%= result[booking].id %>">
                <a href="{{ url('admin/booking/edit') }}/<%= result[booking].id %>">#<%= result[booking].id %></a>
            </td>
            <td><a href="{{ url('admin/customer/edit') }}/<%= result[booking].customer_id %>"><%= result[booking].user_name %></a></td>
            <td><a href="{{ url('admin/package/edit') }}/<%= result[booking].package_id %>"><%= result[booking].product_name %></a></td>
            <td>
                <% if(result[booking].status == 2){ %>
                    <span class="label label-success">Confirmed</span>
                <% }else if(result[booking].status == 3){ %>
                    <span class="label label-info">Rejected</span>
                <% }else{ %>
                    <span class="label <%= result[booking].pendingClass %>">Pending</span>
                <% } %>
            </td>
        </tr>
    <% } %>
</script>
