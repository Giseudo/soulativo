<div class="card" data-id="@if(!empty($package)){{$package->id}}@endif" @if(@$position) data-position="{{ $position }}" @endif>
     <header style="background-image: @if(isset($package->thumbnail_id)) url(/images/{{$package->thumbnail->filename}}) @else url(/images/delete/featured-01.jpg) @endif">
        @if(@$replacing)
            <button class="fa fa-check"></button>
        @else
            <button class="fa fa-pencil" data-toggle="modal" data-target="#select-package"></button>
        @endif
    </header>
    <div class="card-block">
        <h4 class="card-title">@if(isset($package->product->name)) {{$package->product->name}} @else Card title @endif</h4>
    </div>
</div>
