@extends('admin.layouts.app')

@section('content')
    <div class="col-xs-12 -list">
        <nav class="navbar navbar-light bg-faded m-b-1" style="background-color: #e3f2fd;">
            <a class="navbar-brand">@lang('model.package.plural') <a href="{{ url('/admin/package/create') }}" class="btn btn-primary">Add new @lang('model.package.singular')</a></a>
            @include('admin.search.form', ['action' => '/package/search'])
        </nav>

        <ul class="nav nav-tabs" id="nav-tab">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#all" role="tab" aria-controls="all">All ({{count($packages)}})</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#trash" role="tab" aria-controls="trasg">Trash ({{count($deleted_packages)}})</a>
            </li>
        </ul>


        <div class="tab-content">
            {!! Form::open(array('url' => '', 'method' => 'POST', 'id' => 'all', 'class' => 'tab-pane active')) !!}
                <header class="m-t-1 m-b-1">
                    <div class="btn-group m-r-1">
                        <button type="button" class="btn btn-primary-outline btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            – Actions in mass –
                        </button>
                        <div class="dropdown-menu">
                            <button data-action="/admin/package/destroy" data-target="#trash" class="dropdown-item">Move to Trash</button>
                        </div>
                    </div>
                </header>
                <table class="table table-striped">
                    <thead class="">
                        <tr>
                            <th>
                                <input type="checkbox" class="m-r-1" value=""> @lang('model.product.name')
                            </th>
                            <th>@lang('model.package.category')</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                         @foreach ($packages as $package)
                        <tr>
                            <td>
                                <input type="checkbox" name="checkbox[]" class="m-r-1" value="{{$package->id}}">
                                <a href="{{ url('admin/package/edit/' . $package->id) }}">{{$package->product->name}}</a></td>
                            <td>
                                @foreach ($package->product->categories as $cat)
                                    <a href="/admin/packages/category/edit/{{$cat->id}}" title="{{$cat->name}}">{{$cat->name}}</a>
                                @endforeach
                            </td>
                            <td><button data-id="{{$package->id}}" class="-featured fa @if($package->featured) -active fa-star @else fa-star-o @endif"></button></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                <nav>
                    <ul class="pager">
                        <li><a href="/package" class="paginate" data-page="1" data-limit="{{$perPage}}">Load more</a></li>
                    </ul>
                </nav>
            {!! Form::close() !!}

            {!! Form::open(array('url' => '', 'method' => 'POST', 'id' => 'trash', 'class' => 'tab-pane')) !!}
            <input type="hidden" name="trash_delete" id="trash_delete" value="1">
                <header class="m-t-1 m-b-1">
                    <div class="btn-group m-r-1">
                        <button type="button" class="btn btn-primary-outline btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            – Actions in mass –
                        </button>
                        <div class="dropdown-menu">
                            <button data-action="/admin/package/destroy" class="dropdown-item">Delete permanently</button>
                            <button data-action="/admin/package/restore" data-target="#all" class="dropdown-item">Restore</button>
                        </div>
                    </div>
                </header>

                <table class="table table-striped @if (count($deleted_packages) === 0) hidden-xs-up @endif">
                    <thead class="">
                        <tr>
                            <th>
                                <input type="checkbox" class="m-r-1" value=""> @lang('model.product.name')
                            </th>
                            <th>@lang('model.package.category')</th>
                            <th>@lang('model.product.price')</th>
                            <th>Created at</th>
                        </tr>
                    </thead>
                    <tbody>
                         @foreach ($deleted_packages as $package)
                        <tr>
                            <td>
                                <input type="checkbox" name="checkbox[]" class="m-r-1" value="{{$package->id}}">
                                <a href="{{ url('admin/package/edit/' . $package->id) }}">{{$package->product->name}}</a>
                            </td>
                            <td>
                                @foreach ($package->product->categories as $cat)
                                    <a href="/admin/packages/category/edit/{{$cat->id}}" title="{{$cat->name}}">{{$cat->name}}</a>
                                @endforeach
                            </td>
                            <td>{{$package->product->price}}</td>
                            <td><small>{{ isset($package->created_at) ? $package->created_at->format(trans('model.dateformat.full')) : ' - ' }}</small></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                @if (count($deleted_packages) === 0)
                    <div class="alert alert-info" id="empty-trash" role="alert">
                        <strong>Empty trash!</strong> There' nothing inside the trash.
                    </div>
                @endif
            {!! Form::close() !!}
        </div>
    </div>

    <!-- Inclues template for pagination -->
    @include('admin/package/templates/list')
@endsection
