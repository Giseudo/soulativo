@extends('admin.layouts.app')

@section('content')
    {!! Form::open(array(
        'url' => 'admin/package/create',
        'id' => 'form-package',
        'method' => 'post'
    )) !!}

        @include('admin/package/form', ['submitButton' => 'Create'])

    {!! Form::close() !!}
@endsection
