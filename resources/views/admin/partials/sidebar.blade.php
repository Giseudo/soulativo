<div class="list-group m-b-1">
    <a href="{{ url('admin/dashboard')  }}" class="list-group-item active">
        <span class="fa fa-home"></span>
        Dashboard
    </a>
    <a href="{{ url('admin/users')  }}" class="list-group-item">
        <span class="fa fa-users"></span>
        @lang('model.user.plural')
    </a>
    <a href="{{ url('admin/pages')  }}" class="list-group-item">
        <span class="fa fa-file-text"></span>
        @lang('model.page.plural')
    </a>
    <a href="{{ url('admin/seo')  }}" class="list-group-item">
        <span class="fa fa-eye"></span>
        SEO
    </a>
</div>

<div class="list-group m-b-1">
    <a href="{{ url('admin/customers')  }}" class="list-group-item">
        <span class="fa fa-child"></span>
        @lang('model.customer.plural')
    </a>
    <a href="{{ url('admin/bookings')  }}" class="list-group-item">
        <span class="fa fa-ticket"></span>
        @if(\App\Models\Booking::where('status', '1')->count() > 0)
            <span class="label label-warning label-pill pull-xs-right">{{ \App\Models\Booking::where('status', '1')->count() }}</span>
        @endif
        @lang('model.booking.plural')
    </a>
    <a href="{{ url('admin/orders')  }}" class="list-group-item">
        <span class="fa fa-shopping-cart"></span>
        @lang('model.order.plural')
    </a>
</div>

<div class="list-group m-b-1">
    <a href="{{ url('admin/packages')  }}" class="list-group-item">
        <span class="fa fa-plane"></span>
        @lang('model.package.plural')
    </a>
    <a href="{{ url('admin/packages/categories')  }}" class="list-group-item">
        <span class="fa fa-folder-open"></span>
        @lang('model.category.plural')
    </a>
    <a href="{{ url('admin/promotions')  }}" class="list-group-item">
        <span class="fa fa-bullhorn"></span>
        @lang('model.promotion.plural')
    </a>
    <a href="{{ url('admin/blockouts') }}" class="list-group-item">
        <span class="fa fa-calendar-times-o"></span>
        @lang('model.blockout.plural')
    </a>
    <a href="{{ url('admin/coupons')  }}" class="list-group-item">
        <span class="fa fa-ticket"></span>
        @lang('model.coupon.plural')
    </a>
</div>

<div class="list-group m-b-1">
    <a href="{{ url('admin/posts')  }}" class="list-group-item">
        <span class="fa fa-commenting"></span>
        Posts
    </a>
    <a href="{{ url('admin/posts/categories')  }}" class="list-group-item">
        <span class="fa fa-folder-open"></span>
        @lang('model.category.plural')
    </a>
</div>

<div class="list-group m-b-1">
    <a href="{{ url('admin/partners')  }}" class="list-group-item">
        <span class="fa fa-star"></span>
        @lang('model.partner.plural')
    </a>
    <a href="{{ url('admin/stores')  }}" class="list-group-item">
        <span class="fa fa-building"></span>
        @lang('model.store.plural')
    </a>
</div>
