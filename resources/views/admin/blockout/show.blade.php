@extends('admin.layouts.app')

@section('content')
    {!! Form::model($package, array(
        'method' => 'post',
        'action' => ['Admin\BlockoutController@update', $package->id],
        'id' => 'form-blockout'
    )) !!}

        @include('admin.blockout.form', ['submitButton' => 'Update'])

    {!! Form::close() !!}
@endsection
