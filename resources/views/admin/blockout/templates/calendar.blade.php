<script type="text/template" id="blockout-calendar-template">
    <div class="controls">
    	<div class="clndr-previous-button">‹</div>
    	<div class="month"><%= month %></div>
    	<div class="clndr-next-button">›</div>
    </div>
    <div class="days-container">
    	<div class="days">
    		<div class="headers">
    			<% _.each(daysOfTheWeek, function(day) { %>
    				<div class="day-header"><%= day %></div>
    			<% }); %>
    		</div>

    		<% _.each(days, function(day) { %>
    			<div class="<%= day.classes %>" id="<%= day.id %>">
    				<%= day.day %>
    			</div>
    		<% }); %>
    	</div>
    </div>
</script>
