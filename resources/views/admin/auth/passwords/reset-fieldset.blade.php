<fieldset>
    <legend>@lang('model.password.reset')</legend>
    <div class="row">
        <div class="form-group col-xs-12 col-xl-6">
            <label class="text-help" for="">@lang('model.password.new')</label>
            <input name="user[password]" type="password" class="form-control" id="password" placeholder="">
        </div>
        <div class="form-group col-xs-12 col-xl-6">
            <label class="text-help" for="">@lang('model.password.confirm')</label>
            <input name="user[password_confirmation]" type="password" class="form-control" id="password_confirmation" placeholder="">
        </div>
    </div>
</fieldset>
