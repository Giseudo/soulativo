<script type="text/template" id="customer-list-template">
    <% for(var customer in result) { %>
        <tr>
            <td>
                <input type="checkbox" name="checkbox[]" class="m-r-1" value="<%= result[customer].id %>">
                <a href="{{ url('admin/customer/edit') }}/<%= result[customer].id %>"><%= result[customer].name %></a></td>
            <td><%= result[customer].email %></td>
            <td><small><%= result[customer].l_login %></small></td>
        </tr>
    <% } %>
</script>
