@extends('admin.layouts.app')

@section('content')
    <div class="col-xs-12">
        <nav class="navbar navbar-light bg-faded m-b-1" style="background-color: #e3f2fd;">
            <a class="navbar-brand">@lang('model.category.plural') <a href="{{ url('/admin/' . $root->slug . '/category/create') }}" class="btn btn-primary">Add new @lang('model.category.singular')</a></a>
            @include('admin.search.form')
        </nav>

        <ul class="nav nav-tabs" id="nav-tab">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#all" role="tab" aria-controls="all">All ({{count($categories)}})</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#trash" role="tab" aria-controls="trasg">Trash ({{count($deleted_categories)}})</a>
            </li>
        </ul>

        <div class="tab-content">
            {!! Form::open(array('url' => '', 'method' => 'POST', 'id' => 'all', 'class' => 'tab-pane active')) !!}
                <header class="m-t-1 m-b-1">
                    <div class="btn-group m-r-1">
                        <button type="button" class="btn btn-primary-outline btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            – Actions in mass –
                        </button>
                        <div class="dropdown-menu">
                            <button data-action="/admin/{{$root->slug}}/category/destroy" data-target="#trash" class="dropdown-item">Move to Trash</button>
                        </div>
                    </div>
                </header>
                <table class="table table-striped">
                    <thead class="">
                        <tr>
                            <th>
                                <input type="checkbox" class="m-r-1" value=""> @lang('model.category.name')
                            </th>
                            <th>Created at</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($categories as $category)
                            <tr>
                                <td>
                                    <input type="checkbox" name="checkbox[]" class="m-r-1" value="{{$category->id}}">
                                    <a href="{{ url('admin/' . $root->slug . '/category/edit/' . $category->id) }}">
                                        @for ($i = 0; $i < $category->getLevel() - 1; $i++)–@endfor
                                        {{$category->name}}
                                    </a>
                                </td>
                                <td><small>{{ (empty($category->created_at)) ? '––' : $category->created_at->format(trans('model.dateformat.full')) }}</small></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            {!! Form::close() !!}

            {!! Form::open(array('url' => '', 'method' => 'POST', 'id' => 'trash', 'class' => 'tab-pane')) !!}
                <header class="m-t-1 m-b-1">
                    <div class="btn-group m-r-1">
                        <button type="button" class="btn btn-primary-outline btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            – Actions in mass –
                        </button>
                        <div class="dropdown-menu">
                            <button data-action="/admin/{{$root->slug}}/category/destroy" class="dropdown-item">Delete permanently</button>
                            <button data-action="/admin/{{$root->slug}}/category/restore" data-target="#all" class="dropdown-item">Restore</button>
                        </div>
                    </div>
                </header>

                <table class="table table-striped @if (count($deleted_categories) === 0) hidden-xs-up @endif">
                    <thead class="">
                        <tr>
                            <th>
                                <input type="checkbox" class="m-r-1" value=""> @lang('model.category.name')
                            </th>
                            <th>Created at</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($deleted_categories as $category)
                            <tr>
                                <td>
                                    <input type="checkbox" name="checkbox[]" class="m-r-1" value="{{$category->id}}">
                                    <a href="{{ url('admin/' . $root->slug . '/category/edit/' . $category->id) }}">
                                        @for ($i = 0; $i < $category->getLevel() - 1; $i++)–@endfor
                                        {{$category->name}}
                                    </a>
                                </td>
                                <td><small>{{ (empty($category->created_at)) ? '––' : $category->created_at->format(trans('model.dateformat.full')) }}</small></td>
                            </tr>
                       @endforeach
                    </tbody>
                </table>

                @if (count($deleted_categories) === 0)
                    <div class="alert alert-info" id="empty-trash" role="alert">
                        <strong>Empty trash!</strong> There' nothing inside the trash.
                    </div>
                @endif
            {!! Form::close() !!}
        </div>
    </div>

    <!-- Inclues template for pagination -->
    @include('admin/category/templates/list')
@endsection
