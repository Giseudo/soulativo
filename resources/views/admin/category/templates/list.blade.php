<script type="text/template" id="category-list-template">
    <% for(var category in result) { %>
        <tr>
            <td>
                <input type="checkbox" name="checkbox[]" class="m-r-1" value="<%= result[category].id}}">
                <a href="admin/<%= result.root.slug %>/category/edit/<%= result[category].id %>">
                    <% for(i = 0; i < result[category].level - 1; i++) { %>-<% } %>
                    <%= result[category].name %>
                </a>
            </td>
            <td><small><%= result[category].created_at %></small></td>
        </tr>
    <% } %>
</script>
