<fieldset id="package">
    <legend>@lang('model.category.singular')</legend>
    <div class="row">
        <div class="form-group col-xs-12 col-md-6">
            <label class="text-help" for="category-name">@lang('model.category.name')</label>
            {!! Form::text('category[name]', null, ['id' => 'category-name', 'class' => 'form-control']) !!}
        </div>
        <div class="form-group col-xs-12 col-md-6">
            <label class="text-help" for="category-slug">@lang('model.category.slug')</label>
            {!! Form::text('category[slug]', null, ['id' => 'category-slug', 'class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        <label for="category[parent_id]">@lang('model.category.parent')</label>
        <select name="category[parent_id]" class="form-control c-select">
            <option value="{{ $root->id }}" selected>Parent category</option>
            @foreach($categories as $id => $child)
                @if(@!empty($category))
                    @if($child->isSelfOrDescendantOf($category))
                        @continue
                    @endif
                @endif
                <option value="{{$child->id}}" @if(@$category->parent_id == $child->id) selected="selected" @endif>
                    @for ($i = 0; $i < $child->getLevel(); $i++)–@endfor
                    {{$child->name}}
                </option>
            @endforeach
        </select>
    </div>
</fieldset>
