<div class="col-xs-12 col-xl-8">
    @include('admin.partials.flash')
    @include('admin.category.fieldset')
</div>

<aside class="col-xs-12 col-xl-4">
    @include('admin.partials.form.publish-fieldset')
    @include('admin.gallery.thumbnail', [
        'id' => 'category-thumbnail_modal',
        'media' => @$category->thumbnail,
        'name' => 'category[thumbnail_id]'
    ])
</aside>
