@extends('admin.layouts.app')

@section('content')
    {!! Form::model($category, array(
        'method' => 'post',
        'action' => ['Admin\CategoryController@update', $root->slug, $category->id],
        'id' => 'form-category'
    )) !!}

        @include('admin.category.form', ['submitButton' => 'Update'])

    {!! Form::close() !!}
@endsection
