@extends('admin.layouts.app')

@section('content')
    {!! Form::open(array(
        'url' => 'admin/' . $root->slug . '/category/create',
        'id' => 'form-category',
        'method' => 'post'
    )) !!}

        @include('admin/category/form', ['submitButton' => 'Create'])

    {!! Form::close() !!}
@endsection
