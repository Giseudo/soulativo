@extends('admin.layouts.app')

@section('content')
    <div class="col-xs-12 col-md-6">
        <div class="jumbotron">
            <h1 class="display-3">Hello, world!</h1>
            <p class="lead">Precisamos criar alguns elementos para compor nossa Dashboard.</p>
            <hr class="m-y-2">
            <p>Futuramente aqui será possível configurar opções do tipo: produtos listados na página inicial, destaques, etc.</p>
        </div>
    </div>
    <div class="featured col-xs-12 col-md-6">
        <div class="card-deck-wrapper">
            <div class="card-deck">
                @include('admin.package.loop', ['position' => 1, 'package' => $package1])
                @include('admin.package.loop', ['position' => 2, 'package' => $package2])
            </div>
        </div>
        @include('admin.package.loop', ['position' => 3, 'package' => $package3])
    </div>

    <div id="select-package" class="modal fade">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Select a package:</h4>
                </div>
                <div class="modal-body">
                    <div class="replace card-columns">
                        @foreach($packages as $package)
                            @include('admin.package.loop', ['replacing' => true, 'package' => $package])
                        @endforeach
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    @include('admin.package.templates.loop')
@endsection
