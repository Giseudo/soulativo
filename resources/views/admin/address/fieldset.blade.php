<fieldset id="address">
    <legend>@lang('model.address.singular')</legend>
    <div class="row">
        <div class="form-group col-xs-12 col-xl-6">
            <label class="text-help" for="">@lang('model.country.singular')</label>
            {!! Form::select('address[country_id]', $countries, null, ['id' => 'address-country', 'class' => 'form-control c-select']) !!}
        </div>
        <div class="form-group col-xs-12 col-xl-6">
            <label class="text-help" for="">@lang('model.address.state')</label>
            {!! Form::text('address[state]', null, ['id' => 'address-state', 'class' => 'form-control']) !!}
        </div>
    </div>
    <div class="row">
        <div class="form-group col-xs-12 col-xl-6">
            <label class="text-help" for="">@lang('model.address.city')</label>
            {!! Form::text('address[city]', null, ['id' => 'address-city', 'class' => 'form-control']) !!}
        </div>
        <div class="form-group col-xs-12 col-xl-6">
            <label class="text-help" for="">@lang('model.address.zipcode')</label>
            {!! Form::text('address[zipcode]', null, ['id' => 'address-zipcode', 'class' => 'form-control']) !!}
        </div>
    </div>
    <div class="row">
        <div class="form-group col-xs-8 col-xl-10">
            <label class="text-help" for="">@lang('model.address.street')</label>
            {!! Form::text('address[street]', null, ['id' => 'address-street', 'class' => 'form-control']) !!}
        </div>
        <div class="form-group col-xs-4 col-xl-2">
            <label class="text-help" for="">@lang('model.address.number')</label>
            {!! Form::text('address[number]', null, ['id' => 'address-number', 'class' => 'form-control']) !!}
        </div>
    </div>
    <div class="row">
        <div class="form-group col-xs-12 col-xl-6">
            <label class="text-help" for="">@lang('model.address.neighbourhood')</label>
            {!! Form::text('address[neighbourhood]', null, ['id' => 'address-neighbourhood', 'class' => 'form-control']) !!}
        </div>
        <div class="form-group col-xs-12 col-xl-6">
            <label class="text-help" for="">@lang('model.address.complement')</label>
            {!! Form::text('address[complement]', null, ['id' => 'address-complement', 'class' => 'form-control']) !!}
        </div>
    </div>
</fieldset>
