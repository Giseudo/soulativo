<form action="@if(@!empty($action)){{ $action }}@endif" id="search" class="form-inline pull-xs-right hidden-sm-down">
    <input id="autocomplete" class="form-control" type="text" @if(@empty($action)) disabled="disabled" @endif placeholder="Search">
    <button class="btn btn-primary-outline" @if(@empty($action)) disabled="disabled" @endif type="submit">Search</button>
</form>
