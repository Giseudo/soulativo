@extends('admin.layouts.app')

@section('content')
    {!! Form::model($partner, array(
        'method' => 'post',
        'id' => 'form-partner',
        'action' => ['Admin\PartnerController@update', $partner->id]
    )) !!}

        @include('admin.partner.form', ['submitButton' => 'Update', 'partner' => $partner])

    {!! Form::close() !!}
@endsection