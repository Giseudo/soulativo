<script type="text/template" id="partner-list-template">
    <% for(var partner in result) { %>
        <tr>
            <td>
                <input type="checkbox" name="checkbox[]" class="m-r-1" value="<%= result[partner].id %>">
                <a href="{{ url('admin/partner/edit') }}/<%= result[partner].id %>"><%= result[partner].name %></a></td>
            <td><%= result[partner].email %></td>
            <td><small><%= result[partner].l_login %></small></td>
        </tr>
    <% } %>
</script>
