@extends('layouts.app', ['submenu' => 'not-fixed'])

@section('content')

{!! Form::open(array('url' => '/checkout/store', 'class' => 'checkout -form-default')) !!}
    <header class="-steps">
        <h3>@lang('front.checkout.confirmation.step')</h3>
        <h1>@lang('front.checkout.confirmation.title')</h1>
        <p>@lang('front.checkout.confirmation.description', ['link' => '/customer/profile'])</p>
        <a href="/">« @lang('front.checkout.confirmation.back')</a>
    </header>
    <div class="container">
        <div class="orders -list col-xs-12 col-md-8 col-md-offset-2">
            @include('orders.loop', ['order' => $invoice->order])
        </div>

        <div class="package -list">
            <header class="col-xs-12">
                <h2>@lang('front.package.related')</h2>
            </header>
            <?php $cont = 1; ?>

            @foreach($packages as $package)
                @if($cont == 3)
                    @include('package.loop', ['size' => 'big'])
                    <?php $cont = 1; ?>
                @elseif($package == end($packages) && $cont != 1)
                    @include('package.loop', ['size' => 'big'])
                @else
                    @include('package.loop', ['size' => 'medium'])
                    <?php $cont++; ?>
                @endif
            @endforeach
        </div>
    </div>
{!! Form::close() !!}

@endsection
