@extends('layouts.app', ['submenu' => 'not-fixed'])

@section('content')

{!! Form::open(array('url' => '/checkout/store/' . $order->id, 'method' => 'post', 'class' => 'checkout -form-default')) !!}
    <header class="-steps">
        <h3>@lang('front.checkout.payment.step')</h3>
        <h1>@lang('front.checkout.payment.title')</h1>
        <p>@lang('front.checkout.payment.description')</p>
        <a href="/checkout/customer">« @lang('front.checkout.payment.back')</a>
    </header>
    <div class="container payment">
        <aside class="resume col-xs-12 col-md-5">
            <div class="wrapper">
                <h2>@lang('front.checkout.payment.summary')</h2>
                @foreach($order->bookings as $booking)
                    <article>
                        <h4>{{$booking->package->product->name}}</h4>
                        <ul>
                            <li>{{$booking->date}} (@lang('front.order.pickup') {{$booking->departure_hour}})</li>
                            <li>{{$booking->adults}} @lang('front.booking.adult.plural') ({{config('app.currency.prefix') . $booking->getDiscountedPrice()}} @lang('front.booking.each'))</li>
                            @if($booking->childs > 0)
                                <li>{{$booking->childs}} @lang('front.booking.child.plural') ({{config('app.currency.prefix') . $booking->getChildPrice()}} @lang('front.booking.each'))</li>
                            @endif
                        </ul>
                        <span class="price">{{config('app.currency.prefix')}}{{ $booking->getTotal() }}</span>
                    </article>
                @endforeach
                <footer>
                    <div class="total">
                        <span>Total</span>
                        <span>{{config('app.currency.prefix') . ($order->getTotal())}}</span>
                    </div>
                </footer>
            </div>
        </aside>

        <div class="method col-xs-12 col-md-6 col-md-offset-1">
            <h2>@lang('front.checkout.payment.method.title')</h2>
            @include('partials.alerts')
            <fieldset class="payment">
                <div class="form-group">
                    <label>@lang('front.payment.title')</label>

                    @if(Auth::user()->customer->address->country->name == 'Brazil')
                        <!--label class="c-input c-radio">
                          <input type="radio" name="payment[method]" id="creditcard" value="creditcard">
                          <span class="indicator"></span>
                          @lang('front.checkout.payment.method.card')
                        </label-->
                        <label class="c-input c-radio">
                            <input type="radio" name="payment[method]" id="pagseguro" value="pagseguro">
                            <span class="indicator"></span>
                            @lang('front.checkout.payment.method.pagseguro')
                            <img src="/images/pag-seguro-logo@2x.png" alt="PagSeguro">
                        </label>
                        <label class="c-input c-radio">
                            <input type="radio" name="payment[method]" id="mercadopago" value="mercadopago">
                            <span class="indicator"></span>
                            @lang('front.checkout.payment.method.mercadopago')
                            <img src="/images/mercado-pago-logo@2x.png" alt="MercadoPago">
                        </label>
                    @endif
                    <label class="c-input c-radio">
                        <input type="radio" name="payment[method]" id="paypal" value="paypal">
                        <span class="indicator"></span>
                        @lang('front.checkout.payment.method.paypal')
                        <img src="/images/pay-pal-logo@2x.png" alt="PayPal">
                    </label>
                </div>
            </fieldset>
            <fieldset class="creditcard">
                <div class="form-group">
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <label for="card-number" class="text-help">Card number</label>
                            {!! Form::text('card-number', null, ['class' => '-dashed form-control', 'data-stripe' => 'number']) !!}
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <label for="card-number" class="text-help">CVC</label>
                            {!! Form::text('card-cvc', null, ['class' => '-dashed form-control', 'data-stripe' => 'cvc', 'maxlength' => 4]) !!}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="card-holder" class="text-help">Card holder name</label>
                    {!! Form::text('card-holder', null, ['class' => '-dashed form-control card-holder', 'data-stripe' => 'card_holder']) !!}
                </div>
                <div class="form-group">
                    <label for="card-holder" class="text-help">Expiration</label>
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            {{ Form::selectMonth('card-expiry-month', null, ['class' => '-dashed c-select card-expiry-month', 'data-stripe' => 'exp_month']) }}
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <select name="card-expiry-year" class="-dashed c-select card-expiry-year" data-stripe="exp_year">
                                @for($i = \Carbon\Carbon::now()->year; $i <= \Carbon\Carbon::now()->addYears(10)->year; $i++)
                                    <option value="{{$i}}">{{$i}}</option>
                                @endfor
                            </select>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>

        <footer class="col-xs-12 col-md-4 col-md-offset-8">
            <input type="submit" name="name" class="-block -success" value="@lang('front.checkout.payment.submit')">
            {!! Form::hidden('name', Auth::user()->name, ['data-stripe' => 'name']) !!}
            {!! Form::hidden('country', Auth::user()->customer->address->country->name, ['data-stripe' => 'address_country']) !!}
            {!! Form::hidden('state', Auth::user()->customer->address->state, ['data-stripe' => 'address_state']) !!}
            {!! Form::hidden('zipcode', Auth::user()->customer->address->zipcode, ['data-stripe' => 'address_zip']) !!}
            {!! Form::hidden('city', Auth::user()->customer->address->city, ['data-stripe' => 'address_city']) !!}
            {!! Form::hidden('street', Auth::user()->customer->address->street . ', ' . Auth::user()->customer->address->number, ['data-stripe' => 'address_line1']) !!}
            {!! Form::hidden('complement', Auth::user()->customer->address->complement, ['data-stripe' => 'address_line2']) !!}
        </footer>
    </div>
{!! Form::close() !!}

<script type="text/javascript" src="https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js"></script>

@endsection
