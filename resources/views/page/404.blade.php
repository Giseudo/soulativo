@extends('layouts.app')

@section('content')
    <section class="not-found">
        <div class="container">
            <div class="col-xs-12 col-lg-4">
                <h1>404</h1>
                <p>Parece que você está perdido. Que tal <a href="/packages">explorar</a> os passeios e as aventuras que só a SoulAtivo oferece?</p>
            </div>
        </div>
    </section>
@endsection
