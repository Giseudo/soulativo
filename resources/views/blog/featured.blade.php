 <article class="-featured" style="@if(!empty($featured->thumbnail_id)) background-image:url('/images/{{$featured->thumbnail->filename}}') @endif">
    <div class="wrapper col-xs-12 col-md-6">
        <span class="cat">
            @foreach($featured->categories as $cat)
                <a href="/posts/{{$cat->slug}}">{{$cat->name}}</a>
            @endforeach
        </span>
        <h2><a href="/post/{{$featured->slug}}">{{$featured->title}}</a></h2>

        <time>{{ isset($featured->created_at) ? $featured->created_at->format(trans('model.dateformat.full')) : ' - ' }}</time>
    </div>
</article>
