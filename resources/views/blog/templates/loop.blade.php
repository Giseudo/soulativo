<script type="text/template" id="post-loop-template">
    <article class="-loop">
        <div class="row">
            <figure class="col-xs-4 col-md-3">
                <% if(thumbnail != null) { %>
                    <img src="/images/<%= thumbnail.filename %>?w=150&h=150&fit=crop-50-50" alt="">
                <% } %>
            </figure>
            <div class="col-xs-12 col-md-9">
                <% if(categories != null) { %>
                    <span class="cat">                        
                        <% for(var category in categories) { %>
                            <a href="/posts/<%= categories[category].slug %>"><%= categories[category].name %></a>
                        <% } %>
                    </span>
                <% } %>
                <h3><a href="/post/<%= slug %>"><%= title %></a></h3>
                <time><%= created_at %></time>
                <p><%= excerpt %></p>
            </div>
        </div>
    </article>
</script>
