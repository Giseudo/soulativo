@extends('layouts.app')

@section('content')

    <header class="-post-head" style="@if(!empty($post->thumbnail)) background-image:url('/images/{{$post->thumbnail->filename}}') @endif">
        <div class="container">
            <div class="wrapper col-xs-12 col-md-7">
                <span class="cat">
                    @foreach($post->categories as $cat)
                        <a href="/posts/{{$cat->slug}}">{{$cat->name}}</a>
                    @endforeach
                </span>
                <h1>{{$post->title}}</h1>
                <time>{{ isset($post->created_at) ? $post->created_at->format(trans('model.dateformat.full')) : ' - ' }}</time>
            </div>
        </div>
    </header>
    <div class="blog -read container">
        <div class="-content col-xs-12 col-md-7">
            {!! $post->content !!}
            <nav class="share row">
            <div class="col-xs-12 col-md-6">
                <a href="https://twitter.com/share?url={{$currentUrl}}&via=SoulAtivo&text={{$post->title}}" target="blank" class="twitter">
                    <span class="icon icon-twitter-alt"></span>
                    @lang('front.social.twitter.share')
                </a>
            </div>
            <div class="col-xs-12 col-md-6">
                <a href="https://www.facebook.com/sharer/sharer.php?u={{$currentUrl}}" target="blank" class="facebook">
                    <span class="icon icon-facebook-alt"></span>
                    @lang('front.social.facebook.share')
                </a>
            </div>
        </nav>
        </div>
        <aside class="-sidebar col-xs-12 col-md-3 col-md-offset-2">
            @include('blog.aside')
            @include('page.aside', ['categoriesPost' => $post->categories])
            @include('newsletter.aside')
        </aside>
        <div class="-list col-xs-12">
            <h3>@lang('front.blog.latest') <a href="#">@lang('front.blog.more') »</a></h3>
            <div class="row">
                @foreach($recentPosts as $recent)
                    @include('blog.tiny', ['class' => 'col-xs-12 col-md-4'])
                @endforeach
            </div>
        </div>
    </div>
@endsection
