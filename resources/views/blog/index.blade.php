@extends('layouts.app')

@section('content')

    <header class="-page-head"  @if(!empty($slug)) style="@if(!empty($category->thumbnail->filename)) background-image: url('/images/{{$category->thumbnail->filename}}') @endif" @else style="background-image: url(/images/delete/bg-top-01.jpg)" @endif>
        <div class="container">
            <div class="wrapper">
                @if(isset($slug))
                    <nav class="category">
                        <a href="/blog">Blog » </a>
                    </nav>
                    <h1>{{$category->name}}</h1>
                @else
                    <h1>Blog</h1>
                @endif
            </div>
        </div>
    </header>
    <div class="blog container">
        @if(!empty($featured))
            <div class="-list col-xs-12">
                @include('blog.featured')
            </div>
        @endif
        <div class="-list col-xs-12 col-md-7">
            @if(count($posts) > 0)
                @foreach($posts as $post)
                    @include('blog.loop')
                @endforeach
                <footer class="col-md-offset-3 col-md-9">
                    <button class="-load" @if(!empty($slug)) data-url="post/{{$slug}}/paginate" @else data-url="post/paginate" @endif>Carregar posts anteriores »</button>
                </footer>
            @else
                <div class="alert alert-info">
                    <p>Não encontramos posts relacionados</p>
                </div>
            @endif
        </div>
        <aside class="-sidebar col-xs-12 col-md-3 col-md-offset-2">
            @include('blog.aside')
            @include('page.aside')
            @include('newsletter.aside')
        </aside>
    </div>

@endsection
