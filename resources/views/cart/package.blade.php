<article class="package" data-product-id="{{$package->product->id}}">
    <div class="container">
        <figure class="col-xs-12 col-md-3">
            <span>{{$cont}}</span>
            @if(!empty($package->thumbnail_id))
                <img src="/images/{{$package->thumbnail->filename}}?w=250&h=250&fit=crop-50-50" alt="">
            @endif
        </figure>

        <div class="col-xs-12 col-md-6">
            <h2><a href="#">{{$package->product->name}}</a></h2>
            <p>{{$package->product->excerpt}}</p>
            <button class="remove" rel="{{$id}}">@lang('front.checkout.cart.remove')</button>
            <fieldset>
                <div class="form-group">
                    <label class="text-help" for="">@lang('front.booking.when')</label>
                    <div class="row">
                        <div class="col-xs-12">
                            <input type="text" name="booking[{{$id}}][date]" value="{{$item->date}}" class="date form-control -dashed">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="text-help" for="">@lang('front.booking.many')</label>
                    <div class="row">
                        <div class="col-xs-12 col-lg-6">
                            <select name="booking[{{$id}}][adults]" class="adults c-select -dashed">
                                @for ($i = 1; $i < 10; $i++)
                                    <option
                                        @if($item->adults == $i)
                                             selected="selected"
                                        @endif
                                        value="{{$i}}"
                                    >
                                        {{$i}} @lang('front.booking.adult.singular') ({{config('app.currency.prefix') . $package->product->getDiscountedPrice() * $i}})
                                    </option>
                                @endfor
                            </select>
                        </div>
                        <div class="col-xs-12 col-lg-6 {{ ($package->adults_only) ? 'hidden' : '' }}">
                            <select name="booking[{{$id}}][childs]" class="childs c-select -dashed">
                                <option value="0">Nenhuma criança</option>
                                @for ($i = 1; $i < 10; $i++)
                                    <option
                                        @if($item->childs == $i)
                                             selected="selected"
                                        @endif
                                        value="{{$i}}"
                                    >
                                        {{$i}} @lang('front.booking.child.singular') ({{config('app.currency.prefix') . $package->product->getChildPrice() * $i}})
                                    </option>
                                @endfor
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="text-help" for="">@lang('front.booking.pickup.label')</label>
                    <div class="row">
                        <div class="col-xs-12 col-lg-6">
                            <!-- IMPORTANT: Campos abaixo precisam ter o atributo ID único -->
                            <input type="text" name="booking[{{$id}}][departure_location]" id="departure_location-{{$id}}" class="departure-location form-control -dashed" placeholder="@lang('front.booking.pickup.location')" @if(!is_null($item->departure_location)) value="{{$item->departure_location}}" @endif >
                        </div>
                        <div class="col-xs-12 col-lg-6">
                            <input type="text" name="booking[{{$id}}][departure_hour]" id="departure_hour-{{$id}}" class="departure-hour form-control -dashed" placeholder="@lang('front.booking.pickup.hour')" @if(!is_null($item->departure_hour)) value="{{$item->departure_hour}}" @endif>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="subtotal col-xs-12 col-md-3">
            <span>Subtotal</span>
            <span class="value">{{ config('app.currency.prefix') }}<span class="text-subtotal">{{ $package->product->getSubtotal($item->adults, $item->childs) }}</span></span>
            <span class="amount">
                <span class="text-adults">{{$item->adults}}</span> @lang('front.booking.adult.singular')
                @if(!$package->adults_only)
                    , <span class="text-childs">{{$item->childs}}</span> @lang('front.booking.child.plural')
                @endif
            </span>
        </div>
    </div>
</article>
