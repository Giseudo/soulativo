@extends('layouts.app', ['submenu' => 'not-fixed'])

@section('content')

{!! Form::open(array('url' => '/cart/update', 'class' => 'cart -form-default')) !!}
    <header class="checkout -steps">
        <h3>@lang('front.checkout.cart.step')</h3>
        <h1>@lang('front.checkout.cart.title')</h1>
        <p>{{ Auth::user()->first_name }}, @lang('front.checkout.cart.description')</p>
        <a href="/">« @lang('front.checkout.cart.back')</a>
    </header>

    <section class="-list">
        <?php $cont = 1; ?>
        @foreach($cart as $id => $item)
            @include('cart.package', ['package' => $item->package])
            <?php
                $cont++;
                $total += $item->package->product->getSubtotal($item->adults, $item->childs);
            ?>
        @endforeach
    </section>

    <footer>
        @if(count($cart) > 0)
            <div class="next container">
                <div class="total col-xs-12 col-md-6">
                    <span>Total</span>
                    <span>{{config('app.currency.prefix') }}<span class="text-total">{{ $total}}</span></span>
                </div>
                <div class="forward col-xs-12 col-md-offset-2 col-md-4">
                    <input type="submit" name="name" class="-block" value="@lang('front.checkout.submit')">
                </div>
            </div>
        @else
            <div class="empty container">
                <div class="col-xs-12">
                    <div class="alert alert-info">
                        <p>@lang('front.cart.bag.empty')</p>
                    </div>
                </div>
            </div>
        @endif
    </footer>
{!! Form::close() !!}

@endsection
