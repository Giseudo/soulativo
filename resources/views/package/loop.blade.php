<article class="col-xs-12 @if($size == 'medium') col-md-6 @elseif($size == 'big') col-md-12 @endif">
    <a href="/package/{{$package->product->slug}}" class="link-overflow"></a>
    <div class="wrapper" style="@if(!is_null($package->thumbnail_id)) background-image:url('/images/{{$package->thumbnail->filename}}') @endif">
        <aside class="-dark">
            <header>
                <span class="cat">

                    @foreach($package->product->categories as $cat)

                        <a href="/packages/{{$cat->slug}}">{{$cat->name}}</a>
                    @endforeach
                </span>

                <h1><a href="/package/{{$package->product->slug}}">{{$package->product->name}}</a></h1>

                <small class="excerpt">{{$package->product->excerpt}}</small>
            </header>
            @if($package->product->getPrice() > 0)
                <footer>
                    @include('package.price', ['package' => $package])
                </footer>
            @endif
        </aside>

    </div>
</article>
