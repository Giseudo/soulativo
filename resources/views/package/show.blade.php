@extends('layouts.app', ['submenu' => 'not-fixed'])

@section('content')
    <div class="container">
        <div class="package -read">
            <article class="col-xs-12 col-md-8 col-lg-7">
                @if(count($gallery) > 0)
                    <!-- Swiper -->
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                            @foreach($gallery as $img)

                            <div class="swiper-slide">
                                <img src="/images/{{$img->filename}}?w=610&h=460&fit=crop-50-50" alt="">
                            </div>
                            @endforeach
                        </div>
                        <div class="swiper-pagination"></div>
                    </div>
                @endif

                <!--div class="-content">
                    <p>{{$package->product->excerpt}}</p>
                </div-->

                <ul class="nav nav-tabs" id="nav-tab">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#content">@lang('front.package.tabs.content')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#terms">@lang('front.package.tabs.terms')</a>
                    </li>
                    @if($package->map_data)
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#map">@lang('front.package.tabs.map')</a>
                        </li>
                    @endif
                </ul>

                <div class="tab-content">
                    <div id="content" class="tab-pane active -content -dante">
                        {!! $package->product->content !!}
                    </div>

                    <div id="terms" class="tab-pane -content -dante">
                        {!! $package->terms !!}
                    </div>

                    @if($package->map_data)
                        <div id="map" class="tab-pane">
                            <div id="package-map" data-mapdata="{{$package->map_data}}"></div>
                        </div>
                    @endif
                </div>
            </article>

            <aside class="col-xs-12 col-md-4 col-lg-offset-1">
                <header>
                    <span class="cat">
                        @foreach($package->product->categories as $cat)
                            <a href="/packages/{{$cat->slug}}">{{$cat->name}}</a>
                        @endforeach
                    </span>
                    <h1>{{$package->product->name}}</h1>
                </header>
                @if($package->product->getPrice() > 0)
                    <div class="price">
                        <span class="old">
                            @if($package->product->getPrice() != $package->product->getDiscountedPrice())
                                {{config('app.currency.prefix') . $package->product->getPrice()}}
                            @endif
                            <span class="info">
                                @if($package->product->getPrice() != $package->product->getDiscountedPrice())
                                    <span class="-highlight">@lang('front.product.discount', ['percent' => $package->product->getDiscountedPercent()])</span>
                                @endif
                            </span>
                        </span>
                        <span class="current">{{config('app.currency.prefix') . $package->product->getDiscountedPrice()}} @lang('front.package.per.adult')</span>
                        @if(!$package->adults_only)
                            <span class="child">{{config('app.currency.prefix') . $package->product->getChildPrice()}} @lang('front.package.per.child')</span>
                        @endif
                    </div>
                    {!! Form::open(array('url' => '/cart', 'class' => '-form-default', 'data-logged' => Auth::check())) !!}
                        @include('package.fieldset')
                        <footer>
                            <input type="hidden" name="package_id" id="package_id" value="{{$package->id}}" />
                            <input type="hidden" name="price" id="price" value="{{$package->product->getPrice()}}" />
                            <input type="hidden" name="package_name" id="package_name" value="{{$package->product->name}}" />
                            <input type="submit" value="@lang((($package->product->getPrice() != $package->product->getDiscountedPrice())) ? 'front.package.submit.discounted' : 'front.package.submit.regular', ['percent' => $package->product->getDiscountedPercent()])">
                            <input type="hidden" value="{{$blockouts}}" id="blockouts" />
                        </footer>
                    {!! Form::close() !!}
                @else
                    {!! Form::open(array('url' => '', 'class' => '-form-default')) !!}
                        <footer>
                            <a href="/contact" class="contact-price">@lang('front.package.contact-price')</a>
                        </footer>
                    {!! Form::close() !!}
                @endif
                <nav class="share row">
                    <div class="col-xs-12 col-md-6">
                        <a href="https://twitter.com/share?url={{$currentUrl}}&via=SoulAtivo&text={{$package->product->name}}" target="blank" class="twitter">
                            <span class="icon icon-twitter-alt"></span>
                            @lang('front.social.twitter.share')
                        </a>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <a href="https://www.facebook.com/sharer/sharer.php?u={{$currentUrl}}" target="blank" class="facebook">
                            <span class="icon icon-facebook-alt"></span>
                            @lang('front.social.facebook.share')
                        </a>
                    </div>
                </nav>
            </aside>
        </div>
        <div class="package -list col-xs-12">
            <h3>@lang('front.package.related')</h3>
            <div class="row">
                @foreach($related as $package)
                    @include('package.loop', ['size' => 'medium'])
                @endforeach
            </div>
        </div>
        <div class="blog -list col-xs-12 col-md-8 col-lg-8">
            <h3>@lang('front.blog.latest') <a href="#">@lang('front.blog.more') »</a></h3>
            <div class="row">
                @foreach($recentPosts as $recent)
                    @include('blog.tiny')
                @endforeach
            </div>
        </div>
        <aside class="-sidebar col-xs-12 col-md-4 col-lg-3 col-lg-offset-1 m-t-0">
            @include('partials.instagram')
            @include('partials.tripadvisor')
        </aside>
    </div>
@endsection
