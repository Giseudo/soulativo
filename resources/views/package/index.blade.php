@extends('layouts.app')

@section('content')

    <header class="-page-head" style="background-image: url(@if(!empty(@$category->thumbnail_id)) '/images/{{$category->thumbnail->filename}}' @else '/images/delete/bg-top-01.jpg' @endif)">
        <div class="container">
            <div class="wrapper">
                <nav class="category">
                    @foreach($category->getAncestors() as $cat)
                        @if($cat->isRoot())
                            @continue
                        @endif
                        <a href="/packages/{{$cat->slug}}">{{$cat->name}} » </a>
                    @endforeach
                </nav>
                <h1>{{$category->name}}</h1>
            </div>
        </div>
    </header>
    <div class="container">
        <div class="package -list">
            @if(count($packages) > 0)
                <?php $cont = 1; ?>
                @foreach($packages as $package)
                    @if($cont == 3)
                        @include('package.loop', ['size' => 'big'])
                        <?php $cont = 1; ?>
                    @else
                        @include('package.loop', ['size' => 'medium'])
                        <?php $cont++; ?>
                    @endif
                @endforeach
               
            @else
                <div class="col-xs-12">
                    <div class="alert alert-info">
                        <p>Não há produtos relacionados à categoria {{$category->name}}</p>
                    </div>
                </div>
            @endif
        </div>
    </div>

@endsection
