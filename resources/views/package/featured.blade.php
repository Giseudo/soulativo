<section class="featured" style="@if(!empty($package_destaque->thumbnail_id)) background-image:url('/images/{{$package_destaque->thumbnail->filename}}') @endif">
    <div class="container -dark">
        <article class="package col-xs-12 col-md-8 col-lg-6" style="@if(!empty($package_destaque->thumbnail_id)) background-image:url('/images/{{$package_destaque->thumbnail->filename}}') @endif">
            <header>
                <span class="cat">
                    @foreach($package_destaque->product->categories as $cat)
                        <a href="/packages/{{$cat->slug}}">{{$cat->name}}</a>
                    @endforeach
                </span>
                <h1><a href="/package/{{$package_destaque->product->slug}}">{{$package_destaque->product->name}}</a></h1>
            </header>
            <p>{{$package_destaque->product->excerpt}}</p>
            <footer class="-dark">
                @include('package.price', ['package' => $package_destaque])
                <a href="/package/{{$package_destaque->product->slug}}">@lang('front.package.featured.more')</a>
            </footer>
        </article>
        {!! Form::open(array('url' => '/package/search', 'method' => 'POST', 'class' => 'search -form-default col-xs-12 col-lg-5 col-lg-offset-1')) !!}
            <header class="col-xs-12">
                <span class="icon icon-icon-magnifier"></span>
                <h2>@lang('front.search.title')</h2>
            </header>
            @include('search.fieldset')
        {!! Form::close() !!}
    </div>
</section>
