<div class="modal fade -dark" id="auth-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <header>
            <h1>@lang('front.auth.title')</h1>
            <h2>@lang('front.auth.subtitle')</h2>
        </header>
        <div class="wrapper">
            {!! Form::open(array('url' => '/login', 'class' => 'login -form-default col-xs-12 col-md-6 col-lg-5')) !!}
                <h3>@lang('front.auth.login.title')</h3>
                <div class="alert alert-danger">
                    <p>@lang('front.auth.login.error')</p>
                </div>
                @include('auth.login.fieldset')
                <footer>
                    <nav class="share row">
                        <div class="col-xs-12 col-md-6">
                            <a href="/login/twitter" class="twitter">
                                <span class="icon icon-twitter-alt"></span>
                                @lang('front.social.twitter.signin')
                            </a>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <a href="/login/facebook" class="facebook">
                                <span class="icon icon-facebook-alt"></span>
                                @lang('front.social.facebook.signin')
                            </a>
                        </div>
                    </nav>
                </footer>
            {!! Form::close() !!}

            {!! Form::open(array('url' => '/register', 'class' => 'register -form-default col-xs-12 col-md-6 col-lg-5 col-lg-offset-1 col-lg-push-1')) !!}
                <h3>@lang('front.auth.register.title')</h3>
                <div class="alert alert-danger">
                    <p></p>
                </div>
                @include('auth.register.fieldset')
                <footer>
                    <nav class="share row">
                        <div class="col-xs-12 col-md-6">
                            <a href="/login/twitter" class="twitter">
                                <span class="icon icon-twitter-alt"></span>
                                @lang('front.social.twitter.signin')
                            </a>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <a href="/login/facebook" class="facebook">
                                <span class="icon icon-facebook-alt"></span>
                                @lang('front.social.facebook.signin')
                            </a>
                        </div>
                    </nav>
                </footer>
            {!! Form::close() !!}
        </div>
    </div>
</div>
