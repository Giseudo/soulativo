<fieldset>
    <h2>@lang('front.profile.user')</h2>
    <div class="row">
        <div class="form-group col-xs-12 col-md-6">
            {{ Form::label('user-first_name', trans('model.user.first_name')) }}
            {{ Form::text('first_name', $user->first_name, ['class' => 'form-control -dashed', 'id' => 'user-first_name']) }}
        </div>
        <div class="form-group col-xs-12 col-md-6">
            {{ Form::label('user-last_name', trans('model.user.last_name')) }}
            {{ Form::text('last_name', $user->last_name, ['class' => 'form-control -dashed', 'id' => 'user-last_name']) }}
        </div>
    </div>
    <div class="form-group">
        {{ Form::label('user-email', trans('model.user.email')) }}
        {{ Form::email('email', $user->email, ['class' => 'form-control -dashed', 'placeholder' => 'example@gmail.com', 'id' => 'user-email']) }}
    </div>
    <div class="row">
        <div class="form-group col-xs-12 col-md-6">
            {{ Form::label('user-password', trans('model.user.password')) }}
            {{ Form::password('password', ['class' => 'form-control -dashed', 'id' => 'user-password']) }}
        </div>
        <div class="form-group col-xs-12 col-md-6">
            {{ Form::label('user-password_confirmation', trans('model.password.confirm')) }}
            {{ Form::password('password_confirmation', ['class' => 'form-control -dashed', 'id' => 'user-password_confirmation']) }}
        </div>
    </div>
</fieldset>
