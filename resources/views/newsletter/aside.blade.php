{!! Form::open(array('url' => '/newsletter/1bbe9ea6f8/register', 'class' => 'newsletter -form-default m-t-3')) !!}
    <h4>@lang('front.newsletter.aside.title')</h4>
    <div class="form-group text-xs-right">
        <input type="email" name="email" class="-rounded form-control" placeholder="@lang('front.newsletter.placeholder')">
        <input type="submit" name="send" value="@lang('front.newsletter.aside.submit') »">
    </div>
{!! Form::close() !!}
