{!! Form::open(array('url' => '/newsletter/1bbe9ea6f8/register', 'class' => 'newsletter -form-default col-xs-12')) !!}
    <h4>@lang('front.newsletter.footer.title')</h4>
    <div class="form-group">
        <input type="email" name="email" class="form-control -rounded" placeholder="@lang('front.newsletter.placeholder')">
        <input type="submit" value="@lang('front.newsletter.footer.submit') »">
    </div>
{!! Form::close() !!}
