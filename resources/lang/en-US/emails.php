<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Fieldset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'invoice' => [
      'success' => [
        'subject' => 'Payment Confirmation',
        'greetings' => 'Hi :name,',
        'thanks' => 'Thank you for your preference!',
        'title' => 'Purchase completed successfully',
        'order' => 'Your order number is:: #:id',
        'button' => 'See my Orders',
        'bookings' => "Bookings"
      ],
    ],
    'footer' => [
      'facebook' => 'Follow us on Facebook',
    ],
];
