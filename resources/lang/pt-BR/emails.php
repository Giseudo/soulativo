<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Fieldset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'invoice' => [
      'success' => [
        'subject' => 'Confirmação de Pagamento',
        'greetings' => 'Olá :name,',
        'thanks' => 'Obrigado pela preferencia!',
        'title' => 'Compra realizada com sucesso',
        'order' => 'O número do seu pedido é: #:id',
        'button' => 'Ver meus pedidos',
        'bookings' => "Reservas"
      ],
    ],
    'footer' => [
      'facebook' => 'Siga-nos no Facebook',
    ],
];
