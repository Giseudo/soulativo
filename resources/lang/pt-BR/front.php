<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Fieldset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'menu' => [
        'currency' => [
            'real' => 'Moeda – Real (R$)',
            'dollar' => 'Moeda – Dolar ($)',
            'euro' => 'Moeda – Euro (€)',
        ],
        'lang' => [
            'current' => 'Versão em português',
            'english' => 'English version',
            'portuguese' => 'Versão em português',
            'spanish' => 'Version en español',
        ],
        'partner' => 'Seja um parceiro',
    ],
    'categories' => [
        'adventure' => 'Aventuras',
        'city-tour' => 'City Tour',
        'transport' => 'Transporte',
    ],
    'profile' => [
        'title' => 'Cadastro',
        'user' => 'Dados de acesso',
        'address' => 'Dados de cobrança',
        'customer' => 'Dados pessoais',
        'orders' => 'Compras recentes'
    ],
    'auth' => [
        'title' => 'Rumo à aventura!',
        'subtitle' => 'Entre na sua para avançar',
        'signin' => 'Entrar',
        'signout' => 'Sair',
        'login' => [
            'title' => 'Já tenho cadastro',
            'submit' => 'Entrar',
            'password' => 'Aff, esqueci a senha',
        ],
        'register' => [
            'title' => 'Não sou cadastrado',
            'submit' => 'Cadastre-se',
        ]
    ],
    'social' => [
        'facebook' => [
            'share' => 'Compartilhar no Facebook',
            'signin' => 'Entrar via Facebook',
        ],
        'twitter' => [
            'share' => 'Compartilhar no Twitter',
            'signin' => 'Entrar via Twitter'
        ],
    ],
    'cart' => [
        'bag' => [
            'singular' => 'item na mochila',
            'plural' => 'itens na mochila',
            'empty' => 'Nenhum item na mochila',
        ]
    ],
    'search' => [
        'title' => 'Encontre sua aventura',
        'experience' => [
            'title' => 'Qual sua experiência?',
            'beginner' => 'Sou iniciante em aventuras',
            'radical' => 'Curto aventuras radicais',
        ],
        'submit' => 'Buscar'
    ],
    'booking' => [
        'when' => 'Reserve Agora',
        'many' => 'Quantas pessoas?',
        'from' => 'De:',
        'to' => 'Até:',
        'each' => 'cada',
        'reservation' => [
            'singular' => 'reserva',
            'plural' => 'reservas',
        ],
        'adult' => [
            'singular' => 'adulto',
            'plural' => 'adultos',
        ],
        'child' => [
            'empty' => 'Nenhuma criança',
            'singular' => 'criança',
            'plural' => 'crianças',
        ],
        'pickup' => [
            'label' => 'Pick up (opcional)',
            'location' => 'Informe o local onde você estará',
            'hour' => 'Escolha a hora para te buscarmos',
        ]
    ],
    'package' => [
        'per' => [
            'adult' => 'por adulto',
            'child' => 'por criança'
        ],
        'featured' => [
            'more' => 'Aventure-se',
        ],
        'tabs' => [
            'content' => 'Sua aventura',
            'terms' => 'Regulamento',
            'map' => 'Mapa',
        ],
        'parcel' => 'Em até 3x sem juros',
        'related' => 'Veja também',
        'submit' => [
            'regular' => 'Compre já',
            'discounted' => 'Compre já com :percent% de desconto',
        ],
        'contact-price' => 'Consulte o Preço'
    ],
    'product' => [
        'discount' => ':percent% de desconto',
    ],
    'checkout' => [
        'cart' => [
            'step' => 'Passo 1 de 3',
            'title' => 'Mochila',
            'description' => 'verifique a quantidade de itens na sua mochila e<br /> confirme as datas de agendamento.',
            'back' => 'Voltar às compras',
            'remove' => 'Remover da mochila',
        ],
        'customer' => [
            'step' => 'Passo 2 de 3',
            'title' => 'Dados adicionais',
            'description' => 'Precisamos que preencha alguns dados adicionais para<br /> fecharmos a sua compra. É bem rápido.',
            'back' => 'Voltar',
        ],
        'payment' => [
            'step' => 'Último passo',
            'title' => 'Pagamento',
            'description' => 'Escolha a forma de pagamento que for melhor para você.<br /> Esse é um ambiente 100% seguro.',
            'back' => 'Voltar',
            'summary' => 'Resumo da compra',
            'method' => [
                'title' => 'Dados de pagamento',
                'card' => 'Pagar usando cartão de crédito',
                'paypal' => 'Pagar usando PayPal',
                'pagseguro' => 'Pagar usando PagSeguro',
                'mercadopago' => 'Pagar usando MercadoPago',
            ],
            'submit' => 'Continuar com pagamento',
        ],
        'confirmation' => [
            'step' => 'Obrigado!',
            'title' => 'Pagamento Enviado',
            'description' => 'Seu pagamento foi enviado. Você pode acompanhar o status<br /> da sua compra na seção <a href=":link">Perfil</a>.',
            'back' => 'Voltar às compras',
        ],
        'submit' => 'Próximo passo',
    ],
    'order' => [
        'pickup' => 'Pickup às',
        'list' => [
            'date' => 'Pagamento enviado dia',
        ],
        'status' => [
            'pending' => 'Aguardando aprovação',
            'approved' => 'Confirmado',
            'rejected' => 'Rejeitado',
        ]
    ],
    'blog' => [
        'latest' => 'Últimas postagens do blog',
        'more' => 'Ver blog'
    ],
    'newsletter' => [
        'placeholder' => 'Seu email',
        'footer' => [
            'title' => 'Cadastre-se e receba ofertas exclusivas',
            'submit' => 'Receber ofertas',
        ],
        'aside' => [
            'title' => 'Newsletter de ofertas exclusivas',
            'submit' => 'Receber ofertas',
        ]
    ],
    'contact' => [
        'title' => 'Contato',
        'description' => 'Está com dúvida sobre alguma coisa? Sem problemas. Entre em contato conosco por meio do formulário abaixo, por telefone, Facebook, WhatsApp, Twitter, etc.',
        'form' => [
            'title' => 'Fale conosco',
            'subject' => 'Assunto',
            'message' => 'Mensagem',
            'submit' => 'Enviar mensagem',
        ],
        'aside' => [
            'title' => 'Você também nos encontra via',
            'faq' => 'Tente tirar dúvidas em nosso FAQ',
        ],
    ],
    'payment' => [
        'title' => 'Formas de pagamento'
    ],
    'copyright' => 'Todos os direitos reservados 2016 SoulAtivo'
];
