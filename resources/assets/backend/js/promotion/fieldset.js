define([
    'language',
    'picker/date',
    'query-builder'
], function (Lang) {

    var PromotionFieldset= function (element, options) {
        this.init(element, options);
    };

    PromotionFieldset.defaults = {
        'from' : 'input[name="promotion[from]"]',
        'to' : 'input[name="promotion[to]"]',
        'validate' : {
            rules : {

            },
            messages : {

            }
        },
        builder : '#promotion-rules',
        categories : {
            product : '#product-categories'
        }
    };

    PromotionFieldset.prototype = {
        init : function (element, options) {
            this.options = $.extend(true, {}, PromotionFieldset.defaults, options);
            this.element = $(element);
            this.queryBuilder();
            $(this.options.from).pickadate();
            $(this.options.to).pickadate();
        },

        queryBuilder : function(){
            $(this.options.builder).queryBuilder({
                allow_groups : 0,
                icons: {
                    add_group: 'fa fa-plus-square',
                    add_rule: 'fa fa-plus-circle',
                    remove_group: 'fa fa-minus-square',
                    remove_rule: 'fa fa-minus-circle',
                    error: 'fa fa-exclamation-triangle'
                },
                filters: [
                    {
                        id: 'categories',
                        field: 'product',
                        label: 'Product Categories',
                        type: 'integer',
                        input: 'select',
                        values: $(this.options.categories.product).data('categories'),
                        operators: ['in', 'not_in', 'is_null', 'is_not_null']
                    }
                ],
                rules: (this.element.val() != '') ? JSON.parse(this.element.val()) : null
            });
        }
    };

    return PromotionFieldset;
});
