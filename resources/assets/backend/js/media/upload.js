define([

], function () {

    var Upload = function (file, options) {
        this.init(file, options);
    };

    Upload.defaults = {
        'url' : '/upload'
    };

    Upload.prototype = {
        init : function (file, options) {
            var that = this;

            this.options = $.extend(true, {}, Upload.defaults, options);
            this.file = $(file);
        },

        uploadFile : function (callback) {
            var target = $(this.options.element),
                url = '//' + location.host + this.options.url,
                formData = new FormData(),
                parent = this.file.closest('.modal-dialog');

            $.each(this.file[0].files, function (index) {
                formData.append("files[]", this);
            });
            formData.append("_token", $('input[name="_token"]').val());

            $.ajax({
                url : url,
                type : 'POST',
                data : formData,
                contentType: false,
                processData: false,
                dataType : 'json',
                success : function(result){
                    callback(result);
                },
                complete : function(result){

                }
            });

            return false;
        }
    };

    return Upload;
});
