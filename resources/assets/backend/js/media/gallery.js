define([
    'media/upload',
    'sortable',
    'form'
], function (Upload, Sortable, Form) {

    var Gallery = function (element, options) {
        this.init(element, options);
    };

    Gallery.defaults = {
        'multiple' : false,
        'sortable' : false,
        'modal' : '',
        'target' : ''
    };

    Gallery.prototype = {
        init : function (element, options) {
            var that = this;
            this.options = $.extend(true, {}, Gallery.defaults, options);
            this.element = $(element);
            this.createModal(element);
            this.upload = new Upload($('input[name="files"]', this.options.modal), {
                url : $('form.upload', this.options.modal).attr('action')
            });

            // On file upload
            $('input[name="files"]', this.options.modal).on('change', function () {
                $('.nav-tabs li:last-child a', that.options.modal).trigger('click');
                that.preloadFiles(this.files.length);
                that.upload.uploadFile(function (result) {
                    that.loadFiles(result.images);
                });
            });

            // On modal is visible
            $(this.options.modal).on('show.bs.modal', function (e) {
                that.displayFiles();
            });

            // On modal is hidden
            $(this.options.modal).on('hidden.bs.modal', function (e) {
                that.destroyFiles();
                $('aside', that.options.modal).empty();
            });

            // Populates the target with the selected images
            $('button.submit', this.options.modal).on('click', function (){
                that.addToAlbum();
            });

            // Make the defaul images sortable and deletable
            $('figure span.fa-minus-square', this.options.target).on('click', function(){
                that.removeFromAlbum($(this).parent());
            });

            if (this.options.sortable)
              this.initSortable();
        },

        createModal : function (element) {
            this.options.modal = $(element).attr('href');
            var index = $(element).attr('id'),
                template = _.template($('#gallery-modal-template').html()),
                data = {
                    'indent' : this.options.modal.replace('#', ''),
                    'index' : index,
                    'target' : this.options.target,
                    'multiple' : this.options.multiple
                };

            $('body').append(template(data));
        },

        toggleSelection : function (target){
            var that = this;
            $(target, this.options.modal).on('click', function () {
                if($(this).hasClass('-active')){
                    $(this).removeClass('-selected');
                    $(this).removeClass('-active');
                    $('aside', that.options.modal).empty();
                    return;
                }

                if(!that.options.multiple){
                    $('.album figure', that.options.modal).removeClass('-selected');
                }
                $('.album figure', that.options.modal).removeClass('-active');

                $(this).addClass('-selected');
                $(this).addClass('-active');

                that.showDescription(target);
            });
        },

        preloadFiles : function(count){
            var template = _.template($('#gallery-figure-template').html()),
                data = { 'loading' : true };

            for (var i = 0; i < count; i++) {
                $('.album', this.options.modal).prepend(template(data));
            }
        },

        loadFiles : function(images){
            var that = this;

            $.each(images, function (index){
                if(!this.error){
                    template = _.template($('#gallery-figure-template').html()),
                    data = {
                        'id' : this.media.id,
                        'filename' : this.media.filename,
                        'title' : this.media.title,
                        'caption' : this.media.caption,
                        'created' : this.media.created_at,
                        'size' : this.media.size,
                        'width' : this.width,
                        'height' : this.height,
                        'loading' : false
                    };

                    var album = $('.album', that.options.modal).prepend(template(data));
                    that.toggleSelection($(':first-child', album));
                }else{
                    $('.album .loading:eq('+ index +')').addClass('-error');
                    $('.album .loading:eq('+ index +')').html('Erro: Tamanho máximo excedido!');
                    $('.album .loading:eq('+ index +')').removeClass('loading');
                }

                $('.album .loading').remove();
            });
        },

        displayFiles : function (){
            var that = this,
                url = '//' + location.host + '/media/images',
                images = [];

            // TODO: We need to pass selected files
            $.ajax({
                url : url,
                type : 'GET',
                dataType : 'json',
                success : function(result){
                    that.loadFiles(result.images)
                },
                complete : function(result){

                }
            });
        },

        destroyFiles : function(){
            $('.album', this.options.modal).empty();
        },

        showDescription : function(target){
            var that = this,
                details = $('aside', this.options.modal),
                template = _.template($('#gallery-details-template').html()),
                data = {
                    'id' : $(target).data('id'),
                    'path' : '//' + location.host + '/images/' + $(target).data('filename') + '?w=150&h=150&fit=crop-50-50',
                    'title' : $(target).data('title'),
                    'created' : $(target).data('created'),
                    'size' : $(target).data('size'),
                    'width' : $(target).data('width'),
                    'height' : $(target).data('height'),
                    'caption' : $(target).data('caption')
                };

            details.html(template(data));

            var updateForm = new Form('form.update', {
                validate : {
                    rules : {
                        'media[caption]' : 'required',
                    },
                    messages : {

                    },
                    submitHandler: function (form){
                        that.updateImage(form);
                        return false;
                    }
                }
            });

            $('form.destroy').on('submit', function () {
                that.destroyImage(this);
                return false;
            });
        },

        addToAlbum : function(){
            var that = this,
                selected = $('.album figure.-selected', this.options.modal),
                input = $(this.options.target).parent().find('.gallery-json'),
                value = (this.options.multiple) ?
                    ((input.val().length > 0) ?
                        $.parseJSON(input.val()) :
                        { images : [] }
                    ) :
                    input.val();

            if(!this.options.multiple){
                $(this.options.target).empty();
            }

            selected.each(function () {
                $(that.options.target).prepend(this);
                $(':first-child span.fa-minus-square', that.options.target).on('click', function(){
                    that.removeFromAlbum($(this).parent());
                });

                if(that.options.multiple){
                    value.images.unshift($(this).data('id'))
                }else{
                    value = $(this).data('id');
                }
            });

            if (this.options.multiple) {
              this.orderGallery();
            } else {
              input.val(value);
            }
        },

        orderGallery : function () {
          // Order images array
          var input = $(this.options.target).parent().find('.gallery-json'),
            json = {
              images : []
            };

          $('figure', this.options.target).each(function() {
            json.images.push($(this).data('id'));
          });

          input.val(JSON.stringify(json));
        },

        initSortable : function () {
          var that = this,
            sortable = new Sortable($(this.options.target)[0], {
              animation: 150,
              draggable: "figure",
              // Called by any change to the list (add / update / remove)
              onSort: function (evt) {
                  that.orderGallery();
              }
          });

          $(this.options.target).addClass('album--sortable');
        },

        removeFromAlbum : function(image){
            var input = $(this.options.target).parent().find('.gallery-json'),
                value = (this.options.multiple) ?
                    $.parseJSON(input.val()) :
                    input.val();

            if(this.options.multiple){
                value.images.splice(value.images.indexOf(image.data('id')), 1);
            }else{
                value = '';
            }

            input.val((this.options.multiple) ? JSON.stringify(value) : value);
            image.remove();
        },

        updateImage : function (form) {
            var url = $(form).prop('action');

            $.ajax({
                url : url,
                data : $(form).serialize() + '&_token=' + $('input[name="_token"]').val(),
                type : 'POST',
                dataType : 'json',
                success : function(result){
                    if(!result.error){
                        $('.alert-success', form).slideDown();
                    }else{
                        $('.alert-danger', form).slideDown();
                    }
                },
                complete : function(result){
                    setTimeout(function () {
                        $('.alert', form).slideUp();
                    }, 2000);
                }
            });
        },

        destroyImage : function (form) {
            var that = this,
                url = $(form).prop('action');

            $.ajax({
                url : url,
                data : { _token : $('input[name="_token"]').val() },
                type : 'POST',
                dataType : 'json',
                success : function(result){
                    if(!result.error){
                        // Removes image
                        var image = $('figure[data-id="' + $(form).data('id') + '"]');
                        $(form).closest('.sidebar').empty();
                        image.remove();
                    }else{
                        // Show error message
                        $('.alert-danger', form).slideDown();
                    }
                },
                complete : function(result){
                    // Hides message after some time
                    setTimeout(function () {
                        $('.alert', form).slideUp();
                    }, 2000);
                }
            });
        }
    };

    return Gallery;
});
