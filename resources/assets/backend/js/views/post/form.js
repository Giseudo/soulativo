define([
    'post/fieldset',
    'media/gallery',
    'category/sidebar',
    'form'
], function (Post, Gallery, Category, Form) {
    return {
        init : function () {
            var that = this;

            this.post = new Post();
            this.validation = $.extend(true, {},
                this.post.options.validate,
                {
                    submitHandler : function () {
                        //Insert Dante Editor HTML into hidden input
                        $('#page-editor').find('figcaption').attr('contenteditable', false);
                        $('#post-content').val($(that.post.content.el).html());

                        return true;
                    }
                }
            );
            this.form = new Form('#form-post', {
                validate : this.validation,
            });
            this.category = new Category({
                element : '#category-sidebar',
                inputName : 'post[categories][]'
            });
            this.thumbnail = new Gallery('#thumbnail-set', { target : '#thumbnail-wrapper' });
        }
    }
});
