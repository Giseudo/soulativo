define([
    'coupon/fieldset',
    'form'
], function (Coupon, Form) {
    return {
        init : function () {
            var that = this;

            this.coupon = new Coupon();
            this.validation = $.extend(true, {},
                this.coupon.options.validate
            );
            this.form = new Form('#form-coupon', {
                validate : this.validation,
            });
        }
    }
});
