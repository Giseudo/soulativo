define([
    'list/paginate'
], function (Paginate) {
    return {
        init : function () {
            this.postPaginate = new Paginate({
                button : '#posts a.paginate',
                target : '#posts table tbody',
                template : '#post-list-template'
            });

            this.pagePaginate = new Paginate({
                button : '#pages a.paginate',
                target : '#pages table tbody',
                template : '#page-list-template'
            });

            this.categoryPaginate = new Paginate({
                button : '#categories a.paginate',
                target : '#categories table tbody',
                template : '#category-list-template'
            });

            this.packagePaginate = new Paginate({
                button : '#packages a.paginate',
                target : '#packages table tbody',
                template : '#package-list-template'
            });
        }
    }
});
