define([
    'order/modal'
], function (OrderModal) {
    return {
        init : function () {
            this.modal = new OrderModal();
        }
    }
});
