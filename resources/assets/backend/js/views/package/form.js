define([
    'product/fieldset',
    'package/fieldset',
    'category/sidebar',
    'media/gallery',
    'form'
], function (Product, Package, Category, Gallery, Form) {
    return {
        init : function () {
            var that = this;
            this.package = new Package();
            this.product = new Product();

            this.validation = $.extend(true, {},
                this.package.options.validate,
                this.product.options.validate,
                {
                    submitHandler : function () {
                        //Insert Dante Editor HTML into hidden input
                        $('#page-editor').find('figcaption').attr('contenteditable', false);
                        $('#product-content').val(that.product.content.getContent());

                        $('#terms-editor').find('figcaption').attr('contenteditable', false);
                        $('#package-content').val(that.package.content.getContent());
                        return true;
                    }
                }
            );

            this.form = new Form('#form-package', {
                validate : this.validation
            });

            this.category = new Category({
                element : '#category-sidebar',
                inputName : 'product[categories][]'
            });

            this.thumbnail = new Gallery('#thumbnail-set', { target : '#thumbnail-wrapper' });
            this.gallery = new Gallery('#package-gallery-set', { multiple : true, sortable : true, target : '#package-gallery-wrapper' });
        }
    }
});
