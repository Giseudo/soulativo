define([
    'page/fieldset',
    'media/gallery',
    'form'
], function (Page, Gallery, Form) {
    return {
        init : function () {
            var that = this;

            this.page = new Page();
            this.validation = $.extend(true, {},
                this.page.options.validate,
                {
                    submitHandler : function () {
                        //Insert Dante Editor HTML into hidden input
                        $('#page-editor').find('figcaption').attr('contenteditable', false);
                        $('#page-content').val($(that.page.content.el).html());

                        return true;
                    }
                }
            );
            this.form = new Form('#form-page', {
                validate : this.validation,
            });
            this.thumbnail = new Gallery('#thumbnail-set', { target : '#thumbnail-wrapper' });
        }
    }
});
