define([
    'user/fieldset',
    'customer/fieldset',
    'address/fieldset',
    'password/compare',
    'form'
], function (User, Customer, Address, Compare, Form) {
    return {
        init : function () {
            this.user = new User();
            this.customer = new Customer();
            this.address = new Address();
            this.validation = $.extend(true, {},
                this.user.options.validate,
                this.customer.options.validate,
                this.address.options.validate,
                {
                    submitHandler : function () {
                        return true;
                    }
                }
            );
            this.compare = new Compare();
            this.form = new Form('#form-customer', {
                validate : this.validation
            });
        }
    }
});
