require([
    // Dashboard
    'views/dashboard/index',
    // User
    'views/user/list',
    'views/user/form',
    // Customer
    'views/customer/list',
    'views/customer/form',
    // Package
    'views/package/list',
    'views/package/form',
    // Category
    'views/category/list',
    'views/category/form',
    // Booking
    'views/booking/list',
    'views/booking/form',
    // Page
    'views/page/list',
    'views/page/form',
    // Post
    'views/post/list',
    'views/post/form',
    // Store
    'views/store/list',
    'views/store/form',
    // Parner
    'views/partner/list',
    // Promotion
    'views/promotion/list',
    'views/promotion/form',
    // Order
    'views/order/list',
    'views/order/show',
    // Coupon
    'views/coupon/list',
    'views/coupon/form',
    // Blockout
    'views/blockout/list',
    'views/blockout/form',
    // SEO
    'views/seo/list'
]);
