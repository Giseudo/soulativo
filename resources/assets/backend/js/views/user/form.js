define([
    'user/fieldset',
    'password/compare',
    'form'
], function (User, Compare, Form) {
    return {
        init : function () {
            this.user = new User();
            this.validation = $.extend(true, {},
                this.user.options.validate,
                {
                    submitHandler : function () {
                        return true;
                    }
                }
            );
            this.compare = new Compare();
            this.form = new Form('#form-user', {
                validate : this.validation,
            });
        }
    }
});
