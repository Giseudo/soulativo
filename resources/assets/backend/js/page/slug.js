define([

], function () {

    var PageSlug = function (options) {
        this.init(options);
    };

    PageSlug.defaults = {
        title : 'input[name="title"]',
        slug : 'input[name="slug"]'
    };

    PageSlug.prototype = {
        init : function (options) {
            var that = this;
            this.options = $.extend(true, {}, PageSlug.defaults, options);

            $(this.options.title).on('keyup', function () {
                var slug = that.slugify($(this).val());
                $(that.options.slug).val(slug);
            });
        },

        slugify : function (text) {
            return text.toString().toLowerCase().trim()
                .replace(/\s+/g, '-')           // Replace spaces with -
                .replace(/&/g, '-and-')         // Replace & with 'and'
                .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
                .replace(/\-\-+/g, '-');        // Replace multiple - with single -
        }
    };

    return PageSlug;
});
