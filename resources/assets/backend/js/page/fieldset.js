define([
    'language',
    'page/slug',
    'dante'
], function (Lang, Slug) {

    var PageFieldset= function (options) {
        this.init(options);
    };

    PageFieldset.defaults = {
        'validate' : {
            rules : {

            },
            messages : {

            }
        },
        dante : {
            el: "#page-editor",
            upload_url: "/media/upload/editor",
            store_url: ""
        }
    };

    PageFieldset.prototype = {
        init : function (options) {
            this.options = $.extend(true, {}, PageFieldset.defaults, options);
            this.slug = new Slug({
                title : 'input[name="page[title]"]',
                slug : 'input[name="page[slug]"]'
            });
            this.initEditor();
        },

        initEditor : function () {
            this.content = new Dante.Editor(this.options.dante);
            this.content.start();
        }
    };

    return PageFieldset;
});
