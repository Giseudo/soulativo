define([
    'language',
    'page/slug',
    'dante'
], function (Lang, Slug) {

    var PackageFieldset= function (options) {
        this.init(options);
    };

    PackageFieldset.defaults = {
        'validate' : {
            rules : {

            },
            messages : {

            }
        },
        dante : {
            el: "#terms-editor",
            upload_url: "/media/upload/editor",
            store_url: ""
        },
        'map_data' : '',
        'mapId' : 'package-destination-map',
        'searchBoxId' : 'package-destination',
        'dataInput' : 'input[name="package[map_data]"]'
    };

    PackageFieldset.prototype = {
        init : function (options) {
            var that = this;
            this.options = $.extend(true, {}, PackageFieldset.defaults, options);
            this.map_data = ($('#package-map_data').val() != '') ? $.parseJSON($('#package-map_data').val()) : '';
            this.slug = new Slug({
                title : 'input[name="product[name]"]',
                slug : 'input[name="product[slug]"]'
            });
            this.initMap();
            that.initEditor();
        },

        initEditor : function () {
            this.content = new Dante.Editor(this.options.dante);
            this.content.start();

            $('#page-editor').on('click', function () {
                $('#page-editor').removeClass('not-active');
                $('#terms-editor').addClass('not-active');
            });
        },

        initMap : function () {
            var that = this,
                input = document.getElementById(this.options.searchBoxId);

            this.map = new google.maps.Map(document.getElementById(this.options.mapId), {
                center: (this.map_data != '') ? this.map_data : {lat: -34.397, lng: 150.644},
                scrollwheel: false,
                zoom: 8
            });

            if(this.map_data.lat != null || this.map_data.lng != null){
                this.placeMarker(this.map_data);
            }

            this.searchBox = new google.maps.places.SearchBox(input);

            // On map Click, place a marker.
            google.maps.event.addListener(this.map, 'click', function(event) {
                that.placeMarker(event.latLng);
            });

            // Bias the SearchBox results towards current map's viewport.
            this.map.addListener('bounds_changed', function() {
                that.searchBox.setBounds(that.map.getBounds());
            });

            // Listen for the event fired when the user selects a prediction and retrieve more details for that place.
            this.searchBox.addListener('places_changed', function() {
                that.placeChanged();
            });
        },

        placeMarker : function (location) {
            if (this.marker) {
                this.marker.setPosition(location);
            } else {
                this.marker = new google.maps.Marker({
                    position: location,
                    map: this.map
                });
            }

            this.setInputData(this.marker);
        },

        placeChanged : function () {
            var that = this,
                places = this.searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }

            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds(),
                place = places[0],
                icon = {
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };

            // Create a marker for the place.
            that.placeMarker(place.geometry.location);

            if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }

            this.map.fitBounds(bounds);
        },

        setInputData : function (marker) {
            var data = JSON.stringify(marker.position);

            $(this.options.dataInput).val(data);
        }
    };

    return PackageFieldset;
});
