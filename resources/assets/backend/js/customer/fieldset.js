define([
    'language',
    'picker/date'
], function (Lang) {
    var CustomerFieldset = function (options) {
        this.init(options);
    };

    CustomerFieldset.defaults = {
        birthday : 'input[name="customer[birthday]"]',
        datepicker : {
            selectMonths: true,
            selectYears: 100,
            max: true
        },
        validate : {
            rules : {
                'customer[birthday]' : 'required',
                'customer[document]' : 'required'
            },
            messages : {
                'customer[birthday]' : Lang.validate.customer.birthday,
                'customer[document]' : Lang.validate.customer.document
            }
        }
    };

    /* Public Methods */
    CustomerFieldset.prototype = {
        init : function (options) {
            this.options = $.extend(true, {}, CustomerFieldset.defaults, options);
            this.datepicker(this.options.datepicker);
        },

        rules : function () {
            return this.options.validate.rules;
        },

        datepicker : function (options) {
            $(this.options.birthday).pickadate(options);
        }
    };

    return CustomerFieldset;
});
