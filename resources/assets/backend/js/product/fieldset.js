define([
    'language',
    'dante'
], function (Lang) {

    var ProductFieldset= function (options) {
        this.init(options);
    };

    ProductFieldset.defaults = {
        'validate' : {
            rules : {

            },
            messages : {

            }
        },
        dante : {
            el: "#page-editor",
            upload_url: "/media/upload/editor",
            store_url: ""
        }
    };

    ProductFieldset.prototype = {
        init : function (options) {
            this.options = $.extend(true, {}, ProductFieldset.defaults, options);
            this.initEditor();
        },

        initEditor : function () {
            this.content = new Dante.Editor(this.options.dante);
            this.content.start();

            $('#terms-editor').on('click', function () {
                $('#terms-editor').removeClass('not-active');
                $('#page-editor').addClass('not-active');
            });
        }
    };

    return ProductFieldset;
});
