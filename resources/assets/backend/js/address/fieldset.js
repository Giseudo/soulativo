define([
    'language',
    'picker/date'
], function (Lang) {

    var AddressFieldset = function (options) {
        this.init(options);
    };

    AddressFieldset.defaults = {
        'validate' : {
            rules : {
                'address[country_id]' : 'required',
                'address[state]' : 'required',
                'address[city]' : 'required',
                'address[zipcode]' : 'required',
                'address[street]' : 'required',
                'address[number]' : 'required',
                'address[neighbourhood]' : 'required'
            },
            messages : {
                'address[country_id]' : Lang.validate.address.country,
                'address[state]' : Lang.validate.address.state,
                'address[city]' : Lang.validate.address.city,
                'address[zipcode]' : Lang.validate.address.zipcode,
                'address[street]' : Lang.validate.address.street,
                'address[number]' : Lang.validate.address.number,
                'address[neighbourhood]' : Lang.validate.address.neighbourhood
            }
        }
    };

    AddressFieldset.prototype = {
        init : function (options) {
            this.options = $.extend(true, {}, AddressFieldset.defaults, options);
        }
    };

    return AddressFieldset;
});
