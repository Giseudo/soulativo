/*globals require */

require([
    'config',
    'async!//maps.google.com/maps/api/js?v=3&libraries=places'
], function (Config) {
    var view = $('body').data('view');

    if(view){
        require(['views/' + view], function (View) {
            View.init();
        });
    }
});
