define([
    'awesomplete'
], function () {

    var ListSearch= function (options) {
        this.init(options);
    };

    ListSearch.defaults = {
        form : '',
        redirect : '',
        autocomplete : null
    };

    ListSearch.prototype = {
        init : function (options) {
            var that = this,
                timer = 0;
            this.options = $.extend(true, {}, ListSearch.defaults, options);
            that.options.autocomplete = new Awesomplete(document.getElementById("autocomplete"), { list : ['Brazil'] });

            $('input[type="text"]', this.options.form).on('keyup', function (event) {
                var query = $(this).val();

                if (event.keyCode === 13) {
					if($(this).val() != ''){
						$(this).closest('form').submit();
					}
					return false;
				}

				if (event.keyCode === 37 ||
						event.keyCode === 38 ||
						event.keyCode === 39 ||
						event.keyCode === 40) {
					return false;
				}

                if (timer) {
                    clearTimeout(timer);
                }

                timer = setTimeout(function() {
					that.autocomplete(query);
				}, 400);
            });

            // window.addEventListener("awesomplete-select", function(e){
            //     console.log(e);
            // }, false);
        },

        autocomplete : function (query) {
            var that = this,
                url = $(this.options.form).prop('action') + "/" + query,
                data = {};

            $.ajax({
                url : url,
                type : 'GET',
                dataType : 'json',
                success : function(result){
                    var list = [];


                    for (i = 0; i < result.length; i++) {
                        list[result[i].id] = result[i].id = result[i].name;
                    }

                    console.log(list);

                    that.options.autocomplete.list = list;
                },
                complete : function(result){

                }
            });

            return data;
        }
    };

    return ListSearch;
});
