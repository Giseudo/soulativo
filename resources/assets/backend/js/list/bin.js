define([

], function () {

    var ListBin= function (options) {
        this.init(options);
    };

    ListBin.defaults = {
        form : ''
    };

    ListBin.prototype = {
        init : function (options) {
            this.options = $.extend(true, {}, ListBin.defaults, options);
            this.binInit(this.options.form);
        },

        //Initialize Bin
        binInit : function (form) {
            var that = this;

            $('table thead input[type="checkbox"]', form).on('click',
                function () {
                    that.checkAllRows(form, $(this).prop('checked'));
                }
            );

            $('> header .dropdown-menu button', form).on('click',
                function () {
                    var target = $(this).data('target'),
                        action = $(this).data('action');

                    that.changeAction(form, action, target);
                }
            );

            $(form).on('submit',
                function () {
                    that.ajaxPost(this);
                    return false;
                }
            );
        },

        //Check/Uncheck all the form rows
        checkAllRows : function (form, checked) {
            $('table tbody input[type="checkbox"]', form).prop('checked', checked);
        },

        //Update the form properties
        changeAction : function (form, action, target) {
            $(form).prop('action', action);
            $(form).prop('target', target);
        },

        //Post handler
        ajaxPost : function (form) {
            var url = $(form).prop('action');

            $.post(
                url,
                $(form).serialize(),
                null,
                'json'
            ).done(
                function (result) {
                    //Move the success entries
                    $.each(result.success_id, function () {
                        var id = this.valueOf(),
                            checked = $('table tbody input[value="'+ id +'"]:checked', form).closest('tr'),
                            target = $($(form).prop('target'));

                        //If there is a target
                        if (target.length > 0) {
                            $('table', target).removeClass('hidden-xs-up');
                            $('.alert', target).addClass('hidden-xs-up');

                            $('table tbody', target).prepend(checked);
                            $('input[type="checkbox"]', checked).prop('checked', false);
                        //If not, get rid of the node
                        } else {
                            $(checked).remove();
                        }
                    });

                    //Check for errors
                    if(result.error_id.length > 0){
                        alert("Sorry, some entries could not be processed.");
                        $.each(result.error_id, function (index) {
                            console.log("Error: " + result.exception[index]);
                        });
                    }
                }
            ).fail(
                function () {
                    console.log("There was a problem with the submit");
                }
            ).always(
                function () {
                    //
                }
            );
        }
    };

    return ListBin;
});
