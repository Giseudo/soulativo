define([

], function () {

    var ListFeatured = function (options) {
        this.init(options);
    };

    ListFeatured.defaults = {
        'action' : ''
    };

    ListFeatured.prototype = {
        init : function (options) {
            var that = this;
            this.options = $.extend(true, {}, ListFeatured.defaults, options);

            $('button.-featured').on('click', function () {
                that.setFeatured($(this));
                return false;
            });
        },

        setFeatured : function (element) {
            var target = $(element),
                featuredId = target.data('id');
                url = '//' + location.host + '/' + this.options.action + '/' + featuredId,

            $.ajax({
                url : url,
                type : 'GET',
                dataType : 'json',
                success : function(result){
                    $('button.-featured').removeClass('fa-star -active');
                    $('button.-featured').addClass('fa-star-o');
                    target.addClass('fa-star -active');
                    target.removeClass('fa-star-o');
                },
                complete : function(result){

                }
            });
        },

        reset : function () {
            var that = this;

            $('button.-featured').off('click');
            $('button.-featured').on('click', function () {
                that.setFeatured($(this));
                return false;
            });
        }
    };

    return ListFeatured;
});
