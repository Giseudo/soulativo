define([
    'language',
], function (Lang) {
    var OrderAside = function (options) {
        this.init(options);
    };

    OrderAside.defaults = {
        'validate' : {
            rules : {
                'order[customer_id]' : 'required'
            },
            messages : {

            }
        }
    };

    /* Public Methods */
    OrderAside.prototype = {
        init : function (options) {
            this.options = $.extend(true, {}, OrderAside.defaults, options);
        }
    };

    return OrderAside;
});
