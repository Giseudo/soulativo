define([
    'language',
    'picker/date'
], function (Lang) {

    var CouponFieldset= function (options) {
        this.init(options);
    };

    CouponFieldset.defaults = {
        'from' : 'input[name="coupon[from]"]',
        'to' : 'input[name="coupon[to]"]',
        'validate' : {
            rules : {

            },
            messages : {

            }
        }
    };

    CouponFieldset.prototype = {
        init : function (options) {
            this.options = $.extend(true, {}, CouponFieldset.defaults, options);
            $(this.options.from).pickadate();
            $(this.options.to).pickadate();
        }
    };

    return CouponFieldset;
});
