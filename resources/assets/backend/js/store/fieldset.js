define([
    'language',
    'page/slug'
], function (Lang, Slug) {

    var StoreFieldset= function (options) {
        this.init(options);
    };

    StoreFieldset.defaults = {
        'validate' : {
            rules : {

            },
            messages : {

            }
        }
    };

    StoreFieldset.prototype = {
        init : function (options) {
            this.options = $.extend(true, {}, StoreFieldset.defaults, options);
            this.slug = new Slug({
                title : 'input[name="store[name]"]',
                slug : 'input[name="store[slug]"]'
            });
        }
    };

    return StoreFieldset;
});
