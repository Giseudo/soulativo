define([
    'swiper',
    'form',
    'booking/fieldset',
    'config'
], function (Swiper, Form, Booking, Config) {
    return {
        init : function () {
            var that = this,
                swiper = new Swiper('.swiper-container', {
                    pagination: '.swiper-pagination',
                    paginationClickable: true,
                    spaceBetween: 20,
                }),
                booking = new Booking(),
                form = new Form('.package.-read form', {
                    validate : {
                        rules : {
                            'booking[date]' : 'required',
                            'booking[adults]' : 'required'
                        },
                        messages : {

                        },
                        submitHandler : function (form, event) {
                            if(!$(form).data('logged')){
                                $('#auth-modal').modal('show');
                                return false;
                            }else{
                                return true;
                            }
                        }
                    }
                }),
                callback = function () {
                    $('.package.-read form').data('logged', 1);
                    $('.package.-read form').trigger('submit');
                }

            Config.login.options.callback = callback;
            Config.register.options.callback = callback;

            $('.nav-tabs a[href="#map"]').on('show.bs.tab', function () {
                console.log('teste');
                if(that.map == null){
                    that.initMap();
                }
            });
        },

        initMap : function () {
            var data = $('#package-map').data('mapdata');

            this.map = new google.maps.Map(document.getElementById('package-map'), {
                center: {lat: data.lat, lng: data.lng},
                scrollwheel: false,
                zoom: 13
            });

            this.marker = new google.maps.Marker({
                position: {lat: data.lat, lng: data.lng},
                map: this.map
            });
        }
    }
});
