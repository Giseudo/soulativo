require([
    // Index
    'views/index/home',
    'views/index/404',
    'views/index/login',
    // Package
    'views/package/show',
    // Cart
    'views/cart/list',
    // Checkout
    'views/checkout/customer',
    'views/checkout/payment',
    // Blog
    'views/blog/list',
    // Customer
    'views/customer/edit'
]);
