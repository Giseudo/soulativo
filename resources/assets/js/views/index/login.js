define([
    'search'
], function (Search) {
    return {
        init : function () {
            var search = new Search();
            $('#auth-modal').modal('show');
        }
    }
});
