define([
    'form',
    'payment/pagseguro',
], function (Form, PagseguroFieldset) {
    return {
        init : function () {
            var that = this;

            this.pagseguro = new PagseguroFieldset();

            this.validation = $.extend(true, {},
                this.pagseguro.options.validate,
                {
                    rules : {
                        'payment[method]' : 'required'
                    },
                    submitHandler : function () {
                      if($('input[name="payment[method]"]:checked').val() == 'creditcard'){
                          that.pagseguro.getCardBrand();
                          return false;
                      }else{
                          return true;
                      }
                    }
                }
            );

            this.form = new Form('form.checkout', {
                validate : this.validation
            });

            $('input[name="payment[method]"]').on('change', function () {
                $('input[name="payment[method]"]').parent().removeClass('-active');
                $(this).parent().addClass('-active');

                if($(this).val() == 'creditcard'){
                    $('fieldset.creditcard').slideDown();
                }else{
                    $('fieldset.creditcard').slideUp();
                }
            });
        }
    }
});
