define([
    'booking/fieldset',
    'cart',
    'form',
    'picker/date'
], function (Booking, Cart, Form) {
    return {
        init : function () {
            var that = this,
              bookings = [];

            $('form.cart article').each(function(index) {
                var date = $('input.date', this),
                    hour = $('input.departure-hour', this),
                    location = $('input.departure-location', this),
                    adults = $('select.adults', this),
                    childs = $('select.childs', this);

                bookings[index] = new Booking({
                    'date' : 'input[name="' + date.attr('name') + '"]',
                    'hour' : 'input[name="' + hour.attr('name') + '"]',
                    'location' : 'input[name="' + location.attr('name') + '"]',
                    'adults' : 'select[name="' + adults.attr('name') + '"]',
                    'childs' : 'select[name="' + childs.attr('name') + '"]'
                });

                bookings[index].options.validate.rules[date.attr('name')] = 'required';

                that.validation = $.extend(true, that.validation,
                    bookings[index].options.validate
                );
            });

            this.cart = new Cart({
                items : bookings
            });

            this.validation = $.extend(true, this.validation,
                {
                    submitHandler : function () {
                        return true;
                    }
                }
            );

            this.form = new Form('form.cart', {
                validate : this.validation
            });
        }
    }
});
