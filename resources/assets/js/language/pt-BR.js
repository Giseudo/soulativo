define([], function () {
    return {
        validate : {
            address : {
                country : 'Selecione seu país',
                state : 'Preencha o estado',
                city : 'Preencha sua cidade',
                zipcode : 'Preencha seu CEP',
                street : 'Preencha sua rua',
                number : 'Obrigatório',
                neighbourhood : 'Preencha seu bairro'
            },
            customer : {
                birthday : 'Selecione a sua data de nascimento',
                document : 'Preencha seu número de documento'
            },
            user : {
                first_name : 'Preencha seu primeiro nome',
                last_name : 'Preencha seu sobrenome',
                email : 'Preencha um e-mail válido'
            }
        }
    }
});
