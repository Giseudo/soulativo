define([
    'config',
    'language/en-US',
    'language/pt-BR'
], function (Config) {
    return require('language/' + Config.locale);
});
