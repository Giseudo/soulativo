define([
    'language',
    'jquery-mask',
    'stripe'
], function (Lang) {
    var StripeFieldset = function (options) {
        this.validators();
        this.init(options);
    };

    StripeFieldset.defaults = {
        'validate' : {
            rules: {
                "card-cvc" : {
                    cardCVC: true,
                    required: true
                },
                "card-number" : {
                    cardNumber: true,
                    required: true
                },
                "card-expiry-year" : "cardExpiry",
                "card-holder" : 'required'
            },
            messages : {

            }
        }
    };

    /* Public Methods */
    StripeFieldset.prototype = {
        init : function (options) {
            var that = this;
            this.options = $.extend(true, {}, StripeFieldset.defaults, options);
            this.createMasks();
        },

        createMasks : function () {
            $('input[name="card-number"]').mask('0000-0000-0000-0000');
        },

        validators : function () {
            // add custom rules for credit card validating
            $.validator.addMethod("cardNumber", Stripe.validateCardNumber, "Please enter a valid card number");
            $.validator.addMethod("cardCVC", Stripe.validateCVC, "Please enter a valid security code");
            $.validator.addMethod("cardExpiry", function() {
                return Stripe.validateExpiry($(".card-expiry-month").val(),
                $(".card-expiry-year").val())
            }, "Please enter a valid expiration");
        },

        stripeResponseHandler : function (status, response) {
            var form = $('form.checkout');

            if (response.error) {
                // Show the errors on the form
                form.find('.payment-errors').text(response.error.message);
                form.find('button').prop('disabled', false);
            } else {
                // response contains id and card, which contains additional card details
                var token = response.id;
                // Insert the token into the form so it gets submitted to the server
                form.append($('<input type="hidden" name="stripeToken" />').val(token));
                // and submit
                form.get(0).submit();
            }
        }
    };

    return StripeFieldset;
});
