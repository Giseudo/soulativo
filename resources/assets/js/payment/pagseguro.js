define([
    'language',
    'moip',
    'jquery-mask'
], function (Lang) {
    var PagseguroFieldset = function (options) {
        this.validators();
        this.init(options);
    };

    PagseguroFieldset.defaults = {
        'clientId' : '',
        'paymentMethods' : '',
        'validate' : {
            rules: {
                "card-cvc" : {
                    cardCVC: true,
                    required: true
                },
                "card-number" : {
                    cardNumber: true,
                    required: true
                },
                "card-expiry-year" : "cardExpiry",
                "card-holder" : 'required'
            },
            messages : {

            }
        }
    };

    /* Public Methods */
    PagseguroFieldset.prototype = {
        init : function (options) {
            var that = this;
            this.options = $.extend(true, {}, PagseguroFieldset.defaults, options);
            this.createMasks();
            this.setClientId();
        },

        setClientId : function() {
          var that = this;

          $.ajax({
              url : '//' + location.host + '/pagseguro/id',
              type : 'GET',
              dataType : 'json',
              success : function(result){
                  PagSeguroDirectPayment.setSessionId(result.id);
                  that.options.clientId = result.id;
                  that.getPaymentMethods(result.id);
              }
          });
        },

        getPaymentMethods : function (id) {
          var that = this;

          PagSeguroDirectPayment.getPaymentMethods({
              amount : 500.00,
              success : function(response) {
                  that.options.paymentMethods = response;
              },
              error : function(response) {
                  //tratamento do erro
              },
              complete : function(response) {
                  //tratamento comum para todas chamadas
              }
          });
        },

        createMasks : function () {
            $('input[name="card-number"]').mask('0000-0000-0000-0000');
        },

        validators : function () {
            var that = this;

            // add custom rules for credit card validating
            $.validator.addMethod("cardNumber", moip.creditCard.isValid, "Please enter a valid card number");
            $.validator.addMethod("cardCVC", function () {
              return moip.creditCard.isSecurityCodeValid($('input[name="card-number"]').val(), $('input[name="card-cvc"]').val())
            }, "Please enter a valid security code");
            $.validator.addMethod("cardExpiry", function() {
                return moip.creditCard.isExpiryDateValid($(".card-expiry-month").val(), $(".card-expiry-year").val())
            }, "Please enter a valid expiration");
        },

        getCardBrand : function () {
            var that = this,
                card = $('input[name="card-number"]').val().replace(new RegExp("-", "g"), '');
                bin = card.substr(0, 6);

            PagSeguroDirectPayment.getBrand({
                cardBin : bin,
                success : function (response) {
                    // console.log(response);
                    that.createCardToken(card, response.brand);
                },
                error : function (response) {
                    // console.log(response);
                }
            })
        },

        createCardToken : function (card, brand) {
            // Response contains id and card, which contains additional card details
            var form = $('form.checkout');

            PagSeguroDirectPayment.createCardToken({
                cardNumber : card,
                cvv : $('input[name="card-cvc"]').val(),
                expirationMonth : parseInt($('.card-expiry-month').val()),
                expirationYear : parseInt($('.card-expiry-year').val()),
                brand : brand.name,
                success: function(response) {
                    // console.log(response);
                    // Token gerado, esse deve ser usado na chamada da API do Checkout Transparente
                    // Insert the token into the form so it gets submitted to the server
                    form.append($('<input type="hidden" name="cardToken" />').val(response.card.token));
                    form.append($('<input type="hidden" name="senderHash" />').val(PagSeguroDirectPayment.getSenderHash()));
                    form.get(0).submit();
                },
                error: function(response) {
                    //tratamento do erro
                    // console.log(response);
                },
                complete: function(response) {
                    //tratamento comum para todas chamadas
                }
            });
        }

        // stripeResponseHandler : function (status, response) {
        //     var form = $('form.checkout');
        //
        //     if (response.error) {
        //         // Show the errors on the form
        //         form.find('.payment-errors').text(response.error.message);
        //         form.find('button').prop('disabled', false);
        //     } else {
        //         // response contains id and card, which contains additional card details
        //         var token = response.id;
        //         // Insert the token into the form so it gets submitted to the server
        //         form.append($('<input type="hidden" name="stripeToken" />').val(token));
        //         // and submit
        //         form.get(0).submit();
        //     }
        // }
    };

    return PagseguroFieldset;
});


// var param = {
//   cardNumber: $("input#cartao").val(),
//   cvv: $("input#cvv").val(),
//   expirationMonth: $("input#validadeMes").val(),
//   expirationYear: $("input#validadeAno").val(),
//   success: function(response) {
//       //token gerado, esse deve ser usado na chamada da API do Checkout Transparente
//     },
//   error: function(response) {
//       //tratamento do erro
//     },
//   complete: function(response) {
//       //tratamento comum para todas chamadas
//     }
// }
// //parâmetro opcional para qualquer chamada
// if($("input#bandeira").val() != '') {
//   param.brand = $("input#bandeira").val();
// }
// PagSeguroDirectPayment.createCardToken(param);
