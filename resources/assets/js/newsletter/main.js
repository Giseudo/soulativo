define([
    'form'
], function (Form) {
	/* Private variables */
	var Newsletter = function (options) {
			this.init(options);
		};

    Newsletter.defaults = {
        validate : {
            rules : {
                email : 'required'
            },
            submitHandler : function (form) {
                var url = $(form).prop('action');
                console.log(url);

                $.ajax({
                    url : url,
                    type : 'POST',
                    data : $(form).serialize(),
                    dataType : 'json',
                    success : function(result){
                        alert(result.msg);
                    }
                });

                return false;
            }
        }
	};

    Newsletter.prototype = {
		/* Public methods */
		init : function(options){
			this.options = $.extend(true, {}, Newsletter.defaults, options);
            this.form = new Form('form.newsletter', {
                validate : this.options.validate
            });
		}
	};

	return Newsletter;
});
