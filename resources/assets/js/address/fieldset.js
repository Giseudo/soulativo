define([
    'language'
], function (Lang) {

    var AddressFieldset = function (options) {
        this.init(options);
    };

    AddressFieldset.defaults = {
        'zipcode' : 'input[name="address[zipcode]"]',
        'validate' : {
            rules : {
                'address[country_id]' : 'required',
                'address[state]' : 'required',
                'address[city]' : 'required',
                'address[zipcode]' : 'required',
                'address[street]' : 'required',
                'address[number]' : 'required',
                'address[neighbourhood]' : 'required'
            },
            messages : {
                'address[country_id]' : Lang.validate.address.country,
                'address[state]' : Lang.validate.address.state,
                'address[city]' : Lang.validate.address.city,
                'address[zipcode]' : Lang.validate.address.zipcode,
                'address[street]' : Lang.validate.address.street,
                'address[number]' : Lang.validate.address.number,
                'address[neighbourhood]' : Lang.validate.address.neighbourhood
            }
        },
        'customer' : null
    };

    AddressFieldset.prototype = {
        init : function (options) {
            var that = this;
            this.options = $.extend(true, {}, AddressFieldset.defaults, options);

            if(that.options.customer != null){
                $('select[name="address[country_id]"]').on('change', function () {
                    var callingCode = $(this).find(':selected').data('calling-code');
                    that.options.customer.createMasks(callingCode);

                    // Erases the birthday everytime the country changes
                    $(that.options.customer.options.birthday).val('');

                    if (callingCode == 55) {
                        $('#address-state').prop('disabled', true);
                        $('#address-state-brazil').prop('disabled', false);
                        that.zipcodeMask();
                    } else {
                        $('#address-state-brazil').prop('disabled', true);
                        $('#address-state').prop('disabled', false);
                        $(that.options.zipcode).unmask();
                    }
                });
            }

            this.zipcodeMask();
        },

        zipcodeMask : function () {
            $(this.options.zipcode).mask('00000-000');
        }
    };

    return AddressFieldset;
});
