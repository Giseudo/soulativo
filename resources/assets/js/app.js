/*globals require */

require([
    'config',
    'auth/login',
    'auth/register',
    'newsletter',
    'main-menu',
    'async!//maps.google.com/maps/api/js?v=3&libraries=places'
], function (Config, Login, Register, Newsletter, MainMenu) {
    var view = $('body').data('view');

    // Load current view
    if(view){
        require(['views/' + view], function (View) {
            View.init();
        });
    }

    Config.login = new Login('#auth-modal form.login');
    Config.register = new Register('#auth-modal form.register');
    Config.newsletter = new Newsletter();
    Config.mainMenu = new MainMenu();
});
