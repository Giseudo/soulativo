define([

], function () {
    var Cart = function (options) {
        this.init(options);
    };

    Cart.defaults = {
        element : '.cart .-list',
        items : []
    };

    Cart.prototype = {
        init : function (options) {
            var that = this;
            this.options = $.extend(true, {}, Cart.defaults, options);

            $('button.remove', this.options.element).on('click', function () { that.removeFromCart(this); return false; } );

            $.each(this.options.items, function () {
                var item = this;
                $(item.options.adults + ', ' + item.options.childs).on('change', function () {
                    var parent = $(this).closest('article');
                    that.calculateSubtotal(parent.data('product-id'), parent.find(item.options.adults).val(), parent.find(item.options.childs).val(), parent);
                });
            });
        },

        calculateSubtotal: function (id, adults, childs, parent) {
            var that = this,
                url = '/product/' + id + '/subtotal/' + adults + '/' + childs;

            $.ajax({
                url : url,
                type : 'GET',
                dataType : 'json',
                success : function(result){
                    $('.text-adults', parent).html(adults);
                    $('.text-childs', parent).html(childs);
                    $('.text-subtotal', parent).html(result);
                },
                complete : function () {
                    that.calculateTotal();
                }
            });
        },

        calculateTotal : function () {
            var total = 0;
            $('.text-subtotal').each(function () {
                total = total + Number($(this).text());
            });
            $('.text-total').html(total);

            if(total == 0){
                $('form.cart footer').animate({ opacity : 0 }, function () {
                    $(this).slideUp();
                });
            }
        },

        removeFromCart : function (clicked) {
            var that = this,
                target = $(clicked).closest('article');
            target.animate({ opacity : 0 }, function () {
                $(this).slideUp(500, function () {
                    $(this).empty();
                    $(this).addClass('removed');
                    that.countItems();
                    that.calculateTotal();
                });
            });
            this.destroyCart($(clicked).attr('rel'));
        },

        destroyCart : function (id) {
            var url = '//' + location.host + '/cart/destroy';

            $.ajax({
                url : url,
                type : 'POST',
                data : {
                    CartID : id,
                    _token : $('input[name="_token"]').val()
                },
                dataType : 'json',
                success : function(result){
                    console.log('Success');
                },
                complete : function(result){

                }
            });
        },

        countItems : function () {
            $('article:not(.removed)', this.options.element).each(function (index) {
                $('figure > span', this).html(index + 1);
            });
        }
    };

    return Cart;
});
