define([

], function () {
    var MainMenu = function (options) {
        this.init(options);
    };

    MainMenu.defaults = {
        button : '#smartphone-menu button.hamburger'
    };

    /* Public Methods */
    MainMenu.prototype = {
        init : function (options) {
            this.options = $.extend(true, {}, MainMenu.defaults, options);

            $(this.options.button).on('click', function () {
                $('#main-top').toggleClass('is-open');
                $(this).toggleClass('is-active');
            });
        }
    };

    return MainMenu;
});
