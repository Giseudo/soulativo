<?php

use Illuminate\Database\Seeder;

class LocalesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('locales')->insert([
            'code' => 'pt-BR',
            'name' => 'Português (Brasil)'
        ]);

        DB::table('locales')->insert([
            'code' => 'en-US',
            'name' => 'English (International)'
        ]);
    }
}
