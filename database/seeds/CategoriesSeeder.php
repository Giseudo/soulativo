<?php

use Illuminate\Database\Seeder;

use App\Models\Category;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Packages
        $packages = factory(Category::class)->make(['name' => 'Packages', 'slug' => 'packages', 'locale' => 'en-US']);
        $parent_packages = Category::orderBy('id', 'DESC')->get()->first()->id;
        $pacotes = factory(Category::class)->make(['name' => 'Pacotes', 'slug' => 'pacotes', 'locale' => 'pt-BR', 'category_id' => $parent_packages]);

            // Adventure
            $adventure = factory(Category::class)->make(['name' => 'Adventure', 'slug' => 'adventure', 'locale' => 'en-US', 'parent_id' => $parent_packages]);
            $parent_adventure = Category::orderBy('id', 'DESC')->get()->first()->id;
            $aventura = factory(Category::class)->make(['name' => 'Aventura', 'slug' => 'aventura', 'locale' => 'pt-BR', 'category_id' => $parent_adventure]);

                // Hand gliding
                $hangGliding = factory(Category::class)->make(['name' => 'Hang gliding', 'slug' => 'health', 'locale' => 'en-US', 'parent_id' => $parent_adventure]);
                $parent_hangGliding = Category::orderBy('id', 'DESC')->get()->first()->id;
                $asaDelta = factory(Category::class)->make(['name' => 'Asa delta', 'slug' => 'asa-delta', 'locale' => 'pt-BR', 'category_id' => $parent_hangGliding, 'parent_id' => $parent_adventure]);

                // Paragliding
                $paragliding = factory(Category::class)->make(['name' => 'Paragliding', 'slug' => 'paragliding', 'locale' => 'en-US', 'parent_id' => $parent_adventure]);
                $parent_paragliding = Category::orderBy('id', 'DESC')->get()->first()->id;
                $parapente = factory(Category::class)->make(['name' => 'Parapente', 'slug' => 'parapente', 'locale' => 'pt-BR', 'category_id' => $parent_paragliding, 'parent_id' => $parent_adventure]);

                // Parachuting
                $parachuting = factory(Category::class)->make(['name' => 'Parachuting', 'slug' => 'parachuting', 'locale' => 'en-US', 'parent_id' => $parent_adventure]);
                $parent_parachuting = Category::orderBy('id', 'DESC')->get()->first()->id;
                $paraquedismo = factory(Category::class)->make(['name' => 'Paraquedismo', 'slug' => 'paraquedismo', 'locale' => 'pt-BR', 'category_id' => $parent_parachuting, 'parent_id' => $parent_adventure]);

                // Abseiling
                $abseiling = factory(Category::class)->make(['name' => 'Abseiling', 'slug' => 'abseiling', 'locale' => 'en-US', 'parent_id' => $parent_adventure]);
                $parent_abseiling = Category::orderBy('id', 'DESC')->get()->first()->id;
                $rapel = factory(Category::class)->make(['name' => 'Rapel', 'slug' => 'rapel', 'locale' => 'pt-BR', 'category_id' => $parent_abseiling, 'parent_id' => $parent_adventure]);

                // Stand Up Paddle
                $standUpPaddle = factory(Category::class)->make(['name' => 'Stand up paddle', 'slug' => 'stand-up-paddle', 'locale' => 'en-US', 'parent_id' => $parent_adventure]);
                $parent_standUpPaddle = Category::orderBy('id', 'DESC')->get()->first()->id;
                $standUpPaddle = factory(Category::class)->make(['name' => 'Stand up paddle', 'slug' => 'stand-up-paddle', 'locale' => 'pt-BR', 'category_id' => $parent_standUpPaddle, 'parent_id' => $parent_adventure]);

                // Tracks
                $tracks = factory(Category::class)->make(['name' => 'Tracks', 'slug' => 'tracks', 'locale' => 'en-US', 'parent_id' => $parent_adventure]);
                $parent_tracks = Category::orderBy('id', 'DESC')->get()->first()->id;
                $trilhas = factory(Category::class)->make(['name' => 'Trilhas', 'slug' => 'trilhas', 'locale' => 'pt-BR', 'category_id' => $parent_tracks, 'parent_id' => $parent_adventure]);

                // Diving
                $diving = factory(Category::class)->make(['name' => 'Diving', 'slug' => 'diving', 'locale' => 'en-US', 'parent_id' => $parent_adventure]);
                $parent_diving = Category::orderBy('id', 'DESC')->get()->first()->id;
                $mergulho = factory(Category::class)->make(['name' => 'Mergulho', 'slug' => 'mergulho', 'locale' => 'pt-BR', 'category_id' => $parent_diving, 'parent_id' => $parent_adventure]);

            // City Tour
            $cityTour = factory(Category::class)->make(['name' => 'City tour', 'slug' => 'city-tour', 'locale' => 'en-US', 'parent_id' => $parent_packages]);
            $parent_cityTour = Category::orderBy('id', 'DESC')->get()->first()->id;
            $cityTour = factory(Category::class)->make(['name' => 'City tour', 'slug' => 'city-tour', 'locale' => 'pt-BR', 'category_id' => $parent_cityTour, 'parent_id' => $parent_packages]);

                // Air
                $air = factory(Category::class)->make(['name' => 'Air', 'slug' => 'air', 'locale' => 'en-US', 'parent_id' => $parent_cityTour]);
                $parent_air = Category::orderBy('id', 'DESC')->get()->first()->id;
                $aereo = factory(Category::class)->make(['name' => 'Aéreo', 'slug' => 'aereo', 'locale' => 'pt-BR', 'category_id' => $parent_air, 'parent_id' => $parent_cityTour]);

                // Aquatic
                $aquatic = factory(Category::class)->make(['name' => 'Aquatic', 'slug' => 'aquatic', 'locale' => 'en-US', 'parent_id' => $parent_cityTour]);
                $parent_aquatic = Category::orderBy('id', 'DESC')->get()->first()->id;
                $aquatico = factory(Category::class)->make(['name' => 'Aquático', 'slug' => 'aquatico', 'locale' => 'pt-BR', 'category_id' => $parent_aquatic, 'parent_id' => $parent_cityTour]);

                // Land
                $land = factory(Category::class)->make(['name' => 'Aquatic', 'slug' => 'aquatic', 'locale' => 'en-US', 'parent_id' => $parent_cityTour]);
                $parent_land = Category::orderBy('id', 'DESC')->get()->first()->id;
                $terrestre = factory(Category::class)->make(['name' => 'Aquático', 'slug' => 'aquatico', 'locale' => 'pt-BR', 'category_id' => $parent_land, 'parent_id' => $parent_cityTour]);

            // Transport
            $transport = factory(Category::class)->make(['name' => 'Transport', 'slug' => 'transport', 'locale' => 'en-US', 'parent_id' => $parent_packages]);
            $parent_transport = Category::orderBy('id', 'DESC')->get()->first()->id;
            $transporte = factory(Category::class)->make(['name' => 'Transporte', 'slug' => 'transporte', 'locale' => 'pt-BR', 'category_id' => $parent_transport, 'parent_id' => $parent_packages]);

                // Transport
                $transfers = factory(Category::class)->make(['name' => 'Transfers', 'slug' => 'transfers', 'locale' => 'en-US', 'parent_id' => $parent_transport]);
                $parent_transfers = Category::orderBy('id', 'DESC')->get()->first()->id;
                $translados = factory(Category::class)->make(['name' => 'Translados', 'slug' => 'translados', 'locale' => 'pt-BR', 'category_id' => $parent_transfers, 'parent_id' => $parent_transport]);

                // Transport VIP
                $transfersVIP = factory(Category::class)->make(['name' => 'Transport VIP', 'slug' => 'transport-vip', 'locale' => 'en-US', 'parent_id' => $parent_transport]);
                $parent_transfersVIP = Category::orderBy('id', 'DESC')->get()->first()->id;
                $transladosVIP = factory(Category::class)->make(['name' => 'Transporte VIP', 'slug' => 'transporte-vip', 'locale' => 'pt-BR', 'category_id' => $parent_transfersVIP, 'parent_id' => $parent_transport]);

        // Posts
        $posts = factory(Category::class)->make(['name' => 'Posts', 'slug' => 'posts', 'locale' => 'en-US']);
        $parent_posts = Category::orderBy('id', 'DESC')->get()->first()->id;
        $posts = factory(Category::class)->make(['name' => 'Posts', 'slug' => 'posts', 'locale' => 'pt-BR', 'category_id' => $parent_posts]);

            // News
            $news = factory(Category::class)->make(['name' => 'News', 'slug' => 'news', 'locale' => 'en-US', 'parent_id' => $parent_posts]);
            $parent_news = Category::orderBy('id', 'DESC')->get()->first()->id;
            $noticias = factory(Category::class)->make(['name' => 'Notícias', 'slug' => 'noticias', 'locale' => 'pt-BR', 'category_id' => $parent_news, 'parent_id' => $parent_posts]);

            // Health
            $health = factory(Category::class)->make(['name' => 'Health', 'slug' => 'health', 'locale' => 'en-US', 'parent_id' => $posts->get()->first()->id]);
            $parent_health = Category::orderBy('id', 'DESC')->get()->first()->id;
            $bemEstar = factory(Category::class)->make(['name' => 'Bem estar', 'slug' => 'bem-estar', 'locale' => 'pt-BR', 'category_id' => $parent_health, 'parent_id' => $parent_posts]);
    }
}
