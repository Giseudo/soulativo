<?php

use Illuminate\Database\Seeder;
use App\Models\Package;
use App\Models\Product;
use App\Models\ProductTranslation;

class PackagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $product = new Product();
        $product->name = 'Hello world';
        $product->slug = 'hello-world';
        $product->excerpt = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam varius nisl non tempor feugiat. Nulla ligula ex, elementum sollicitudin nibh pharetra, porttitor consequat libero.';
        $product->content = '<h2>Lorem upsum sit amet</h2><p>Praesent sit amet arcu eget diam pharetra tristique sed eget enim. Nam molestie elementum lorem, pulvinar lacinia eros eleifend at. Suspendisse nec ligula sit amet massa aliquet tempor quis sed nibh. Aenean lobortis tellus facilisis ex aliquam scelerisque.</p>';
        $product->price = 100;
        $product->store_id = 1;
        $product->save();
        $product->categories()->sync([ 1 ]);

        $translation = ProductTranslation::where('product_id', $product->id)->first();
        $translation->locale = 'pt-BR';
        $translation->save();

        $package = new Package();
        $package->product_id = $product->id;
        $package->featured = 1;
        $package->map_data = '{"lat":0,"lng":0}';
        $package->gallery = '{ "images" : [] }';
        $package->save();
    }
}
