<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('key')->unique();
            $table->boolean('type')->default(0);
            $table->boolean('status')->default(0);
            $table->float('value');

            $table->timestamps();
            $table->softDeletes();

            $table->engine = 'InnoDB';
        });
        
        Schema::table('orders', function(Blueprint $table) {
            $table->integer('coupon_id')->unsigned()->nullable()->after('id');
            $table->foreign('coupon_id')->references('id')->on('coupons')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('coupons');
        Schema::table('orders', function ($table) {
            $table->dropColumn('coupon_id');
        });
    }
}
