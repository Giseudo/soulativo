<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medias', function ($table) {
            $table->increments('id');
            $table->string('filename');
            $table->text('size');

            $table->timestamps();
            $table->softDeletes();
        });

        /*
         * Translations
         * */
        Schema::create('media_translations', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('media_id')->unsigned();
            $table->string('locale')->index();
            $table->string('title');
            $table->text('caption');

            $table->engine = 'InnoDB';
            $table->foreign('media_id')->references('id')->on('medias')->onDelete('cascade');
            $table->foreign('locale')->references('code')->on('locales');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('medias');
        Schema::drop('media_translations');
    }
}
