<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('package_id')->unsigned();
            $table->integer('order_id')->unsigned();
            $table->integer('status')->default(1);
            $table->float('total');
            $table->float('discount');
            $table->integer('adults');
            $table->integer('childs')->nullable();
            $table->string('departure_location')->nullable();
            $table->time('departure_hour')->nullable();

            $table->timestamp('date');
            $table->timestamps();
            $table->softDeletes();

            $table->engine = 'InnoDB';
            $table->foreign('order_id')->references('id')->on('orders');
            $table->foreign('package_id')->references('id')->on('packages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bookings');
    }
}
