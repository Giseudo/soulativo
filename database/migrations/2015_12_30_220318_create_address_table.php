<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id')->index();
            $table->string('state');
            $table->string('zipcode');
            $table->string('city');
            $table->string('street');
            $table->string('number');
            $table->string('neighbourhood');
            $table->string('complement');

            $table->timestamps();

            $table->engine = 'InnoDB';
            $table->foreign('country_id')->references('id')->on('countries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('countries');
        Schema::drop('addresses');
    }
}
