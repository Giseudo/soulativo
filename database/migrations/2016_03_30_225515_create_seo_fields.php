<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeoFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('keywords');
            $table->string('description');

            $table->timestamps();

            $table->engine = 'InnoDB';
        });

        Schema::table('posts', function(Blueprint $table) {
            $table->integer('seo_id')->unsigned()->nullable()->after('thumbnail_id');
            $table->foreign('seo_id')->references('id')->on('seo')->onDelete('set null');
        });

        Schema::table('pages', function(Blueprint $table) {
            $table->integer('seo_id')->unsigned()->nullable()->after('thumbnail_id');
            $table->foreign('seo_id')->references('id')->on('seo')->onDelete('set null');
        });

        Schema::table('packages', function(Blueprint $table) {
            $table->integer('seo_id')->unsigned()->nullable()->after('thumbnail_id');
            $table->foreign('seo_id')->references('id')->on('seo')->onDelete('set null');
        });

        Schema::table('categories', function(Blueprint $table) {
            $table->integer('seo_id')->unsigned()->nullable()->after('thumbnail_id');
            $table->foreign('seo_id')->references('id')->on('seo')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('promotions');

        Schema::table('posts', function ($table) {
            $table->dropColumn('seo_id');
        });

        Schema::table('pages', function ($table) {
            $table->dropColumn('seo_id');
        });

        Schema::table('packages', function ($table) {
            $table->dropColumn('seo_id');
        });

        Schema::table('categories', function ($table) {
            $table->dropColumn('seo_id');
        });
    }
}
