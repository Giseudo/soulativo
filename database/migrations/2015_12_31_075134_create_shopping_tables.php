<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShoppingTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('address_id')->nullable();
            $table->string('name');
            $table->string('slug');
            $table->string('document');

            $table->timestamps();
            $table->softDeletes();

            $table->engine = 'InnoDB';
        });

        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('store_id')->unsigned();
            $table->float('price');

            $table->timestamps();
            $table->softDeletes();

            $table->engine = 'InnoDB';
        });

        Schema::create('categories', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->nullable()->index();
            $table->integer('thumbnail_id')->nullable()->unsigned();
            $table->integer('lft')->nullable()->index();
            $table->integer('rgt')->nullable()->index();
            $table->integer('depth')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->engine = 'InnoDB';
            $table->foreign('thumbnail_id')->references('id')->on('medias')->onDelete('set null');
        });

        Schema::create('packages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->integer('thumbnail_id')->nullable()->unsigned();
            $table->boolean('featured');
            $table->string('destination');
            $table->text('map_data');
            $table->text('gallery');

            $table->timestamps();
            $table->softDeletes();

            $table->engine = 'InnoDB';
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->foreign('thumbnail_id')->references('id')->on('medias')->onDelete('set null');
        });

        Schema::create('category_product', function (Blueprint $table) {
            $table->integer('product_id')->unsigned();
            $table->integer('category_id')->unsigned();

            $table->engine = 'InnoDB';
            $table->primary(['product_id', 'category_id']);
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
        });

        Schema::create('package_translations', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('package_id')->unsigned();
            $table->string('locale')->index();
            $table->text('terms');

            $table->engine = 'InnoDB';
            $table->foreign('package_id')->references('id')->on('packages')->onDelete('cascade');
            $table->foreign('locale')->references('code')->on('locales');
        });

        /*
         * Translations
         * */

        Schema::create('product_translations', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->string('locale')->index();
            $table->string('name');
            $table->string('slug');
            $table->text('content');
            $table->text('excerpt');

            $table->engine = 'InnoDB';
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->foreign('locale')->references('code')->on('locales');
        });

        Schema::create('category_translations', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->string('locale')->index();
            $table->string('name');
            $table->string('slug');

            $table->engine = 'InnoDB';
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->foreign('locale')->references('code')->on('locales');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stores');
        Schema::drop('products');
        Schema::drop('packages');
        Schema::drop('categories');
        Schema::drop('category_package');
        Schema::drop('product_translations');
        Schema::drop('package_translations');
        Schema::drop('category_translations');
    }
}
