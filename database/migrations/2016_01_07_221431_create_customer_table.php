<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->integer('id')->unsigned();
            $table->integer('address_id')->unsigned();
            $table->string('stripe_id')->nullable();
            $table->string('document')->nullable();
            $table->timestamp('birthday')->nullable();
            $table->string('telephone')->nullable();
            $table->string('card_brand')->nullable();
            $table->string('card_last_four')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->engine = 'InnoDB';
            $table->foreign('id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('address_id')->references('id')->on('addresses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customers');
    }
}
