<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function ($table) {
            $table->increments('id');
            $table->integer('thumbnail_id')->nullable()->unsigned();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('thumbnail_id')->references('id')->on('medias')->onDelete('set null');
        });

        /*
         * Translations
         * */
        Schema::create('page_translations', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('page_id')->unsigned();
            $table->string('locale')->index();
            $table->string('title');
            $table->string('slug');
            $table->text('content');
            $table->text('excerpt');

            $table->engine = 'InnoDB';
            $table->unique(['slug']);
            $table->foreign('page_id')->references('id')->on('pages')->onDelete('cascade');
            $table->foreign('locale')->references('code')->on('locales');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pages');
        Schema::drop('page_translations');
    }
}
