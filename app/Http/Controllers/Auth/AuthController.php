<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\Http\Controllers\Auth\AuthenticateSocial;
use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\User;
use Validator;
use Auth;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, AuthenticateSocial, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password'])
        ]);

        return $user;
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postLogin(Request $request)
    {
        $input = $request->all();
        $response = $this->login($request);
        $error = false;

        // Check if user is logged in
        if (!Auth::check()) {
            $error = true;
        }

        echo json_encode([
            'error' => $error,
            'redirect' => (!empty($input['redirectUrl'])) ? '/' . $input['redirectUrl'] : $this->redirectTo
        ]);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postRegister(Request $request)
    {
        try{
            $this->register($request);
        }catch(\Exception $err){
            return json_encode([
                'error' => true,
                'message' => $err->getMessage(),
                'redirect' => null
            ]);
        }

        $error = false;

        // Check if user is logged in
        if (!Auth::check()) {
            $error = true;
        }
        
        $user = Auth::user();
        $user->addRole('customer');

        return json_encode([
            'error' => $error,
            'message' => null,
            'redirect' => $this->redirectTo
        ]);
    }

}
