<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Customer;
use App\Models\Address;
use Countries;
Use Carbon\Carbon;

class CustomerController extends Controller
{
    /**
     * Display a listing of the user.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $frontend['view'] = 'customer/list';
        $perPage = 10;
        $customers = User::findByRole(3)->take($perPage);
        $deleted_users = User::findByRole(3, true);

        return view('admin.customer.index')->with('customers', $customers)
            ->with('deleted_users', $deleted_users)
                ->with('breadcrumb', 'customer')
                ->with('perPage', $perPage)
            ->with('frontend', $frontend);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $frontend['view'] = 'customer/form';

        $countries[null] = trans('model.country.select');
        foreach(Countries::getList() as $key => $country){
            $countries[$key] = $country['name'];
        }

        return view('admin.customer.create')
            ->with('frontend', $frontend)
                ->with('breadcrumb', 'customer.create')
            ->with('countries', $countries);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
            array_merge(
                User::getValidation(),
                Customer::getValidation(),
                ['user.password' => 'required|confirmed|min:6']
            )
        );
        $input = $request->all();

        $input['user']['password'] = bcrypt($input['user']['password']);

        $carbon = new Carbon($input['customer']['birthday']);
        $input['customer']['birthday'] = $carbon->toDateString();

        // Saves user
        $user = User::create($input['user']);
        $user->addRole('customer');

        //Saves Address
        $address = new Address();
        $address->fill($input['address']);
        $address->country_id = $input['address']['country_id'];
        $address->save();
        $address_id = $address->getKey();

        // Saves Customer
        $customer = new Customer();
        $customer->fill($input['customer']);
        $customer->id = $user->id;
        $customer->address_id = $address_id;
        $customer->document = $input['customer']['document'];
        $customer->save();

        $request->session()->flash('status', trans('model.customer.create'));
        return redirect('admin/customer/edit/' . $user->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $frontend['view'] = 'customer/form';

        $customer = Customer::findOrFail($id);
        $customer->customer = $customer;

        $countries[null] = trans('model.country.select');
        foreach(Countries::getList() as $key => $country){
            $countries[$key] = $country['name'];
        }

        return view('admin.customer.edit')
            ->with('frontend', $frontend)
            ->with('customer', $customer)
            ->with('breadcrumb', 'customer.edit')
            ->with('model', $customer)
            ->with('user', $customer->user)
            ->with('address', $customer->address)
            ->with('orders', $customer->orders)
            ->with('countries', $countries);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $customer = Customer::find($id);
        $address = Address::find($request['customer']['address_id']);

        $this->validate($request, array_merge(
                User::getValidation(),
                Customer::getValidation()
            ));
        $input = $request->all();
        $input['user']['password'] = bcrypt($input['user']['password']);

        $carbon = new Carbon($input['customer']['birthday']);
        $input['customer']['birthday'] = $carbon->toDateString();

//        dd($input['customer']);

        // Updates user
        $user->fill($input['user'])->save();
        //Update Address
        $address->fill($input['address'])->save();
        // Updates Costumer
        $customer->fill($input['customer'])->save();

        $request->session()->flash('status', trans('model.customer.edit'));
        return redirect()->back();
    }

    /**
     * Remove the specified user from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
     {
         //
     }

     /**
      * Remove multiple entries of users from storage.
      *
      * @param  Request  $request
      * @return \Illuminate\Http\Response
      */
     public function destroyMultiple(Request $request)
     {
         if($request->ajax()) {
             $success = [];
             $error = [];
             $exception = [];

            foreach($request['checkbox'] as $list){
                if(isset($request['trash_delete'])){
                    $user = User::onlyTrashed()->find($list);
                }else{
                    $user = User::find($list);
                }

                if(!$user){
                    $error[] = $list;
                    continue;
                }
                if(isset($request['trash_delete'])){
                    try
                    {
                        $user->forceDelete();
                    }
                    catch(Exception $ex)
                    {
                       $error[] = $list;
                       $exception[] = $ex->getMessage();
                       continue;
                    }
                    if(!isset($ex)){
                        $success[] = $user->id;
                    }
                }else{
                    if(!$user->destroy($list)){
                        $error[] = $list;
                        continue;
                    }else{
                        $success[] = $user->id;
                    }
                }
            }
            echo json_encode([
                'input' => $request->all(),
                'success_id' => $success,
                'exception' => $exception,
                'error_id' => $error
            ]);
        }
    }

    public function restore(Request $request)
     {
         if($request->ajax()) {
            $success = [];
            $error = [];
            foreach($request['checkbox'] as $list){
                $user = User::withTrashed()->where('id', $list);
                if(!$user->restore()){
                $error[] = $list;
                        continue;
                }else{
                    $success[] = $list;
                }
            }
            echo json_encode([
                'error' => false, //or true
                'input' => $request->all(),
                'success_id' => $success,
                'error_ids' => $error
            ]);
        }
    }
}
