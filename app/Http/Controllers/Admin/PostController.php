<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Post;
use App\Models\PostTranslation;
use App\Models\Category;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $frontend['view'] = 'post/list';
        $perPage = 10;
        $posts = Post::all()->take($perPage);
        $deleted_posts = Post::onlyTrashed()->get();

        return view('admin/post/index')
            ->with('frontend', $frontend)
            ->with('breadcrumb', 'post')
            ->with('perPage', $perPage)
            ->with('posts', $posts)
            ->with('deleted_posts', $deleted_posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $frontend['view'] = 'post/form';
        $root = Category::findBySlug(Post::$root_slug);
        $categories = $root->getDescendants();

        return view('admin/post/create')
            ->with('frontend', $frontend)
            ->with('breadcrumb', 'post.create')
            ->with('root', $root)
            ->with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $this->validate($request,
            Post::getValidation()
        );

        $post = Post::create();
        $post->fill($input['post']);
        $post->save();
        $post->categories()->sync((!empty($input['post']['categories'])) ? $input['post']['categories'] : []);

        $request->session()->flash('status', trans('model.post.create'));
        return redirect('admin/post/edit/' . $post->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $frontend['view'] = 'post/form';
        $post = Post::find($id);
        $root = Category::findBySlug(Post::$root_slug);
        $post->post = $post;
        $categories = $root->getDescendants();
//        dd($categories);

        return view('admin/post/edit')
            ->with('frontend', $frontend)
            ->with('breadcrumb', 'post.edit')
            ->with('model', $post)
            ->with('post', $post)
            ->with('root', $root)
            ->with('categories', $categories);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $post = Post::findOrFail($id);

        $this->validate($request,
            Post::getValidation()
        );

        // Updates Post
        $post->fill($input['post']);
        $post->categories()->sync((!empty($input['post']['categories'])) ? $input['post']['categories'] : []);
        $post->save();

        $request->session()->flash('status', trans('model.post.edit'));
        return redirect()->back();
    }

    /**
     * Remove the specified Post from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
     {
         //
     }

     /**
      * Remove multiple entries of stores from storage.
      *
      * @param  Request  $request
      * @return \Illuminate\Http\Response
      */
     public function destroyMultiple(Request $request)
     {
         if($request->ajax()) {
             $success = [];
             $error = [];
             $exception = [];

            foreach($request['checkbox'] as $list){
                if(isset($request['trash_delete'])){
                    $post = Post::onlyTrashed()->find($list);
                }else{
                    $post = Post::find($list);
                }

                if(!$post){
                    $error[] = $list;
                    continue;
                }
                if(isset($request['trash_delete'])){
                    try
                    {
                        $post->forceDelete();
                    }
                    catch(Exception $ex)
                    {
                       $error[] = $list;
                       $exception[] = $ex->getMessage();
                       continue;
                    }
                    if(!isset($ex)){
                        $success[] = $post->id;
                    }
                }else{
                    if(!$post->destroy($list)){
                        $error[] = $list;
                        continue;
                    }else{
                        $success[] = $post->id;
                    }
                }
            }

            echo json_encode([
                'input' => $request->all(),
                'success_id' => $success,
                'exception' => $exception,
                'error_id' => $error
            ]);
        }
    }

    public function restore(Request $request)
     {
         if($request->ajax()) {
            $success = [];
            $error = [];
            foreach($request['checkbox'] as $list){
                $post = Post::withTrashed()->where('id', $list);
                if(!$post->restore()){
                $error[] = $list;
                        continue;
                }else{
                    $success[] = $list;
                }
            }
            echo json_encode([
                'error' => false, //or true
                'input' => $request->all(),
                'success_id' => $success,
                'error_ids' => $error
            ]);
        }
    }
}
