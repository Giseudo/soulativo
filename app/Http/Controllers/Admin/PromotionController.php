<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Promotion;
use App\Models\Category;
use Carbon\Carbon;

class PromotionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $frontend['view'] = 'promotion/list';
        $perPage = 10;
        $promotions = Promotion::all()->take($perPage);

        return view('admin.promotion.index')
            ->with('promotions', $promotions)
            ->with('perPage', $perPage)
            ->with('breadcrumb', 'promotion')
            ->with('frontend', $frontend);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $frontend['view'] = 'promotion/form';

        // Get the product categories in array form
        // We set it up on the view, to pass it to the QueryBuilder script
        $root = Category::findBySlug('packages');
        $categories = $root->getDescendantsAndSelf()->all();
        $filter = [];
        foreach($categories as $category){
            $dash = "";
            for ($i=0; $i < $category->getLevel(); $i++) {
                $dash .= "–";
            }
            $filter[$category->id] = $dash . ((!empty($dash)) ? ' ' : '') . $category->name;
        }

        return view('admin.promotion.create')
            ->with('frontend', $frontend)
            ->with('breadcrumb', 'promotion.create')
            ->with('categories', json_encode($filter));
    }


    public function store(Request $request)
    {
        $input = $request->all();

        $from = new Carbon($input['promotion']['from']);
        $to = new Carbon($input['promotion']['to']);
        $input['promotion']['from'] = $from->toDateString();
        $input['promotion']['to'] = $to->toDateString();

        $promotion = Promotion::create($input['promotion']);

        $request->session()->flash('status', trans('model.promotion.create'));
        return redirect('admin/promotion/edit/' . $promotion->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $frontend['view'] = 'promotion/form';
        $promotion = Promotion::find($id);
        $promotion->promotion = $promotion;

        // Get the product categories in array form
        // We set it up on the view, to pass it to the QueryBuilder script
        $root = Category::findBySlug('packages');
        $categories = $root->getDescendantsAndSelf();
        $filter = [];
        foreach($categories as $category){
            $dash = "";
            for ($i=0; $i < $category->getLevel(); $i++) {
                $dash .= "– ";
            }
            $filter[$category->id] = $dash . $category->name;
        }

        return view('admin.promotion.edit')
            ->with('promotion', $promotion)
            ->with('breadcrumb', 'promotion.edit')
            ->with('model', $promotion)
            ->with('frontend', $frontend)
            ->with('categories', json_encode($filter));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $promotion = Promotion::findOrFail($id);

        $this->validate($request, Promotion::getValidation());

        $from = new Carbon($input['promotion']['from']);
        $to = new Carbon($input['promotion']['to']);
        $input['promotion']['from'] = $from->toDateString();
        $input['promotion']['to'] = $to->toDateString();

        // Updates Promotion
        $promotion->fill($input['promotion'])->save();

        $request->session()->flash('status', trans('model.promotion.edit'));
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function destroyMultiple(Request $request)
    {
         //
    }

    public function restore(Request $request)
    {
         //
    }
}
