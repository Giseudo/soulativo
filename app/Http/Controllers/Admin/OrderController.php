<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Coupon;
use App\Models\User;
use Mail;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $frontend['view'] = 'order/list';
        $perPage = 10;
        $orders = Order::all()->take($perPage);

        return view('admin.order.index')
            ->with('orders', $orders)
            ->with('perPage', $perPage)
            ->with('breadcrumb', 'order')
            ->with('frontend', $frontend);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $frontend['view'] = 'order/show';
        $order = Order::find($id);


        return view('admin.order.show')
            ->with('order', $order)
            ->with('breadcrumb', 'order.show')
            ->with('model', $order)
            ->with('frontend', $frontend);
    }

    /**
     * Send the order to customer's email
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return bool
     */
    public function sendMail(Request $request, $id)
    {
        $error = false;
        $input = $request->all();
        $user = User::where('email', $input['email'])->first();
        $order = Order::find($id);
        
//        dd($input);

        $mail = Mail::send('emails.invoice', ['user' => $user, 'order' => $order, 'bookings' => $order->bookings, 'text' => $input['text']], function ($m) use ($user) {
            $m->from('contato@soulativo.com.br', 'SoulAtivo');
            $m->to($user->email, $user->name)->subject('Confirmação de Pagamento');
        });

        return [
            'error' => $error
        ];
    }

    /**
     * Create and apply a discount coupon to the order
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \App\Models\Coupon
     */
    public function applyCoupon(Request $request, $id)
    {
        $input = $request->all();
        $error = false;
        $coupon = new Coupon();

        if($input['coupon']['id'] == ''){
           $coupon->status = 0;
           $coupon->name = 'Cupom Direcionado | Order (' . $id   . ')';
           $coupon->value = $input['coupon']['value'];
           $coupon->key = Coupon::generateCouponCode(7);
           $coupon->type = 1;
           $coupon->save();

           $order = Order::find($id);
           $order->coupon_id = $coupon->id;
           $order->save();

        }

        return [
            'error' => $error,
            'order' => $order,
            'coupon' => $coupon
        ];
    }

    /**
     * Removes a discount coupon from the order
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return bool
     */
    public function removeCoupon(Request $request, $id)
    {
        $input = $request->all();
        $error = false;
        $order = Order::find($id);
        $order->coupon_id = NULL;
        $order->save();

        return [
            'error' => $error,
            'order' => $order
        ];
    }
}
