<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Order;
use App\Models\Booking;
use App\Models\Package;
use App\Models\Promotion;
use Carbon\Carbon;
use Cart;

class BookingController extends Controller
{
    public function store(Order $order)
    {
        $cart = Cart::contents();
        foreach($cart as $item){
            $package = Package::where('product_id', $item->package->id)->first();
            Promotion::products([$package], \App\Models\Product::$baseCurrency);

            $booking = new Booking();
            $booking->order_id = $order->id;
            $booking->package_id = $package->id;
            $booking->adults = $item->adults;
            $booking->childs = $item->childs;
            $booking->departure_location = $item->departure_location;
            $booking->departure_hour = $item->departure_hour;
            $booking->date = new Carbon($item->date);
            $booking->total = $package->product->getPrice('BRL');
            $booking->discount = $booking->total - $package->product->getDiscountedPrice(\App\Models\Product::$baseCurrency);
            $booking->status = 1;
            $booking->save();
        }

        // Creates a booking
        return redirect('/checkout/payment/' . $order->id);
    }
}
