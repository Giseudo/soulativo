<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Page;
use Session;

class PageController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        
        $pagesSide = Page::all();
        $page = Page::findBySlug($slug);
        Session::put('seo', $page->seo);
        return view('page.show')
                ->with('pages', $pagesSide)
                ->with('page', $page)
                ->with('slug', $slug);
    }

    public function notFound()
    {
        return view('page.404');
    }
}
