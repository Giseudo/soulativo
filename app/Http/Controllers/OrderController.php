<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Order;
use Auth;

class OrderController extends Controller
{
    public function store()
    {
        // Creates an order
        $order = new Order();
        $order->customer_id = Auth::user()->id;
        $order->save();

        // Creates a booking
        return app('App\Http\Controllers\BookingController')->store($order);
    }
}
