<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Package;
use App\Models\Product;
use App\Models\Media;
use App\Models\Category;
use App\Models\Promotion;
use App\Models\Post;
use App\Models\SEO;
use App\Models\Blockout;
use Session;


class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $frontend['view'] = 'package/list';
        $category = Category::findBySlug('packages');
        $packages = Package::all();

        Promotion::products($packages); // Creates a promotion into the passed products

        return view('package.index')
            ->with('category', $category)
            ->with('packages', $packages);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function category($slug)
    {
        $frontend['view'] = 'package/list';
        $packages = Package::getAllByCategorySlug($slug);
        
        $category = Category::findBySlug($slug);
        Session::put('seo', $category->seo);

        Promotion::products($packages); // Creates a promotion into the passed products

        return view('package.index')
            ->with('packages', array_filter($packages))
            ->with('category', $category);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $slug)
    {
        $package = Package::findBySlug($slug);
        $blockout = new Blockout();
        $blockouts = $blockout->getBlockouts($package->id);
        Session::put('seo', $package->seo);
        $currentUrl = $request->url();
        $frontend['view'] = 'package/show';

        $arrayIdsPackages = array($package->id);

        $gallery = Media::getByJson($package->gallery);
        $related = Package::orderBy('created_at', 'desc')->whereNotIn('id', $arrayIdsPackages)->take(2)->get();
        $recentPosts = Post::orderBy('created_at', 'desc')->take(6)->get();
        Promotion::products([$package]); // Creates a promotion into the passed products



        return view('package.show')
            ->with('package', $package)
            ->with('currentUrl', $currentUrl)
            ->with('blockouts', $blockouts)
            ->with('gallery', $gallery)
            ->with('recentPosts', $recentPosts)
            ->with('frontend', $frontend)
            ->with('related', $related);
    }
    
    public function search(Request $request)
    {
        $frontend['view'] = 'package/list';
        $input = $request->all();
        $category = Category::findBySlug('packages');
        $packages = Package::all();

        Promotion::products($packages); // Creates a promotion into the passed products

        return view('package.search')
            ->with('category', $category)
            ->with('search', $input)
            ->with('packages', $packages);
    }
}
