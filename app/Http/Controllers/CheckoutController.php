<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests;
use App\Models\Customer;
use App\Models\Package;
use App\Models\Promotion;
use App\Models\Invoice;
use App\Models\Order;

use Cart;
use Mail;
use Omnipay;
use PagSeguro;
use MercadoPago;
use Countries;
use Carbon\Carbon;

class CheckoutController extends Controller
{
    public function customer()
    {
        $frontend['view'] = 'checkout/customer';
        $user = Auth::user();
        $customer = Customer::find($user->id);

        if(!empty($customer)){
            if($customer->address->country->calling_code != 55){
                $customer->formatedBirthday = date('Y-m-d', strtotime(new Carbon($customer->birthday)));
            }else{
                $customer->formatedBirthday = date('d/m/Y', strtotime(new Carbon($customer->birthday)));
            }
            $customer->customer = $customer;
        }

        $countries[null] = trans('model.country.select');
        foreach(Countries::getList() as $key => $country){
            $countries[$key] = array('name' => $country['name'], 'calling_code' => $country['calling_code']);
        }

        return view('checkout.customer')
            ->with('customer', $customer)
            ->with('user', $user)
            ->with('countries', $countries)
            ->with('frontend', $frontend);
    }

    /**
     * Display a listing of the user.
     *
     * @return \Illuminate\Http\Response
     */
    public function payment($id)
    {
        if(!Auth::user()->hasRole('customer')){
            return redirect('/checkout/customer');
        }

        $frontend['view'] = 'checkout/payment';
        $order = Order::find($id);

        return view('checkout.payment')
            ->with('frontend', $frontend)
            ->with('order', $order);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $input = $request->all();
        $user = Auth::user();
        $cart = Cart::contents();
        $order = Order::find($id);
        $input['customer'] = $user->customer;
        $input['address'] = $user->customer->address;
        $input['user'] = $user;
        $input['cart'] = $cart;
        $input['order'] = $order;
        $input['bookings'] = $order->bookings;


        $invoice = new Invoice();
        $invoice->method = $input['payment']['method'];
        $invoice->order_id = $order->id;
        $invoice->save();

        // Checks the payment method
        switch ($input['payment']['method']) {
            case 'creditcard':
                // $invoice->stripe($request);
                $invoice->creditcard($request);
                break;

            case 'paypal':
                $invoice->payPal($request);
                break;

            case 'pagseguro':
                return $invoice->pagSeguro($request, $input);
                break;

            case 'mercadopago':
                return $invoice->mercadoPago($input);
                break;
        }

        // Check for errors
        if($request->session()->get('status')){
            return redirect('/checkout/payment/' . $order->id);
        }else{
            return $this->success($invoice);
        }
    }

    public function success(Invoice $invoice)
    {
        $user = Auth::user();
        $order = $invoice->order;
        $bookings = $order->bookings;

        // If everything goes ok, then destroys the cart
        Cart::destroy();

        // Send confirmation email to customer
        Mail::send('emails.invoice', ['user' => $user, 'order' => $order, 'bookings' => $bookings], function ($m) use ($user) {
            $m->from('contato@soulativo.com.br', 'SoulAtivo');
            $m->to($user->email, $user->name)->subject('Confirmação de Pagamento');
        });

        return redirect('/checkout/confirmation/' . $invoice->id);
    }

    /**
     * Display checkout confirmation.
     *
     * @return \Illuminate\Http\Response
     */
    public function confirmation($id)
    {
        $invoice = Invoice::find($id);
        $packages = Package::all()->take(3);

        return view('checkout.confirmation')
            ->with('invoice', $invoice)
            ->with('packages', $packages);
    }
}
