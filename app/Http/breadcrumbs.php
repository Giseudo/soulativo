<?php
    
    // Dashboard
    Breadcrumbs::register('dashboard', function($breadcrumbs) {
        $breadcrumbs->push('Dashboard', route('dashboard'));
    });

    // User
    Breadcrumbs::register('user', function($breadcrumbs) {
        $breadcrumbs->push(trans('model.user.plural'), route('user'));
    });
    
    Breadcrumbs::register('user.create', function($breadcrumbs) {
        $breadcrumbs->parent('user');
        $breadcrumbs->push(trans('model.user.create'), route('user.create'));
    });
    
    Breadcrumbs::register('user.edit', function($breadcrumbs, $model) {
        $breadcrumbs->parent('user');
        $breadcrumbs->push($model->name, route('user.edit', $model->id));
    });
    
    
    // Page
    Breadcrumbs::register('page', function($breadcrumbs) {
        $breadcrumbs->push(trans('model.page.plural'), route('page'));
    });
    
    Breadcrumbs::register('page.create', function($breadcrumbs) {
        $breadcrumbs->parent('page');
        $breadcrumbs->push(trans('model.page.create'), route('page.create'));
    });
    
    Breadcrumbs::register('page.edit', function($breadcrumbs, $model) {
        $breadcrumbs->parent('page');
        $breadcrumbs->push($model->title, route('page.edit', $model->id));
    });
    
     // SEO
    Breadcrumbs::register('seo', function($breadcrumbs) {
        $breadcrumbs->push('SEO', route('seo'));
    });
    
    Breadcrumbs::register('seo.edit', function($breadcrumbs, $model, $slug) {
        $breadcrumbs->parent('seo');
        $breadcrumbs->push($model->title, route('seo.edit', array('slug' => $slug, 'id' => $model->id)));
    });
    
    // Costumers
    Breadcrumbs::register('customer', function($breadcrumbs) {
        $breadcrumbs->push(trans('model.customer.plural'), route('customer'));
    });
    
    Breadcrumbs::register('customer.create', function($breadcrumbs) {
        $breadcrumbs->parent('customer');
        $breadcrumbs->push(trans('model.customer.create'), route('customer.create'));
    });
    
    Breadcrumbs::register('customer.edit', function($breadcrumbs, $model) {
        $breadcrumbs->parent('customer');
        $breadcrumbs->push($model->user->name, route('customer.edit', $model->id));
    });

    
    // Booking
    Breadcrumbs::register('booking', function($breadcrumbs) {
        $breadcrumbs->push(trans('model.booking.plural'), route('booking'));
    });
    Breadcrumbs::register('booking.create', function($breadcrumbs) {
        $breadcrumbs->parent('booking');
        $breadcrumbs->push(trans('model.booking.create'), route('booking.create'));
    });
    
    Breadcrumbs::register('booking.edit', function($breadcrumbs, $model) {
        $breadcrumbs->parent('booking');
        $breadcrumbs->push('#'.$model->id, route('booking.edit', $model->id));
    });
    
    // Order
    Breadcrumbs::register('order', function($breadcrumbs) {
        $breadcrumbs->push(trans('model.order.plural'), route('order'));
    });
    Breadcrumbs::register('order.show', function($breadcrumbs, $model) {
        $breadcrumbs->parent('order');
        $breadcrumbs->push('#'. $model->id, route('order.show', $model->id));
    });
    
    // Package
    Breadcrumbs::register('package', function($breadcrumbs) {
        $breadcrumbs->push(trans('model.package.plural'), route('package'));
    });
    Breadcrumbs::register('package.create', function($breadcrumbs) {
        $breadcrumbs->parent('package');
        $breadcrumbs->push(trans('model.package.create'), route('package.create'));
    });
    
    Breadcrumbs::register('package.edit', function($breadcrumbs, $model) {
        $breadcrumbs->parent('package');
        $breadcrumbs->push($model->product->name, route('package.edit', $model->id));
    });
    
    // Post
    Breadcrumbs::register('post', function($breadcrumbs) {
        $breadcrumbs->push(trans('model.post.plural'), route('post'));
    });
    Breadcrumbs::register('post.create', function($breadcrumbs) {
        $breadcrumbs->parent('post');
        $breadcrumbs->push(trans('model.post.create'), route('post.create'));
    });
    
    Breadcrumbs::register('post.edit', function($breadcrumbs, $model) {
        $breadcrumbs->parent('post');
        $breadcrumbs->push($model->title, route('post.edit', $model->id));
    });
    
    // Category
    Breadcrumbs::register('category', function($breadcrumbs, $model, $slug) {
        switch($slug){
            case 'pacotes':
                $breadcrumbs->parent('package');
                break;
            case 'packages':
                $breadcrumbs->parent('package');
                break;
            case 'posts':
                $breadcrumbs->parent('post');
                break;
        }
        $breadcrumbs->push(trans('model.category.plural'), route('category', array('slug'=> $slug)));
    });
    Breadcrumbs::register('category.create', function($breadcrumbs, $model, $slug) {
        $breadcrumbs->parent('category', $model, $slug);
        $breadcrumbs->push(trans('model.category.create'), route('category.create', array('slug' => $slug)));
    });
    
    Breadcrumbs::register('category.edit', function($breadcrumbs, $model, $slug) {
        $breadcrumbs->parent('category', $model, $slug);
        $breadcrumbs->push($model->name, route('category.edit', array('slug' => $slug, 'id' => $model->id)));
    });
    
    // Promotion
    Breadcrumbs::register('promotion', function($breadcrumbs) {
        $breadcrumbs->push(trans('model.promotion.plural'), route('promotion'));
    });
    Breadcrumbs::register('promotion.create', function($breadcrumbs) {
        $breadcrumbs->parent('promotion');
        $breadcrumbs->push(trans('model.promotion.create'), route('promotion.create'));
    });
    
    Breadcrumbs::register('promotion.edit', function($breadcrumbs, $model) {
        $breadcrumbs->parent('promotion');
        $breadcrumbs->push($model->name, route('promotion.edit', $model->id));
    });
    
    // Blockout
    Breadcrumbs::register('blockout', function($breadcrumbs) {
        $breadcrumbs->push(trans('model.blockout.plural'), route('blockout'));
    });
    Breadcrumbs::register('blockout.show', function($breadcrumbs, $model) {
        $breadcrumbs->parent('blockout');
        $breadcrumbs->push($model->product->name, route('blockout.show', $model->id));
    });
    
    // Coupon
    Breadcrumbs::register('coupon', function($breadcrumbs) {
        $breadcrumbs->push(trans('model.coupon.plural'), route('coupon'));
    });
    Breadcrumbs::register('coupon.create', function($breadcrumbs) {
        $breadcrumbs->parent('coupon');
        $breadcrumbs->push(trans('model.coupon.create'), route('coupon.create'));
    });
    
    Breadcrumbs::register('coupon.edit', function($breadcrumbs, $model) {
        $breadcrumbs->parent('coupon');
        $breadcrumbs->push($model->name, route('coupon.edit', $model->id));
    });
    
    // Partner
    Breadcrumbs::register('partner', function($breadcrumbs) {
        $breadcrumbs->push(trans('model.partner.plural'), route('partner'));
    });
    
    Breadcrumbs::register('partner.create', function($breadcrumbs) {
        $breadcrumbs->parent('partner');
        $breadcrumbs->push(trans('model.partner.create'), route('partner.create'));
    });
    
    Breadcrumbs::register('partner.edit', function($breadcrumbs, $model) {
        $breadcrumbs->parent('partner');
        $breadcrumbs->push($model->user->name, route('partner.edit', $model->id));
    });
    
     // Store
    Breadcrumbs::register('store', function($breadcrumbs) {
        $breadcrumbs->push(trans('model.store.plural'), route('store'));
    });
    Breadcrumbs::register('store.create', function($breadcrumbs) {
        $breadcrumbs->parent('store');
        $breadcrumbs->push(trans('model.store.create'), route('store.create'));
    });
    
    Breadcrumbs::register('store.edit', function($breadcrumbs, $model) {
        $breadcrumbs->parent('store');
        $breadcrumbs->push($model->name, route('store.edit', $model->id));
    });
    
    
    // Home > About
//    Breadcrumbs::register('about', function($breadcrumbs) {
//        $breadcrumbs->parent('home');
//        $breadcrumbs->push('About', route('about'));
//    });
//
//    // Home > Blog
//    Breadcrumbs::register('blog', function($breadcrumbs) {
//        $breadcrumbs->parent('home');
//        $breadcrumbs->push('Blog', route('blog'));
//    });
//
//    // Home > Blog > [Category]
//    Breadcrumbs::register('category', function($breadcrumbs, $category) {
//        $breadcrumbs->parent('blog');
//        $breadcrumbs->push($category->title, route('category', $category->id));
//    });
//
//    // Home > Blog > [Category] > [Page]
//    Breadcrumbs::register('page', function($breadcrumbs, $page) {
//        $breadcrumbs->parent('category', $page->category);
//        $breadcrumbs->push($page->title, route('page', $page->id));
//    });
    ?>
