<?php

namespace App\Policies;

use Auth;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Models\User;

class PostPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
	
    /**
     * Determine if the given post can be updated by the user.
     *
     * @param  \App\Models\Post  $post
     * @return bool
     */
    public function update(User $user)
    {
		return $user->hasRole('admin');
    }
}
