<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use SoftDeletes;
    use Validatable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['last_login','created_at', 'updated_at','deleted_at'];

    /**
     * The fieldset validation settings.
     *
     * @var array
     */
    public static $validation = [
        'user.first_name' => 'required|min:4',
        'user.last_name' => 'required|min:4',
        'user.email' => 'required|email',
        'user.password' => 'confirmed|min:6'
    ];
    
    public function customer(){
        return $this->hasOne(\App\Models\Customer::class, 'id', 'id');
    }

    /**
     * Get the users that owns the role.
     */
    public function roles()
	{
        return $this->belongsToMany(Role::class);
    }

    public function findByUsernameOrCreate($userData)
    {
        return User::firstOrNew([
            'email' => $userData->user['email']
        ]);
    }
    
    public static function findByName($name, $role_id = null)
    {
        $results = User::join('role_user', 'users.id', '=', 'role_user.user_id')->where(function($q) use ($name, $role_id) {
            $slice = explode(' ', $name);
            if(!is_null($role_id)){
                $q->where('role_user.role_id', $role_id);
            }
            $q->where('users.first_name', 'LIKE', '%'.$slice[0].'%');
            if(count($slice) > 1){
            for($i = 1; $i < count($slice); $i++):
                $q->where('users.last_name', 'LIKE', '%'.$slice[$i].'%');
            endfor;
        }
        })->get();
        
        return $results;
    }

    /**
     * Verify if the user has a role.
     *
     * @param  string  $slug
     * @return bool
     */
	public function hasRole($slug) {
		foreach ($this->roles as $role) {
			if ($role->slug === $slug) {
				return true;
			}
		}
		return false;
	}

    /**
     * Adds a role to an user.
     *
     * @param  string  $slug
     */
	public function addRole($slug) {
		$role = \App\Models\Role::getBySlug($slug);

		if (empty($role)) {
			abort(403, "Role doesn't exist");
		}

		$this->roles()->sync([ $role->id ], false);
	}

	/**
	 * Sets a role to an user.
	 *
	 * @param  string  $slug
	 */
	public function setRole($slug) {
		$role = \App\Models\Role::getBySlug($slug);

		if (empty($role)) {
			abort(403, "Role doesn't exist");
		}

		$this->roles()->sync([ $role->id ]);
	}

    /**
     * Removes a role to an user.
     *
     * @param  string  $slug
     */
	public function removeRole($slug) {
		$role = \App\Models\Role::getBySlug($slug);

		if (empty($role)) {
			abort(403, "Role doesn't exist");
		}

		$this->roles()->detach([ $role->id ]);
	}

	/**
	 * Get the user's full name.
	 *
	 * @return string
	 */
	public function getNameAttribute()
	{
		return $this->attributes['first_name']. " " . $this->attributes['last_name'];
	}

        public static function findByRole($role_id, $trashed = false){

            if($trashed === true){
                $data = User::whereHas( 'roles', function($q) use($role_id){  $q->where('id', $role_id); })->onlyTrashed()->get();
            }else{
                $data = User::whereHas( 'roles', function($q) use($role_id){  $q->where('id', $role_id); })->get();
            }
            if(!empty($data)){
                return $data;
            }else{
                return false;
            }

        }
        
        public static function findByRolePaginator($role_id, $limit, $page){
            return $users = User::whereHas( 'roles', function($q) use($role_id){  $q->where('id', $role_id); })->take($limit)->skip(($page - 1) * $limit)->get();
        }
}
