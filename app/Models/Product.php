<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use App\Models\ProductTranslation;
use DB;

use Cart;
use Swap;

class Product extends Model
{
    use Translatable;
    use Validatable;

    protected $discount = 0;
    protected $fillable = ['name', 'slug', 'content', 'excerpt', 'price','child_percent'];
    public $translatedAttributes = ['name', 'slug', 'content', 'excerpt'];
//    public static $childPercent = 40;
    public static $baseCurrency = 'BRL';
    public static $validation = [
        'product.name' => 'required|min:4',
        'product.slug' => 'required|min:4',
        'product.price' => 'required|min:1',
        'product.content' => 'required|min:4'
    ];

    public function package()
    {
        return $this->hasOne(\App\Models\Package::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function addDiscount($value)
    {
        $this->discount += $value;
    }

    public static function getRate($currency = null)
    {
        if(!empty($currency)){
            $currentCurrency = $currency;
        }else{
            $currentCurrency = config('app.currency.name');
        }

        if($currentCurrency == static::$baseCurrency){
            return 1;
        }else{
            return Swap::quote($currentCurrency. '/' . static::$baseCurrency)->getValue();
        }
    }

    public function getPrice($currency = null)
    {
        return round($this->price / static::getRate($currency));
    }

    public function getDiscountedPrice($currency = null)
    {
        return round($this->getPrice($currency) - $this->getDiscount());
    }

    public function getDiscount($currency = null)
    {
        return $this->discount;
    }

    public function getDiscountedPercent()
    {
        if($this->price != 0)
            return number_format(($this->getDiscount() / $this->getPrice()) * 100, 0);
        else
            return 0;
    }

    public function getChildPrice($currency = null)
    {
//        $discount = (static::$childPercent / 100) * $this->getDiscountedPrice($currency);
        $discount = ($this->child_percent / 100) * $this->getDiscountedPrice($currency);
        return round($this->getDiscountedPrice($currency) - $discount);
    }

    public function getSubtotal($adults, $childs, $currency = null)
    {
        $adultSubtotal = $adults * $this->getDiscountedPrice($currency);
        $childSubtotal = $childs * $this->getChildPrice($currency);
        return $adultSubtotal + $childSubtotal;
    }

    public static function getAllByCategorySlug($slug)
    {
        $category = Category::findBySlug($slug);
        $query = DB::table('products')
            ->join('category_product', 'products.id', '=', 'category_product.product_id')
            ->join('packages', 'products.id', '=', 'packages.product_id')
            ->select('products.*')
            ->where('category_product.category_id',$category->id)
            ->orderBy('packages.order', 'ASC')
            ->get();
        
        $products = array();
        foreach($query as $i){
            $products[] = Product::find($i->id);
        }
        return $products;
    }

}
