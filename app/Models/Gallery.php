<?php

namespace App\Models;

Use App\Models\Media;

trait Gallery {

    /**
     * Get the user's full name.
     *
     * @return string
     */
    public function getGalleryAttribute() {
        if(!empty($this->attributes['gallery'])){
            $arrayJsonGallery = json_decode($this->attributes['gallery']);
            foreach ($arrayJsonGallery->images as $index => $id) {
                $media = Media::find($id);
                if (is_null($media)) {
                    unset($arrayJsonGallery->images[$index]);
                }
            }
            $newArrGallery = "[";
            $newArrGallery .= implode(array_values($arrayJsonGallery->images), ',');
            $newArrGallery .= "]";
            $newArrGallery = '{"images":' . $newArrGallery . '}';

            $pk = Package::find($this->attributes['id']);
            $pk->gallery = $newArrGallery;
            $pk->save();

            $this->attributes['gallery'] = $newArrGallery;
        }

        return $this->attributes['gallery'];
    }

}
