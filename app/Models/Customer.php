<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Cashier\Billable;
use Carbon\Carbon;

class Customer extends Model
{
    use Billable;
    use Validatable;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'birthday', 'updated_at'];

    protected $fillable = [
        'document', 'card_last_four', 'card_brand', 'birthday', 'telephone'
    ];

    public static $validation = [
        'customer.document' => 'required|min:5',
//        'customer.birthday' => 'required|date',
        'customer.telephone' => 'required'
    ];

    /**
     * Get the customer's user.
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id');
    }

    /**
     * Get the customer's address.
     */
    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    /**
     * Get the orders of the customer.
     */
    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function getBirthdayAttribute(){
        $newDateCarbon = new Carbon($this->attributes['birthday']);

        return $newDateCarbon->toFormattedDateString();
    }
}
