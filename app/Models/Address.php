<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Country;

class Address extends Model
{
    use Validatable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'zipcode', 'state', 'city', 'street', 'number', 'complement', 'neighbourhood', 'country_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * The fieldset validation settings.
     *
     * @var array
     */
    public static $validation = [
        'address.country_id' => 'required',
        'address.state' => 'required',
        'address.city' => 'required',
        'address.zipcode' => 'required',
        'address.street' => 'required',
        'address.number' => 'required',
        'address.neighbourhood' => 'required'
    ];

    /**
     * The address' country.
     */
    public function country() {
        return $this->belongsTo(Country::class);
    }

}
