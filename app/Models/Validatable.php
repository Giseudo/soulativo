<?php

namespace App\Models;

trait Validatable
{
    /**
    * Get the user's full name.
    *
    * @return string
    */
    public static function getValidation()
    {
        return static::$validation;
    }
}
