<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Package;

class Blockout extends Model
{
    public $timestamps = false;
    
    
    public function package()
    {
        return $this->hasOne(Package::class, 'id');
    }

    
    public function getBlockouts($id){
        
        $package = Package::find($id);
        
        if($package->blockouts){
            $blockouts = '[';
            foreach($package->blockouts as $i => $b){
                $date = explode('-', $b->date);
                if($i != 0){
                    $blockouts .= ',"'.$b->date.'"';
                }else{
                    $blockouts .= '"'.$b->date.'"';
                }
            }
            $blockouts .= ']';
            return $blockouts;
        }else{
            return null;
        }
        
        
    }
}
