<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Dimsav\Translatable\Translatable;
use App\Models\PackageExtraTranslation;

class PackageExtra extends Model
{
    use Translatable;
    use SoftDeletes;

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $fillable = ['name', 'value'];
    public $translatedAttributes = ['name'];
    public static $baseCurrency = 'BRL';
  

    public function package()
    {
        return $this->hasOne(\App\Models\Package::class);
    }

    public static function getRate($currency = null)
    {
        if(!empty($currency)){
            $currentCurrency = $currency;
        }else{
            $currentCurrency = config('app.currency.name');
        }

        if($currentCurrency == static::$baseCurrency){
            return 1;
        }else{
            return Swap::quote($currentCurrency. '/' . static::$baseCurrency)->getValue();
        }
    }

    public function getValue($currency = null)
    {
        return round($this->value / static::getRate($currency));
    }
}
