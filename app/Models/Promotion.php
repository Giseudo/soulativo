<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Models\Category;
use Hoa\Ruler\Ruler;
use Hoa\Ruler\Context;
use Hoa\Visitor;

class Promotion extends Model
{
    use Validatable;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['from', 'to', 'created_at', 'updated_at'];
    protected $fillable = ['name', 'rules', 'value', 'from', 'to'];
    public static $validation = [];

    public static function getActive()
    {
        return Promotion::where('from', '<', Carbon::now())
            ->where('to', '>', Carbon::now())
            ->whereNotNull('rules')
            ->where('rules', '!=', '')
            ->where('rules', '!=', '{}')
            ->get();
    }

    public static function products($items, $currency = null)
    {
        $ruler = new Ruler();

        foreach(Promotion::getActive() as $promotion){
            foreach($items as $item){
                $context = new Context();
                $context['product'] = [
                    'id' => $item->product->id,
                    'name' => $item->product->name,
                    'slug' => $item->product->slug,
                    'categories' => $item->product->categories->lists('id')->toArray()
                ];


                // If we catch an error, simply ignore the current rule
                try{
                    // Build rule
                    $compiler = \Hoa\Compiler\Llk\Llk::load(new \Hoa\File\Read(base_path('vendor/hoa/json/Grammar.pp')));
                    $ast = $compiler->parse($promotion->rules);
                    $rules = new RuleStringify();
                    
                    if($ruler->assert($rules->visit($ast), $context)){
                        $discount = ($promotion->value / 100) * $item->product->getDiscountedPrice($currency);
                        $item->product->addDiscount($discount);
                    }
                } catch (\Exception $e) {
                    // dd($e->getMessage());
                }
            }
        }
    }

    public function getFromAttribute(){
        $from = new Carbon($this->attributes['from']);
        return $from->toFormattedDateString();
    }

    public function getToAttribute(){
        $to = new Carbon($this->attributes['to']);
        return $to->toFormattedDateString();
    }
}

class RuleStringify implements Visitor\Visit
{
    private $operator = null;
    private $condition = 'true';
    private $rule = '';
    public function visit (\Hoa\Visitor\Element $element, &$handle = null, $eldnah = null) {
        switch ($element->getId()) {
            // { … }
            case '#object':
                foreach ($element->getChildren() as $e => $child) {

                }
            // [ … ]
            case '#array':
                foreach ($element->getChildren() as $e => $child) {
                    if(0 < $e){
                        // echo ","; // TODO: Concatenate array items
                    }
                    $child->accept($this, $handle, $eldnah);
                }
                break;
            // … : …
            case '#pair':
                // If the pair is a condition
                if($element->getChild(0)->getValueValue() == '"condition"'){
                    switch ($element->getChild(1)->getValueValue()) {
                        case '"AND"':
                            $this->operator = ' and ';
                            break;
                        case '"OR"':
                            $this->operator = ' or ';
                            break;
                        default:
                            break;
                    }
                }

                // If the pair has rules
                if($element->getChild(0)->getValueValue() == '"rules"'){
                    $i = 0;

                    foreach($element->getChild(1)->getChildren() as $rule){
                        $this->rule .= "(";

                        switch ($rule->getChild(4)->getChild(1)->getValueValue()) {
                            case '"in"':
                            case '"not_in"':
                                $rule->getChild(5)->getChild(1)->accept($this, $handle, $eldnah); // Value
                                $rule->getChild(4)->getChild(1)->accept($this, $handle, $eldnah); // Operator
                                $rule->getChild(0)->getChild(1)->accept($this, $handle, $eldnah); // Id
                                break;

                            default:
                                $rule->getChild(0)->getChild(1)->accept($this, $handle, $eldnah); // Id
                                $rule->getChild(4)->getChild(1)->accept($this, $handle, $eldnah); // Operator
                                $rule->getChild(5)->getChild(1)->accept($this, $handle, $eldnah); // Value
                                break;
                        }

                        // Check is the condition is either false or true
                        if($rule->getChild(4)->getChild(1)->getValueValue() == '"not_in"' ||
                            $rule->getChild(4)->getChild(1)->getValueValue() == '"is_not_null"'){
                            $this->condition = 'false';
                        }else{
                            $this->condition = 'true';
                        }

                        $this->rule .= ") = " . $this->condition;

                        $i++;
                        if($i < count($element->getChild(1)->getChildren())){
                            $this->rule .= $this->operator; // Prints the condition
                        }
                    }
                }
            // Many tokens.
            case 'token':
                switch ($element->getValueToken()) {
                    // String: "…".
                    case 'string':
                        switch ($element->getValueValue()) {
                            case '"categories"':
                                $this->rule .= 'product[' . $element->getValueValue() . ']';
                                break;
                            case '"equal"':
                                $this->rule .= ' = ';
                                break;
                            case '"in"':
                            case '"not_in"':
                                $this->rule .= ' in ';
                                break;
                            case '"is_null"':
                            case '"is_not_null"':
                                $this->rule .= ' = null';
                                break;
                            default:
                                $this->rule .= str_replace('"', '', $element->getValueValue());
                                break;
                        }
                        break;

                    // Booleans.
                    case 'true':
                    case 'false':

                    // Null.
                    case 'null':

                    // Number.
                    case 'number':
                        break;
                }
        }
        return $this->rule;
    }
}
