<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Models\Customer;
use Auth;
use Omnipay;
use PagSeguro;
use Session;
use LivePixel\MercadoPago\MP;
use GuzzleHttp;
use GuzzleHttp\Exception\ClientException;

class Invoice extends Model {

    public function order() {
        return $this->belongsTo(Order::class);
    }

    public function getCallback() {
        return json_decode($this->callback);
    }

    public function getPrice($row = 0) {
        $callback = $this->getCallback();
        $price = '';

        switch ($this->method) {
            case 'creditcard':
            case 'pagseguro':
                if (!empty($callback->grossAmount)) {
                    $price = strtoupper("BRL " . number_format(($callback->grossAmount), 2));
                }
                break;

            case 'paypal':
                if (!empty($callback->PAYMENTINFO_0_CURRENCYCODE) && !empty($callback->PAYMENTINFO_0_AMT)) {
                    $price = $callback->PAYMENTINFO_0_CURRENCYCODE . " " . number_format($callback->PAYMENTINFO_0_AMT, 2);
                }
                break;
            case 'mercadopago':
                if(!empty($callback->payments[$row])){
                    $price = $callback->payments[$row]->total_paid_amount;
                }

                break;
        }

        return (!empty($price)) ? $price : '–––';
    }

    public function getStatus($row = 0) {
        $callback = $this->getCallback();
        $status = 1;

        switch ($this->method) {
            case 'creditcard':
            case 'pagseguro':
                if (!empty($callback->status)) {
                    if ($callback->status == 3)
                        $status = 2;
                }
                break;

            case 'paypal':
                if (!empty($callback->PAYMENTINFO_0_PAYMENTSTATUS)) {
                    if ($callback->PAYMENTINFO_0_PAYMENTSTATUS == 'Completed')
                        $status = 2;
                }
                break;
            case 'mercadopago':
                if(!empty($callback->payments[$row])) {
                    if ($callback->payments[$row]->status == 'approved')
                        $status = 2;
                }
                break;
        }

        if ($status == 3) {
            return ['class' => 'label-info', 'text' => 'Rejected'];
        } else if ($status == 2) {
            return ['class' => 'label-success', 'text' => 'Succeeded'];
        } else if ($status == 1) {
            return ['class' => 'label-warning', 'text' => 'Pending'];
        }
    }

    // The current method to direct creditcard payment
    public function creditcard(Request $request) {
        $input = $request->all();
        $credentials = PagSeguro::credentials()->get();
        $user = Auth::user();

        // Telephone number
        $telephone = preg_replace('/\([^)]*\)/', '', $user->customer->telephone); // Remove region code
        $telephone = str_replace('+55', '', $telephone); // Remove country code
        $telephone = str_replace('-', '', $telephone); // Remove dashs
        $telephone = str_replace(' ', '', $telephone); // Remove empty spaces

        // Area code
        $area;
        preg_match('/\([^)]*\)/', $user->customer->telephone, $area); // Get area code
        $area = str_replace('(', '', $area); // Remove parenthesis
        $area = str_replace(')', '', $area); // Remove parenhesis
        $area = $area[0];

        // CPF
        $cpf = str_replace('.', '', $user->customer->document);
        $cpf = str_replace('-', '', $cpf);

        // Birthday
        $birthday = new \Carbon\Carbon($user->customer->attributes['birthday']);
        $birthday = $birthday->format('d/m/Y');

        // Item description
        $description = '';

        // Concatenate the description name
        foreach ($this->order->bookings as $booking) {
            $description .= $booking->package->product->name . ', ';
        }

        // Fix the last comma
        $description = substr($description, 0, -2);

        $data = [
          'email' => $credentials->getEmail(),
          'token' => $credentials->getToken(),
          'paymentMode' => 'default',
          'paymentMethod' => 'creditCard',
          'receiverEmail' => $credentials->getEmail(),
          'currency' => 'BRL',
          'extraAmount' => "0.00",
          'itemId1' => $this->order->id,
          'itemAmount1' => number_format($this->order->getTotal(), 2, '.', ''),
          'itemQuantity1' => 1,
          'itemDescription1' => $description,
          'reference' => $this->id,
          'senderName' => $input['card-holder'],
          'senderEmail' => $user->email,
          'senderCPF' => $cpf,
          'senderPhone' => $telephone,
          'senderHash' => $input['senderHash'],
          'senderAreaCode' => $area,
          'shippingAddressStreet' => $user->customer->address->street,
          'shippingAddressNumber' => $user->customer->address->number,
          'shippingAddressComplement' => $user->customer->address->complement,
          'shippingAddressDistrict' => $user->customer->address->neighbourhood,
          'shippingAddressPostalCode' => $user->customer->address->zipcode,
          'shippingAddressCity' => $user->customer->address->city,
          'shippingAddressState' => $user->customer->address->state,
          'shippingAddressCountry' => $user->customer->address->country->name,
          'shippingType' => 1, // FIXME
          'shippingCost' => "0.00",
          'creditCardToken' => $input['cardToken'],
          'creditCardHolderName' => $input['card-holder'],
          'creditCardHolderCPF' => $cpf,
          'creditCardHolderBirthDate' => $birthday,
          'creditCardHolderPhone' => $telephone,
          'creditCardHolderAreaCode' => $area,
          'billingAddressStreet' => $user->customer->address->street,
          'billingAddressNumber' => $user->customer->address->number,
          'billingAddressComplement' => $user->customer->address->complement,
          'billingAddressDistrict' => $user->customer->address->neighbourhood,
          'billingAddressPostalCode' => $user->customer->address->zipcode,
          'billingAddressCity' => $user->customer->address->city,
          'billingAddressState' => $user->customer->address->state,
          'billingAddressCountry' => $user->customer->address->country->name,
          'installmentQuantity' => 1,
          'installmentValue' => number_format($this->order->getTotal(), 2, '.', '')
        ];

        try {
            $client = new GuzzleHttp\Client();
            $res = $client->post('https://ws.' . ((env('APP_ENV') != 'production') ? 'sandbox.' : '') .'pagseguro.uol.com.br/v2/transactions/', [
              'headers' => [
                'Content-Type', 'application/x-www-form-urlencoded; charset=ISO-8859-1'
              ],
              'body' => $data
            ]);

            $body = $res->getBody();
            $xml = simplexml_load_string($body->getContents());

            $this->callback = json_encode($xml);
            $this->save();
        } catch (ClientException $e) {
            $body = $e->getResponse()->getBody();
            $data = simplexml_load_string($body->getContents());

            // Set error messages
            $request->session()->flash('status', (string)$data->error[0]->message);

            // Delete the current invoice
            $this->delete();
        }
    }

    // Previous attempt to direct creditcard payment
    // It is working, though PayPal does not accept BRL on direct transactions yet
    public function payPalCreditcard(Request $request) {
        $input = $request->all();

        // Select the creditcard gateway
        Omnipay::setGateway('creditcard');

        // Get the logged user
        $user = Auth::user();

        // Proccess payment
        try {
            $card = [
                'firstName' => $user->first_name,
                'lastName' => $user->last_name,
                'number' => $input['card-number'],
                'expiryMonth' => $input['card-expiry-month'],
                'expiryYear'  => $input['card-expiry-year'],
                'cvv'         => $input['card-cvc'],
                'billingAddress1' => $user->customer->address->street . ', ' . $user->customer->address->number,
                'billingAddress2' => $user->customer->address->neighbourhood,
                'billingCountry' => $user->customer->address->country->iso_3166_2,
                'billingCity' => $user->customer->address->city,
                'billingState' => $user->customer->address->state,
                'billingPostcode' => $user->customer->address->zipcode
            ];

            $response = Omnipay::purchase([
                'amount' => $this->order->getTotal('BRL'),
                'currency' => 'BRL',
                'returnUrl' => 'https://soulativo.com.br/paypal/success/' . $this->id,
                'cancelUrl' => 'https://soulativo.com.br/paypal/refused/' . $this->id,
                'card' => $card
            ])->send();

            $data = $response->getData();

            if ($response->isSuccessful()) {
                // If everything went ok, save the invoice
                $this->callback = json_encode($data);
                $this->save();
            } else {
                // If there are any errors, redirect back with an error
                $request->session()->flash('status', $data['details'][0]['issue']);
                $this->delete();
            }
        } catch (\Exception $e) {
            $request->session()->flash('status', $e->getMessage());
            $this->delete();
        }
    }

    // Previous attempt to use direct credit card
    // It is working, though Stripe does not accept BRL payments yet
    public function stripeCreditcard(Request $request) {
        $input = $request->all();
        // Sets API Key

        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

        $user = Auth::user();
        $token = $input['stripeToken'];

        // Try to charge
        try {
            // If customer do not have a stripe_id yet, then create one
            if (empty($user->customer->stripe_id)) {
                // Create a Customer
                $customer = \Stripe\Customer::create(array(
                            'source' => $token,
                            'description' => $user->name,
                            'email' => $user->email
                ));
                $user->customer->stripe_id = $customer->id;
            } else {
                $customer = \Stripe\Customer::retrieve($user->customer->stripe_id);

                // Updates info with new card
                $customer->source = $token;
                $customer->save();
            }

            // Charge the Customer instead of the card
            $charge = \Stripe\Charge::create(array(
                        "amount" => $this->order->getTotal() * 100, // amount in cents
                        "currency" => config('app.currency.name'),
                        "customer" => $customer->id,
                        "description" => "Product names"
            ));

            // Updates the invoice callback
            $this->callback = json_encode($charge);
            $this->save();

            // Saves customer data
            $user->customer->card_brand = $charge->source->brand;
            $user->customer->card_last_four = $charge->source->last4;
            $user->customer->save();
        } catch (\Exception $e) {
            // The card has been declined
            $request->session()->flash('status', $e->getMessage());
            $this->delete();
        }
    }

    // Do a Transactions through PayPal provider
    public function payPal(Request $request) {
        $params = [
            'amount' => $this->order->getTotal(),
            'currency' => config('app.currency.name'),
            'returnUrl' => 'https://soulativo.com.br/paypal/success/' . $this->id,
            'cancelUrl' => 'https://soulativo.com.br/paypal/refused/' . $this->id,
            'description' => 'test purchase'
        ];

        $response = Omnipay::purchase($params)->send();

        // Process response
        if ($response->isRedirect()) {
            // Redirect to offsite payment gateway
            return $response->redirect();
        } else {
            // Payment failed
            $request->session()->flash('status', $response->getMessage());
            $this->delete();
        }
    }

    // Do a Transaction through PagSeguro provider
    public function pagSeguro(Request $request, $data) {
        $items = [];

        foreach($data['bookings'] as $bk):
            $package = Package::find($bk->package_id);
            $items[] = array(
                'id' => $bk->id,
                'description' => $package->product->name,
                'quantity' => '1',
                'amount' => $bk->total,
                'weight' => '45',
                'shippingCost' => '0',
                'width' => '0',
                'height' => '0',
                'length' => '0',
            );
        endforeach;

        $clientName = $data['user']['first_name'] . " " . $data['user']['last_name'];
        $dados = array(
            'items' => $items,
            'shipping' => array(
                'address' => array(
                    'postalCode' => str_replace('-', '', $data['zipcode']),
                    'street' => $data['address']['street'],
                    'number' => $data['address']['number'],
                    'district' => $data['address']['neighbourhood'],
                    'city' => $data['address']['city'],
                    'state' => 'SP',
                    'country' => 'BRA',
                ),
                'type' => 3,
                'cost' => 0
            ),
            'sender' => array(
                'email' => $data['user']['email'],
                'name' => $clientName,
                'documents' => [
                    [
                        'number' => $data['customer']['document'],
                        'type' => 'CPF'
                    ]
                ],
                'phone' => $data['customer']['telephone'],
                'bornDate' => null,

            ),
            'reference' => $this->id,
            'redirectURL' => 'https://soulativo.com.br/pagseguro/redirect/' . $this->id
        );


        $checkout = PagSeguro::checkout()->createFromArray($dados);
        $credentials = PagSeguro::credentials()->get();

        try {
            $information = $checkout->send($credentials);

            if ($information) {
              print_r($information->getCode());
              print_r($information->getDate());
              print_r($information->getLink());
            }

            return redirect($information->getLink());
        } catch(\RuntimeException $response) {
            $request->session()->flash('status', $response->getMessage());
            return back();
        }
    }

    // Do a Transaction through MercadoPago provider
    public function mercadoPago($request) {
        $this->save();

        $mp = new MP(config('mercadopago.app_id'), config('mercadopago.app_secret'));

        $itens = [];
        foreach($request['bookings'] as $booking):
            $package = Package::where('id', $booking->package_id)->first();
            $itens[] = [
                "title" => $package->product->name,
                "quantity" => $booking->adults + $booking->childs,
                "currency_id" => "BRL",
                "unit_price" => $package->product->getDiscountedPrice()
            ];
        endforeach;

        $data = [
            "external_reference" => $this->id,
            "items" => $itens
        ];

        // Try to process the payment
        try {
            $preference = $mp->create_preference($data);
            return redirect()->to($preference['response']['init_point']);
        } catch (Exception $e){
            dd($e->getMessage());
        }
    }

}
