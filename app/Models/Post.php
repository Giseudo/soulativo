<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Dimsav\Translatable\Translatable;
use App\Models\PostTranslation;
use App\Models\Category;

class Post extends Model
{
    use SoftDeletes;
    use Translatable;
    use Validatable;

    public $translatedAttributes = ['title', 'slug', 'content', 'excerpt'];
    protected $fillable = ['title', 'slug', 'content', 'excerpt', 'thumbnail_id'];
    protected $dates = ['created_at', 'updated_at','deleted_at'];
    public static $root_slug = 'posts';

    /**
     * The fieldset validation settings.
     *
     * @var array
     */
    public static $validation = [
        'post.title' => 'required|min:4',
        'post.slug' => 'required|min:4',
        'post.content' => 'required|min:4',
        'post.excerpt' => 'required|min:4'
    ];

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function thumbnail()
    {
        return $this->hasOne(Media::class, 'id', 'thumbnail_id');
    }

    public function seo()
    {
        return $this->belongsTo(SEO::class);
    }

    public function setThumbnailIdAttribute($value)
    {
        $this->attributes['thumbnail_id'] = $value ?: null;
    }

    public static function findBySlug($slug)
    {
        $translation = PostTranslation::where('slug', $slug)->first();
        return Post::where('id', $translation->post_id)->first();
    }

    public static function getAllByCategorySlug($slug)
    {
        $category = Category::findBySlug($slug);
        return $category->posts;
    }
}
