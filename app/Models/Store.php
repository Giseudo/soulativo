<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Store extends Model
{
    use SoftDeletes;
    use Validatable;
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at','deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'slug', 'document'
    ];

    /**
     * The fieldset validation settings.
     *
     * @var array
     */
    public static $validation = [
        'store.name' => 'required|min:4',
        'store.slug' => 'required|min:4',
        'store.document' => 'required|min:4'
    ];

    /**
     * Get the customer's address.
     */
    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    /**
    * Get the user's full name.
    *
    * @return string
    */
    public static function getValidation()
    {
            return static::$validation;
    }
}
